var gulp = require('gulp');
var dotenv = require('dotenv');
var variableExpansion = require('dotenv-expand');
const env = dotenv.config().parsed;

gulp.task('default', function () {

    gulp.src([
        '!../components/node_modules',
        '!../components/node_modules/**/*',
        '!../components/*',
        '../components/**/*',
    ])
        .pipe(gulp.dest(env.AD+'/node_modules/components'))
        .pipe(gulp.dest(env.CU+'/node_modules/components'))
		.pipe(gulp.dest(env.CHAT+'/node_modules/components'))
		.pipe(gulp.dest(env.SEL+'/node_modules/components'))
		;

    gulp.src(['../components/index.js',])
        .pipe(gulp.dest(env.AD+'/node_modules/components'))
        .pipe(gulp.dest(env.CU+'/node_modules/components'))
		.pipe(gulp.dest(env.CHAT+'/node_modules/components'))
		.pipe(gulp.dest(env.SEL+'/node_modules/components'))
		;
});

gulp.task('copy-modules',function () {

    gulp.src([
        '../components/node_modules/**/*',
    ])
        .pipe(gulp.dest(env.AD+'/node_modules/components/node_modules'))
        .pipe(gulp.dest(env.CU+'/node_modules/components/node_modules'));

})

gulp.task('copy-other',function () {

    gulp.src([
        '../components/*',
    ])
        .pipe(gulp.dest(env.AD+'/node_modules/components'))
        .pipe(gulp.dest(env.CU+'/node_modules/components'));

})


gulp.task('copy-emoji', function () {
    let stream = gulp.src(env.CHAT+'/node_modules/emojione/assets/png/*')
        .pipe(gulp.dest(env.CHAT + '/public/images/emoji/emojione/png'));

    stream.on('end', () => {
        process.emit('exit');
    })
});

gulp.watch(['../components/src/**/*',
    '../components/index.js'], function () {

    gulp.start('default');
    // gulp.start('copy-modules');
    // gulp.start('copy-other');
    console.log('Обновлен');
});

process.on('exit', function () {
    process.nextTick(function () {
        process.exit(0)
    })
});

