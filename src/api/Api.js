import ApiRequest from "./ApiRequest";
import { CR } from 'components';

export default class Api{
	
	//CONSTRUCT
	static create(target, focus, data = {}, method = 'GET'){
		return new ApiRequest(target, focus, data, method);
	}
	static createArg(target, focus, arg, data = {}, method = 'GET'){
		return new ApiRequest(target, focus, data, method).addArg(arg);
	}
	
	//SELF
	static getArg(target, focus, arg, data = {}){
		return Api.get(target, focus, data).addArg(arg);
	}
	static postArg(target, focus, arg, data = {}){
		return Api.post(target, focus, data).addArg(arg);
	}
	static putArg(target, focus, arg, data = {}){
		return Api.put(target, focus, data).addArg(arg);
	}


	//CONSTRUCT
	static get(target, focus, data = {}){
		return new ApiRequest(target, focus, data, 'GET');
	}
	static post(target, focus, data = {}){
		return new ApiRequest(target, focus, data, 'POST');
	}
	static put(target, focus, data = {}){
		return new ApiRequest(target, focus, data, 'PUT');
	}
	static delete(target, focus, id, data = {}){
		return new ApiRequest(target, focus, data, 'DELETE').addArg(id);
	}

	static makeRequest(obj)
	{
		var type = 'csrf_token';
		obj.url = 'http://' + window.location.hostname + obj.url;
		obj.xhrFields = { withCredentials: true };
		
		if(typeof obj["data"] === 'undefined'){
			obj["data"] = {};
		}
		if(typeof obj["headers"] === 'undefined'){
			obj["headers"] = {};
		}

		if(type === 'csrf_token'){
			//csrf_token
			obj["headers"]["X-CSRF-TOKEN"] = $("meta[name='_token']").attr("content");
		}else if(type === 'access_token'){
			//access_token
			obj["data"]["access_token"] = localStorage.getItem('access_token');
		}
		
		//console.log(obj);
		
		$.ajax(obj);
	}
};