import Noty from 'noty';

export default class Notify
{
    constructor() {
        this.notify = [];
    }

    add(notify) {
        this.notify.push(notify);
    }

    clear() {
        this.notify = [];
    }

    showUserMessages(type) {
        let self = this;

        this.notify
            .filter(
                function (item) {
                    return type === item.type;
                }
            )
            .map(
                function (item, key) {
                    new Noty(item).show();
                    delete self.notify[key];
                }
            );
    }

    showDefaultMessages(type, text) {
        switch(type) {
            case 'success':
                this.notify.filter((item) => !item.text.length && !item.type.length).map((item) => {
                        item.text = text;
                        item.type = type;
                        new Noty(item).show();
                    }
                );
                break;
            case 'error':
                this.notify.filter((item) => !item.text.length && !item.type.length).map((item) => {
                        Object.values(text).map((error) => {
                            item.text = error[0];
                            item.type = type;
                            new Noty(item).show();
                        });
                    }
                );
                break;
        }
    }

    show(type = 'success', text) {
        if (typeof text !== 'undefined') {
            this.showDefaultMessages(type, text);
        } else {
            this.showUserMessages(type)
        }

        this.clear();
    }
};