import { CR } from 'components';

export default class Preloader
{
    constructor(layoutName = 'app') {
        this.layout = CR.get(layoutName);
    }

    show() {
        if (typeof this.layout !== 'undefined' && 'preload' in this.layout.refs) {
            this.layout.refs.preload.show();
        }
    }

    hide() {
        if (typeof this.layout !== 'undefined' && 'preload' in this.layout.refs) {
            this.layout.refs.preload.hide();
        }
    }
};