import Builder from "./Builder";
import Binding from "./Binding";
import Api from "./Api";
import { CR } from 'components';
import Notify from './Notify';
import Preloader from './Preloader';

export default class ApiRequest{

    /**
     *
     * @param target
     * @param focus
     * @param data
     * @param method
     */
    constructor(target, focus, data = {}, method = 'GET'){
        let self = this;
        this.target = target;
        this.focus = focus;
        this.method = method;
        this.data = data;
        this.arguments = [];
        this.builder = new Builder();
        this.notify = new Notify();
        this.preloader = new Preloader();
        this.responseBinding = null;
        this.responseBindingErrors = null;

        Builder.availableMethod.map(function(val){
            self[val] = function(){
                self.builder.add(val, Array.prototype.slice.call(arguments));
                return self;
            }
        });
    }

    /**
     *
     * @param arg
     * @returns {ApiRequest}
     */
    addArg(arg){
        if(Array.isArray(arg)){
            this.arguments = this.arguments.concat(arg);
        }else{
            this.arguments.push(arg);
        }
        return this;
    }

    /**
     *
     * @param success
     * @param error
     * @returns {*}
     */
    first(success = function(){}, error = function(){}){
        this.data['paginateType'] = 'first';
        return this.call(success, error);
    }

    /**
     *
     * @param success
     * @param error
     * @returns {*}
     */
    all(success = function(){}, error = function(){}){
        this.data['paginateType'] = 'all';
        return this.call(success, error);
    }

    /**
     *
     * @param page
     * @param success
     * @param error
     * @returns {*}
     */
    paginate(page = 1, success = function(){}, error = function(){}){
        this.data['paginateType'] = 'paginate';
		this.data['page'] = page;
        return this.call(success, error);
    }

    /**
     *
     * @param success
     * @param error
     * @returns {ApiRequest}
     */
    call(success = function(){}, error = function(){}){
        let self = this;
        self.preloader.show();

        Api.makeRequest({
            url: window.laroute.route('api.v1.call', {target: this.target, method: this.focus}),
            method: this.method,
            data: {
                arguments: this.arguments,
                query: this.builder.toArray(),
                data: this.data,
            },
            success: function(response){
                self.preloader.hide();
                self.notify.show('success', response.meta.text);
                self.toBind(response);
                success(response);
            },
            error: function(response){
                console.log(response);
                self.preloader.hide();
                self.notify.show('error', response.responseJSON.meta.errors);
                self.toBindErrors(response);
                error(response);
            }
        });

        return this;
    }

    /**
     *
     * @param obj
     * @param item
     * @param key
     */
    bind(obj, item, key='data'){
        let self = this;
        if(typeof item == 'object'){
            this.responseBinding = [];
            item.map(function(data){
                self.responseBinding[self.responseBinding.length] = new Binding(obj, data[0], data[1]);
            })
        }else{
            this.responseBinding = new Binding(obj, item, key);
        }
    }

    /**
     *
     * @param response
     */
    toBind(response) {
        if(this.responseBinding !== null){
            if(Array.isArray(this.responseBinding)){
                //this.responseBinding.map(function(data){
                //    data.fire(response);
                //})
            }else{
                this.responseBinding.fire(response);
            }
        }
    }

    withNotify(text = '', type = '') {
        this.notify.add({
            type: type,
            layout: 'topRight',
            text: text,
            timeout: 3000,
            closeWith: ['button'],
        });

        return this;
    }

    toBindErrors(response = {}) {
        if(this.responseBindingErrors !== null && 'responseJSON' in response){
            this.responseBindingErrors.fire(response.responseJSON.meta.errors);
        }
    }

    withValidateForm(obj, item = 'errors', key='meta') {
        this.responseBindingErrors = new Binding(obj, item, key);
    }
};