
export default class Builder{
	
	static availableMethod = [
		'select',
		'where',
		'has',
		'whereIn',
		'whereNotIn',
		'whereHas',
		'orderBy',
		'groupBy',
		'whereNull',
		'whereNotNull',
		'with',
		'limit',
	];
	
	constructor(){
		var self = this;
		this.query = [];
		Builder.availableMethod.map(function(val){
			self[val] = function(){
				self.add(val, Array.prototype.slice.call(arguments));
				return self;
			}
		});
	}
	
	add(method, args){
		this.query.push({});
		args.map(function(val, key){
			if(typeof val == 'function'){
				var func = val;
				args[key] = {query: new Builder()};
				args[key].query = func(args[key].query).toArray();
			}
		});
		this.query[this.query.length - 1][method] = args;
		return this;
	}
	
	toArray(){
		return this.query;
	}
};