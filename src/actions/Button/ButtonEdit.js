import React from 'react';
import { Button } from 'components';
import { Link } from 'react-router-dom';

/**
 * Class ButtonEdit.
 */
export class ButtonEdit extends React.Component {

    static defaultProps = {
        content: '',
        title: 'Редактировать',
        useIcon: true,
        icon: 'edit',
        background: 'blue',
        as: Link,
    };

    constructor(props) {
        super(props);
    }

    /**
     * Render.
     *
     * @returns {XML}
     */
    render() {
        const { ...rest } = this.props;
        
        return (
            <Button.Default { ...rest } />
        );
    }
}