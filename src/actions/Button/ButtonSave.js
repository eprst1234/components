import React from 'react';
import { Button, Popup, Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

/**
 * Class ButtonSave.
 */
export class ButtonSave extends React.Component {

    static propTypes = {
        content: PropTypes.string,
        title: PropTypes.string,
        useIcon: PropTypes.bool,
        icon: PropTypes.string,
    };

    static defaultProps = {
        content: '',
        title: 'Save',
        useIcon: true,
        icon: 'save',
    };

    constructor(props) {
        super(props);
    }

    /**
     * Render.
     *
     * @returns {XML}
     */
    render() {
        const { title, useIcon, icon, content, ...rest } = this.props;

        return (
            <Popup
                trigger={
                    <Button
                        className='small'
                        positive
                        { ...rest }
                    >
                        { useIcon && (<Icon name={ icon } />)}
                        { ' ' + content }
                    </Button>
                }
                content={ title }
            />
        );
    }
}