import React from 'react';
import { Button } from 'components';
import PropTypes from 'prop-types';
import copy from 'copy-to-clipboard';

/**
 * Class ButtonCopy.
 */
export class ButtonCopy extends React.Component {

    static propTypes = {
        titleSuccess: PropTypes.string,
        copyText: PropTypes.string,
    };

    static defaultProps = {
        title: 'Скопировать',
        titleSuccess: 'Скопировано!',
        useIcon: true,
        icon: 'copy',
        content: '',
        background: 'blue',
        copyText: ' ',
        type: 'button',
        className: 'copy',
    };

    constructor(props) {
        super(props);

        this.timeoutLength = 3000;

        this.state = {
            title: this.props.title
        };
    }

    copyText = () => {
        const { copyText, title, titleSuccess } = this.props;
        let self = this;

        copy(copyText);
        self.setState({title: titleSuccess});
        setTimeout(() => {
            self.setState({title: title})
        }, self.timeoutLength);
    };

    /**
     * Render.
     *
     * @returns {*}
     */
    render() {
        const { title } = this.state;
        const { title: titleProps, titleSuccess, copyText, ...rest } = this.props;

        return (
            <Button.Default
                onClick={ this.copyText }
                title={ title }
                { ...rest }
            />
        );
    }
}