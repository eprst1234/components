import React from 'react';
import { Button } from 'components';

/**
 * Class ButtonGeneratePassword.
 */
export class ButtonGeneratePassword extends React.Component {

    static defaultProps = {
        content: 'Сгенерировать пароль',
        title: 'Сгенерировать рандомный пароль',
        useIcon: false,
        icon: 'edit',
        background: 'grey',
        type: 'button'
    };

    constructor(props) {
        super(props);
    }

    handlerGeneratePassword = () => {
        let chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
        let pass = "";
        for (let i = 0; i < 15; i++) {
            pass += chars.charAt(Math.floor(Math.random() * chars.length));
        }

        this.props.onGeneratePassword({ password: pass });
    };

    /**
     * Render.
     *
     * @returns {*}
     */
    render() {
        const { onGeneratePassword, ...rest } = this.props;

        return (
            <Button.Default
                onClick={ this.handlerGeneratePassword }
                { ...rest }
            />
        );
    }
}