import React from 'react';
import { Button, Popup, Icon } from 'semantic-ui-react';
import { ButtonSave } from './ButtonSave';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

/**
 * Class ButtonSorting.
 */
export class ButtonSorting extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            saveSortedElements: false,
        };
    }

    static propTypes = {
        content: PropTypes.string,
        title: PropTypes.string,
        useIcon: PropTypes.bool,
        icon: PropTypes.string,
        color: PropTypes.string,
        onClick: PropTypes.func,
        onSave: PropTypes.func,
    };

    static defaultProps = {
        content: '',
        title: 'Sorted element',
        useIcon: true,
        icon: 'sort',
        color: 'brown',
        onClick: () => {},
        onSave: () => {},
    };

    handleSorted(callback) {
        //if (this.state.saveSortedElements) {
        //    this.props.onSave();
        //}

        this.setState({ saveSortedElements: !this.state.saveSortedElements });
        callback();
    }

    /**
     * Render.
     *
     * @returns {XML}
     */
    render() {
        const { title, useIcon, icon, content, color, onClick, onSave, ...rest } = this.props;
        const { saveSortedElements } = this.state;

        if (saveSortedElements) {
            return (
                <Popup
                    trigger={
                        <Button
                            className='small'
                            color='teal'
                            onClick={ this.handleSorted.bind(this, onClick) }
                            { ...rest }
                        >
                            { useIcon && (<Icon name='checkmark box' />)}
                        </Button>
                    }
                    content='Выбрать елементы'
                />
            );
        }

        return (
            <Popup
                trigger={
                    <Button
                        className='small'
                        color={ color }
                        onClick={ this.handleSorted.bind(this, onClick) }
                        { ...rest }
                    >
                        { useIcon && (<Icon name={ icon } />)}
                        { ' ' + content }
                    </Button>
                }
                content={ title }
            />
        );
    }
}