import React from 'react';
import { Button, Popup, Icon, Modal } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

/**
 * Class ButtonRemove.
 */
export default class ButtonRemove extends React.Component {

    static propTypes = {
        cancelButton: PropTypes.string,
        confirmButton: PropTypes.string,
        content: PropTypes.string,
        title: PropTypes.string,
        useIcon: PropTypes.bool,
        icon: PropTypes.string,
        confirmContent: PropTypes.string,
        onConfirm: PropTypes.func,
    };

    static defaultProps = {
        cancelButton: 'Отменить',
        confirmButton: 'Удалить',
        content: '',
        title: 'Удалить',
        useIcon: true,
        icon: 'trash',
        confirmHeader: 'Внимание!',
        confirmContent: 'Вы действительно хотите удалить выбранные елементы?',
        onConfirm: () => {},
    };

    constructor(props) {
        super(props);

        this.state = {
            openConfirm: false
        }
    }

    show = () => this.setState({ openConfirm: true });
    handleCancel = () => this.setState({ openConfirm: false });
    handleConfirm = () => {
        this.setState({ openConfirm: false });
        this.props.onConfirm();
    };

    /**
     * Render.
     *
     * @returns {*}
     */
    render() {
        const {
            title,
            useIcon,
            icon,
            content,
            confirmHeader,
            confirmContent,
            confirmButton,
            cancelButton
        } = this.props;
        const { openConfirm } = this.state;

        return (
            <div>
                <Popup
                    trigger={
                        <Button
                            className='small'
                            negative
                            onClick={ this.show }
                        >
                            { useIcon && (<Icon name={ icon } />)}
                            { ' ' + content }
                        </Button>
                    }
                    content={ title }
                />

                <Modal size={'tiny'} open={openConfirm}>
                    <Modal.Header>
                        { confirmHeader }
                    </Modal.Header>
                    <Modal.Content>
                        { confirmContent }
                    </Modal.Content>
                    <Modal.Actions>
                        <Button
                            negative
                            onClick={this.handleCancel}
                            content={cancelButton}
                        />
                        <Button
                            positive
                            onClick={this.handleConfirm}
                            icon='checkmark'
                            labelPosition='right'
                            content={confirmButton}
                        />
                    </Modal.Actions>
                </Modal>
            </div>
        );
    }
}