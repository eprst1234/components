import React from 'react';
import { Button } from 'components';
import { Link } from 'react-router-dom';

/**
 * Class ButtonView.
 */
export class ButtonView extends React.Component {

    static defaultProps = {
        content: '',
        title: 'Просмотр',
        useIcon: true,
        icon: 'eye',
        background: 'teal',
        as: Link,
    };

    constructor(props) {
        super(props);
    }

    /**
     * Render.
     *
     * @returns {XML}
     */
    render() {
        const { ...rest } = this.props;

        return (
            <Button.Default { ...rest } />
        );
    }
}