import React from 'react';
import { Button, Popup, Icon } from 'semantic-ui-react';
import PropTypes from 'prop-types';

/**
 * Class ButtonDefault.
 */
export class ButtonDefault extends React.Component {

    static propTypes = {
        content: PropTypes.string,
        title: PropTypes.string,
        useIcon: PropTypes.bool,
        icon: PropTypes.string,
        background: PropTypes.string,
        className: PropTypes.string,
        as: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.element,
            PropTypes.func,
        ]),
        to: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.bool,
        ]),
    };

    static defaultProps = {
        content: '',
        title: 'Default button',
        useIcon: true,
        icon: 'info',
        background: 'green',
        className: 'small',
    };

    constructor(props) {
        super(props);
    }

    /**
     * Render.
     *
     * @returns {*}
     */
    render() {
        const { title, useIcon, icon, content, background, className, ...rest } = this.props;

        return (
            <Popup
                trigger={
                    <Button
                        className={ className }
                        color={ background }
                        { ...rest }
                    >
                        { useIcon && (<Icon name={ icon } />)}
                        { ' ' + content }
                    </Button>
                }
                content={ title }
            />
        );
    }
}