import { ButtonSave } from './ButtonSave';
import ButtonRemove from './ButtonRemove';
import { ButtonCreate } from './ButtonCreate';
import { ButtonSorting } from './ButtonSorting';
import { ButtonDefault } from './ButtonDefault';
import { ButtonEdit } from './ButtonEdit';
import { ButtonView } from './ButtonView';
import { ButtonCopy } from './ButtonCopy';
import { ButtonGeneratePassword } from './ButtonGeneratePassword';

/**
 * Class Button.
 */
export default class Button
{
    /**
     * Кнопка для сохранения.
     *
     * @type {ButtonRemove}
     */
    static Save = ButtonSave;

    /**
     * Кнопка для удаления выбранных елементов.
     *
     * @type {ButtonRemove}
     */
    static Remove = ButtonRemove;

    /**
     * Кнопка для создания елемента.
     *
     * @type {ButtonCreate}
     */
    static Create = ButtonCreate;

    /**
     * Кнопка для сортировки.
     *
     * @type {ButtonSorting}
     */
    static Sorting = ButtonSorting;

    /**
     * Кнопка для редактирования.
     *
     * @type {ButtonEdit}
     */
    static Edit = ButtonEdit;


    /**
     * Кнопка для просмотра.
     *
     * @type {ButtonView}
     */
    static View = ButtonView;

    /**
     * Кнопка копирования текста.
     *
     * @type {ButtonCopy}
     */
    static Copy = ButtonCopy;

    /**
     * Кнопка генерации пароля.
     *
     * @type {ButtonGeneratePassword}
     */
    static GeneratePassword = ButtonGeneratePassword;

    /**
     * Кнопка по умолчанию.
     *
     * @type {ButtonDefault}
     */
    static Default = ButtonDefault;
}
