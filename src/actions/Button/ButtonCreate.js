import React from 'react';
import { Button, Popup, Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

/**
 * Class ButtonCreate.
 */
export class ButtonCreate extends React.Component {

    constructor(props) {
        super(props);
    }

    static propTypes = {
        content: PropTypes.string,
        title: PropTypes.string,
        useIcon: PropTypes.bool,
        icon: PropTypes.string,
        as: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.element,
            PropTypes.func,
        ]),
        to: PropTypes.oneOfType([
            PropTypes.string.isRequired,
            PropTypes.bool,
        ]),
    };

    static defaultProps = {
        content: '',
        title: 'Create',
        useIcon: true,
        icon: 'add',
        as: Link,
        to: '#',
    };

    /**
     * Render.
     *
     * @returns {XML}
     */
    render() {
        const { title, useIcon, icon, content, as, to, ...rest } = this.props;

        return (
            <Popup
                trigger={
                    <Button
                        className='small'
                        primary
                        as={ as }
                        to={ to }
                        { ...rest }
                    >
                        { useIcon && (<Icon name={ icon } />)}
                        { ' ' + content }
                    </Button>
                }
                content={ title }
            />
        );
    }
}