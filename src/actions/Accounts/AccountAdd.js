import React from 'react';
import {BaseDropdown, CR} from 'components';

export default class AccountAdd extends React.Component
{
    /**
     * 
     * @param props
     */
    constructor(props){
        super(props);
        this.state = {
			pay_methods: [],
			form: {
				pay_method_id: '',
				sum: this.props.startSum,
			},
			course: 1,
            rubl:this.props.startSum,
            converted: '',
        };
    }
	
	componentDidMount(){
		var self = this;
		CR.resolve('payMethod').set(function(data){
			self.setState({pay_methods: data});
		});
		//this.setState({pay_methods: CR.resolve('payMethod').get()});
	}
	
	changeSum(event){
        var course = this.state.course;
        var value = event.target.value;
        this.setState(function (prv) {
            prv.rubl=value,
            prv.converted = value / course;
			return prv;
        });
        this.setState(function (prv) {
            prv.form.sum = value;
			return prv;
        });
		this.props.handleChange('sum', value);
    }

    changeType(value) {
        this.setState(function (prv) {
            prv.form.pay_method_id = value;
			return prv;
        });
        let course = 1;
        this.state.pay_methods.map(function (crs) {
            if (value == crs.id)
                course = crs.course;
			return crs;
        });
		
        this.setState({
            course:course,
            converted: this.state.rubl / course,
        });
		this.props.handleChange('pay_method_id', value);
    }

    render(){
        return (
			<div>
				<div><label>{`На оплату:${this.state.converted}`}</label></div>
				<BaseDropdown onChange={this.changeType.bind(this)} value={this.state.form.pay_method_id} options={{data: this.state.pay_methods, value: 'id', text: 'name'}}/>
				<input onChange={this.changeSum.bind(this)} value={this.state.form.sum} placeholder="Сумма"/>(р.)
				<button onClick={this.props.handleSubmit}>Оплатить</button>
				<br/>
				<br/>
		   </div>
        )
    }
};
