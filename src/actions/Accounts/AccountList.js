import React from 'react';
import {Timer} from 'components';

export default class AccountList extends React.Component
{
   constructor(props){
        super(props);
        this.state = {
			
        };
    }
	
	getFields(data){
		return (
			<div key={data.id}>
						<h3>Счет #{data.id}</h3>
						<p>Адрес: {data.account_number}</p>
						<p>Платежная система: {data.payMethod.name}</p>
						<p>На оплату: {data.sum}(р.) / {data.sum / data.course}({data.payMethod.currency})</p>
						<p>Оплачено: {data.sum_pay}(р.)
							/ {data.sum_pay / data.course}({data.payMethod.currency})</p>
						<p>Осталось: {data.sum - data.sum_pay}(р.)
							/ {(data.sum - data.sum_pay) / data.course}({data.payMethod.currency})</p>
						<p>Курс: {data.course}</p>
						<p>Создан: {data.created_at}</p>
						<p>Истекает: {data.expired_at}</p>
						<p>Осталось: {<Timer seconds={data.expired_second} countdown={true}
											 countdownSlug={'Истекло'}/>}</p>
						<p>Статус: {data.status}</p>
					</div>
		);
	}
	
	
	render(){
		var self = this;
		if(Array.isArray(this.props.items)){
			var content = (<div>{this.props.items.map(function (data) {
					return self.getFields(data);
				})}</div>);
		}else{
			var content = (<div>{self.getFields(self.props.items)}</div>);
		}
        return (
			<div>
				{content}
		   </div>
        )
    }
};