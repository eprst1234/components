import React from "react";
import {Checkbox, Form, Segment, TextArea} from "semantic-ui-react";

export default class ApproveField extends React.Component {

    constructor(props) {
        super(props)
        this.state = {

        }
    }
	
	static defaultProps = {
		template: false,
	}
	
	getApproveSlug(){
		switch(this.props.is_approved){
			case 0: return 'В ожидании';
			case 1: return 'Одобрено';
			case 2: return 'Отклонено';
		}
	}
	
	getReason(){
		if(this.props.is_approved !== 2){
			return null;
		}else{
			return this.props.reason;
		}
	}
	
	getReasonWithTemplate(call){
		if(this.getReason() !== null){
			return call(this.getReason());
		}
		return null;
	}
	
	getTemplate(){
		if(!this.props.template){
			return (
				<div>
					{this.getApproveSlug()}
					{this.getReasonWithTemplate(function(reason){
						return (<p>Причина отклонения: {reason}</p>)
					})}
				</div>
			);
		}else{
			return this.props.template(this.getApproveSlug(), this.getReasonWithTemplate.bind(this), this.getReason());
		}
	}

    render() {
        return (
			<div>
				{this.getTemplate()}
			</div>
        )
    }

}