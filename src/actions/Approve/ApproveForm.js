import React from "react";
import {Checkbox, Form, Segment, TextArea} from "semantic-ui-react";
import PropTypes from 'prop-types';

export default class ApproveForm extends React.Component {

    static propTypes = {
        isApproved: PropTypes.number,
        id: PropTypes.number,
        reason: PropTypes.string,
        onChange: PropTypes.func,
    };

    static defaultProps = {
        isApproved: 0,
        id: 0,
        reason: '',
        onChange: () => {},
    };

    constructor(props) {
        super(props);

        this.state = {
            isApproved: props.isApproved,
            reason: props.reason !== null ? props.reason : '',
        };
    }

    getSegment = (isApproved) => {
        let segment = {};

        switch (isApproved) {
            case 2:
                segment = { inv: false, color: 'red' };
                break;
            case 1:
                segment = { inv: false, color: 'green' };
                break;
            default:
                segment = { inv: false, color: 'grey' };
        }

        return segment;
    };

    handleChange = (e, { name, value }) => {
        this.setState({ [name]: value });
        this.props.onChange(e, { ...this.state, id: this.props.id, [name]: value });
    };

    isApproved = () => {
        const { isApproved, reason } = this.state;

        return parseInt(isApproved) === 2 && (
            <Form.TextArea
                autoHeight
                placeholder='Причина'
                value={ reason }
                name='reason'
                onChange={ this.handleChange }
            />
        )
    };

    render() {
        const { isApproved } = this.state;
        const segment = this.getSegment(isApproved);

        return (
            <Segment.Group>
                <Form>
                    <Segment inverted={ segment.inv } color={ segment.color }>
                        <Form.Group>
                            <Form.Radio
                                label='Одобрить'
                                name='isApproved'
                                value={ 1 }
                                checked={ parseInt(isApproved) === 1 }
                                onChange={ this.handleChange }
                            />
                            <Form.Radio
                                label='Отклонить'
                                name='isApproved'
                                value={ 2 }
                                checked={ parseInt(isApproved) === 2 }
                                onChange={ this.handleChange }
                            />
                        </Form.Group>
                        <Form.Field>
                            { this.isApproved() }
                        </Form.Field>
                    </Segment>
                </Form>
            </Segment.Group>
        )
    }
}

/*

this.state = {
            segment:{inv:false,color:'white'}
        };

 getApprove(approve){
        if(this.state.isApproved==1){
            this.setState({
                segment:{inv:true,color:'red'}
            })
        }
        else{
            this.setState({
                segment:{inv:false,color:'white'}
            })
        }
        this.setState({
            isApproved:approve
        })
    }

render(){
 <BaseApprove onApprove={this.getApprove.bind(this)}/>
}

 */