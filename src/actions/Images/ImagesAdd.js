import React from 'react';
import DropZone from './../../base/DropZone';
import {
    Image
} from 'semantic-ui-react';
import ImagesPreview from './ImagesPreview';

export default class ImagesAdd extends React.Component
{
    /**
     * 
     * @param props
     */
    constructor(props){
        super(props);
        this.state = {
			images: [],
        };

        this.finishReadFile = this.finishReadFile.bind(this);
    }
	
	getImages(){
		if(this.props.multi)
			return this.state.images;
		else
			return this.state.images[0];
	}
	
	handleDelete(key){
		this.setState(function(prv){
			prv.images.splice(key, 1);
			return prv;
		});
	}
	
	finishReadFile(file){
		var self = this;
		if(this.props.multi){
			this.setState(function(prv){
				prv.images.push(self.prepareItem(file));
				return prv;
			});
		}else{
            this.setState({ images: [self.prepareItem(file)] });
		}
	}
	
	prepareItem(image) {
		return {
			content: image.content,
			name: image.name,
			type: image.type,
			size: image.size,
		};
	}

    render(){
	    const { images } = this.state;
		let self = this;
		//var Preview = React.createElement(this.props.preview);
		const Preview = this.props.preview;
        return (
			<div>
				<div>
					<Preview {...this.props.propsPreview} images={this.state.images}/>
					{/*images.length > 0 && this.state.images.map(function(data, key){
						return (<div key={key}>
							<p>{data.name}</p>
							<Image src={data.content}/>
							<button onClick={self.handleDelete.bind(self, key)}>Удалить</button>
						</div>);
					})*/}
				</div>
				<DropZone finishRead={this.finishReadFile}/>
		   </div>
        )
    }
};
ImagesAdd.defaultProps = {
  multi: true,
  preview: ImagesPreview,
  propsPreview: {},
};