import React from 'react';
import {
    Image
} from 'semantic-ui-react';

export default class ImagesPreview extends React.Component
{
	constructor(props){
		super(props);
		this.state = {
			
		}
	}
	
	handleDelete(key){
		this.setState(function(prv,props){
			props.images.splice(key, 1);
			return props;
		});
	}
	
	
    render(){
		var self = this;
		return(<div>
			{this.props.images.map(function(data, key){
				return (<div key={key}>
								<p>{data.name}</p>
								<Image src={data.content}/>
								<button onClick={self.handleDelete.bind(self, key)}>Удалить</button>
							</div>);
			})}
		</div>);
	}
};