import React from 'react';
import { Api, ImagesUpload } from 'components';
import { Button, Grid, Modal } from 'semantic-ui-react';
import PropTypes from 'prop-types';

/**
 * Class Account.
 */
export default class ChangeAvatar extends React.Component {

    constructor() {
        super();

        this.state = {
            modalOpen: false,
            avatarLoaded: false,
        };
    }

    static propTypes = {
        onSuccess: PropTypes.func
    };

    static defaultProps = {
        onSuccess: () => {},
    };

    handleAvatarLoaded = (status = true) => {
        this.setState({ avatarLoaded: status });
    };

    modalOpen = (status = true) => {
        this.setState({ modalOpen: status });
    };

    handleChangeAvatar = () => {
        let self = this;
        this.handleAvatarLoaded(false);
        this.modalOpen(false);

        Api.put('profile', 'avatar', this.refs.images.getImages())
            .call(
                () => {
                    self.props.onSuccess();
                    self.handleAvatarLoaded(true);
                },
                () => {
                    self.handleAvatarLoaded(true);
                }
            );
    };

    getAvatar() {
        return this.refs.images.getImages();
    }

    modal = () => {
        const { avatarLoaded, modalOpen } = this.state;

        let image = null;
        if (typeof this.refs.images === 'object') {
            image = (
                <div>
                    <h3>Preview avatar</h3>
                    <ImagesUpload.Preview
                        images={ [this.refs.images.getImages()] }
                        removeButton={ false }
                    />
                </div>
            );
        }

        return (
            <div>
                <Button
                    attached='bottom'
                    content='Change image'
                    onClick={ this.modalOpen.bind(this, true) }
                />
                <Modal
                    open={ modalOpen }
                    onClose={ this.modalOpen.bind(this, false) }
                >
                    <Modal.Header>Change avatar!</Modal.Header>
                    <Modal.Content>
                        <Grid>
                            <Grid.Column width={6}>
                                <h3>Upload avatar</h3>
                                <ImagesUpload ref='images' multi={ false } onSuccess={this.handleAvatarLoaded} />
                            </Grid.Column>
                            <Grid.Column width={10}>
                                { image }
                            </Grid.Column>
                        </Grid>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button
                            positive
                            onClick={ this.handleChangeAvatar }
                            disabled={ !avatarLoaded }
                        >
                            Upload
                        </Button>
                    </Modal.Actions>
                </Modal>
            </div>
        );
    };

    render() {
        return (
            <div>
                { this.modal() }
            </div>
        );
    }

}