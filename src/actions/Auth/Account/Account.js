import React from "react";
import { Auth, CR, BaseHeader } from 'components';
import DotEnv from 'dotenv';
import ChangeAvatar from './ChangeAvatar';
import {
    Button,
    Divider,
    Container,
    Dropdown,
    Grid,
    List,
    Header,
    Icon,
    Image,
    Label,
    Menu,
    Message,
    Segment,
    Table,
    Breadcrumb,
    Form,
    Input,
    Modal
} from 'semantic-ui-react';
import { Link, Redirect, withRouter } from 'react-router-dom';

/**
 * Class Account.
 */
export class Account extends React.Component {

    constructor() {
        super();

        this.state = {
            user: Auth.user
        }
    }

    handleChangeAvatar = () => {
        CR.resolve('user', true);
        this.setState({ user: Auth.user });
    };

    /**
     * Хлебные крошки для страницы восстановить пароль.
     *
     * @returns {[*,*]}
     */
    static getBreadcrumb() {
        return [
            { key: 'auth', content: 'Home', as: Link, to: '/' },
            { key: 'divider-1', divider: 'right arrow' },
            { key: 'forgot', content: 'Profile settings', active: true }
        ];
    }

    header = () => {
        return (
            <BaseHeader
                title='Profile settings'
                breadcrumb={ Account.getBreadcrumb() }
            />
        );
    };

    form = () => {
        const { user } = this.state;

        return (
            <Form>

                <Grid>
                    <Grid.Column width={8}>
                        <Header as='h4' dividing>
                            Basic information
                        </Header>

                        <Grid>
                            <Grid.Column width={15}>
                                <Form.Field inline>
                                    <label>Login</label>
                                    <input placeholder='User name' value={Auth.user.name} disabled/>
                                </Form.Field>
                                <Form.Field inline>
                                    <label>Email</label>
                                    <Input action='Change' placeholder='Email' disabled/>
                                </Form.Field>
                            </Grid.Column>
                        </Grid>
                    </Grid.Column>

                    <Grid.Column width={8}>
                        <Header as='h4' dividing>
                            Change image
                        </Header>

                        <Grid>
                            <Grid.Column width={8}>
                                <Segment attached='top' style={{ padding: 0 }}>
                                    <Image src={'http://market.crabdance.com/' + user.avatar} />
                                </Segment>
                                <ChangeAvatar ref='avatar' onSuccess={this.handleChangeAvatar} />
                            </Grid.Column>
                        </Grid>
                    </Grid.Column>
                </Grid>
            </Form>
        );
    };

    render() {
        return (
            <Container className="page">
                {this.header()}
                <Header as='h4' attached='top' block>
                    <Icon name="user circle outline" /> Profile
                </Header>
                <Segment attached>
                    {this.form()}
                </Segment>
            </Container>
        );
    }

}