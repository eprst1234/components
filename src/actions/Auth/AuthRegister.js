import React from "react";
import { Button, Form, Grid, Header, Segment, Breadcrumb, Message } from 'semantic-ui-react';
import { Link, Redirect } from 'react-router-dom';
import { Api, Auth, FormMessages } from 'components';

/**
 * Class AuthRegister.
 */
export class AuthRegister extends React.Component {

    state = {
        form: {
            nickname: '',
            name: '',
            password: '',
            password_confirmation: '',
            email: '',
        },

        redirectToLogin: false,
    };

    /**
     * Хлебные крошки для страницы регистрации.
     *
     * @returns {[*,*]}
     */
    static getBreadcrumb() {
        return [
            { key: 'auth', content: 'Авторизация', as: Link, to: '/auth/login' },
            { key: 'forgot', content: 'Регистрация', active: true }
        ];
    }

    handleChange = (e, { name, value }) => {
        this.setState({ form: { ...this.state.form, [name]: value } });
    };

    handleSubmit = () => {
        let self = this;
        this.setState({ form: this.state.form });

        Api.makeRequest({
            url: laroute.route('api.v1.auth.register'),
            method: 'POST',
            data: {data: this.state.form},
            dataType: 'json',
            success: function (r) {
                if (r.result === 'success') {
                    let messages = [
                        r.meta.text,
                        'Идет переадресация..'
                    ];
                    self.refs.formMessages.show(messages, '', 'success');

                    setTimeout(() => {
                        self.setState({ redirectToLogin: true });
                    }, 1500);
                }
            },
            error: function (xhr) {
                switch (xhr.status) {
                    case 422:
                        if (xhr.responseJSON.result === 'error') {
                            self.refs.formMessages.show(xhr.responseJSON.meta.errors);
                        }
                        break;
                    default:
                        self.refs.formMessages.show('Произошла ошибка.. Попробуйте еще раз!');
                }
            }
        });
    };

    /**
     * Render.
     *
     * @returns {XML}
     */
    render() {
        const { nickname, name, password, password_confirmation, email } = this.state.form;

        if (this.state.redirectToLogin) {
            return (
                <Redirect to="/" />
            );
        }

        return (
            <div>
                <Breadcrumb icon='right angle' sections={ AuthRegister.getBreadcrumb() } />

                <div className='login-form'>

                    <Grid
                        textAlign='center'
                        style={{height: '100%'}}
                        verticalAlign='middle'
                    >
                        <Grid.Column style={{maxWidth: 450}}>
                            <Header as='h2' color='teal' textAlign='center'>
                                Регистрация
                            </Header>
                            <Form size='large' onSubmit={ this.handleSubmit } >
                                <Segment stacked>
                                    <Form.Input
                                        fluid
                                        icon='user'
                                        iconPosition='left'
                                        type='text'
                                        name='nickname'
                                        placeholder='Логин'
                                        value={ nickname }
                                        onChange={ this.handleChange }
                                        required
                                    />
                                    <Form.Input
                                        fluid
                                        icon='user'
                                        iconPosition='left'
                                        type='text'
                                        name='name'
                                        value={ name }
                                        onChange={ this.handleChange }
                                        placeholder='Имя в системе'
                                    />
                                    <Form.Input
                                        fluid
                                        icon='lock'
                                        iconPosition='left'
                                        type='password'
                                        name='password'
                                        value={ password }
                                        onChange={ this.handleChange }
                                        placeholder='Пароль'
                                    />
                                    <Form.Input
                                        fluid
                                        icon='lock'
                                        iconPosition='left'
                                        type='password'
                                        name='password_confirmation'
                                        value={ password_confirmation }
                                        onChange={ this.handleChange }
                                        placeholder='Пароль еще раз'
                                    />
                                    <Form.Input
                                        fluid
                                        icon='mail'
                                        iconPosition='left'
                                        type='email'
                                        name='email'
                                        value={ email }
                                        onChange={ this.handleChange }
                                        placeholder='E-mail'
                                    />
                                    <Button color='teal' fluid size='large'>Отправить</Button>

                                    <FormMessages ref="formMessages" />

                                </Segment>
                            </Form>
                        </Grid.Column>
                    </Grid>
                </div>
            </div>
        );
    }
}
