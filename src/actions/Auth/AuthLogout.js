import React from "react";
import { Api, Auth } from 'components';
import { Button, Form, Grid, Header, Breadcrumb, Message, Segment } from 'semantic-ui-react';
import { Link, Redirect, withRouter } from 'react-router-dom';

/**
 * Class AuthLogin.
 */
export class AuthLogout extends React.Component {

    logout = () => {
        let { history } = this.props;

        Api.makeRequest({
            url: laroute.route('api.v1.auth.logout'),
            method: 'GET',
            dataType: 'json',
            success: function (r) {
                if (r.result === 'success') {
                    Auth.logout(() => history.push('/auth/login'));
                }
            },
            error: function (xhr) {
                switch (xhr.status) {
                    case 401:
                        Auth.logout(() => history.push('/auth/login'));
                        break;
                    default:
                        console.log(xhr);
                }

            }
        });
    };

    render() {
        this.logout();

        return (
            <div>LOGOUT!</div>
        );
    }

}