import React from "react";
import { Api, Auth, Button as BaseButton, BaseHeader, FormMessages } from 'components';
import {
    Button,
    Container,
    Grid,
    Header,
    Icon,
    Segment,
    Form,
    Input,
    Popup
} from 'semantic-ui-react';
import { Link, Redirect, withRouter } from 'react-router-dom';

/**
 * Class AuthChangePassword.
 */
export class AuthChangePassword extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            form: {
                old_password: '',
                password: '',
                disableSubmit: false,
            },

            inputPasswordType: 'password',
            inputPasswordIcon: 'unhide'
        };
    }

    /**
     * Хлебные крошки для страницы восстановить пароль.
     *
     * @returns {[*,*]}
     */
    static getBreadcrumb() {
        return [
            { key: 'auth', content: 'Авторизация', as: Link, to: '/auth/login' },
            { key: 'divider-1', divider: 'right arrow' },
            { key: 'forgot', content: 'Восстановить доступ', active: true }
        ];
    }

    handleChange = (e, { name, value }) => {
        this.setState({ form: { ...this.state.form, [name]: value } });
    };

    disableSubmit = () => {
        this.setState({ form: { ...this.state.form, ['disableSubmit']: true } });
    };

    enableSubmit = () => {
        this.setState({ form: { ...this.state.form, ['disableSubmit']: false } });
    };

    handleShowPassword = () => {
        this.setState({
            inputPasswordType: this.state.inputPasswordType === 'password' ? 'text' : 'password',
            inputPasswordIcon: this.state.inputPasswordIcon === 'unhide' ? 'hide' : 'unhide',
        });
    };

    handleGeneratePassword = ({ password }) => {
        this.setState({ form: { ...this.state.form, ['password']: password } });
    };


    header = () => {
        return (
            <BaseHeader
                title='Change password'
                breadcrumb={ AuthChangePassword.getBreadcrumb() }
                buttons={
                    <Button
                        type='submit'
                        form='form'
                        positive
                        disabled={this.state.form.disableSubmit}
                    >
                        <Icon name="save" /> Save
                    </Button>
                }
            />
        );
    };

    handleSubmit = () => {
        let self = this;

        self.disableSubmit();
        Api.put('profile', 'password', {
            old_password: self.state.form.old_password,
            password: self.state.form.password
        })
            .call(
                (xhr) => {
                    self.refs.formMessages.show(xhr.meta.text, '', 'success');
                    self.enableSubmit();
                },
                (xhr) => {
                    switch (xhr.status) {
                        case 422:
                            if (xhr.responseJSON.result === 'error') {
                                self.refs.formMessages.show(xhr.responseJSON.meta.text, 'error');
                            }
                            break;
                        default:
                            self.refs.formMessages.show('Произошла ошибка.. Попробуйте еще раз!');
                    }

                    self.enableSubmit();
                }
            )
            .withNotify('Error.', 'error')
            .withNotify('Success.', 'success');
    };

    form = () => {
        const { old_password, password } = this.state.form;

        return (
            <Form id='form' onSubmit={this.handleSubmit}>
                <Grid centered>
                    <Grid.Column width={15}>

                        <FormMessages ref="formMessages" />

                        <Form.Field inline>
                            <label>Old password</label>
                            <Input placeholder='Enter old password'
                                   type='password'
                                   value={ old_password }
                                   name='old_password'
                                   onChange={ this.handleChange }
                            />
                        </Form.Field>

                        <Form.Field inline>
                            <label>New password</label>
                            <Input placeholder='Enter new password'
                                   type={this.state.inputPasswordType}
                                   value={ password }
                                   name='password'
                                   onChange={ this.handleChange }
                                   action
                            >
                                <input />
                                <BaseButton.Default
                                    title={ this.state.inputPasswordIcon === 'unhide' ? 'Show' +
                                        ' password' : 'Hide password' }
                                    onClick={ this.handleShowPassword }
                                    icon={ this.state.inputPasswordIcon }
                                    type={ 'button' }
                                    background={ 'teal' }
                                    className={ 'copy' }
                                    style={{ padding: '0 12px' }}
                                />
                                <BaseButton.Copy
                                    title={ 'Скопировать пароль' }
                                    titleSuccess={ 'Пароль успешно скописовано!' }
                                    copyText={password}
                                />
                                <BaseButton.GeneratePassword
                                    onGeneratePassword={ this.handleGeneratePassword }
                                />
                            </Input>
                        </Form.Field>

                    </Grid.Column>
                </Grid>
            </Form>
        );
    };

    render() {
        return (
            <Container className="page">
                {this.header()}

                <Header as='h4' attached='top' block>
                    <Icon name="user circle outline" /> Change password
                </Header>
                <Segment attached>
                    {this.form()}
                </Segment>
            </Container>
        );
    }
}