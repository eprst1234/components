import { Account } from './Account/Account';
import { AuthChangePassword } from './AuthChangePassword';
import { AuthForgot } from './AuthForgot';
import { AuthLogin } from './AuthLogin';
import { AuthLogout } from './AuthLogout';
import { AuthRegister } from './AuthRegister';
import { CR } from 'components';

/**
 * Class Auth.
 */
export default class Auth
{
    /**
     * Authenticate.
     *
     * @param cb
     */
    static authenticate(cb)
    {
        let user = CR.resolve('user', true);
        let routes = CR.get('app');
        if (typeof routes !== 'undefined') {
            routes.setState({ user: user });
        }
        setTimeout(cb, 100);
    }

    /**
     * Logout.
     *
     * @param cb
     */
    static logout(cb)
    {
        setTimeout(cb, 100);
    }

    /**
     * Check auth.
     *
     * @returns boolean
     */
    static isAuth()
    {
        return !!CR.resolve('user');
    }

    /**
     * Set user to local storage.
     *
     * @param user
     */
    static setUser(user)
    {
        Auth.user = user;
        localStorage.setItem('user', JSON.stringify(user));
    }

	static is(role) {
		return typeof Auth.getUser() !== 'undefined'
            && Auth.getUser() !== null
            && Auth.getUser().role === role;
	}

    /**
     * Get user from local storage.
     *
     */
    static getUser(){
        try {
            return JSON.parse(localStorage.getItem('user'));
        }catch (e){
            return null;
        }
    }
	
	static getSessionId(){
		if(Auth.getUser() !== null){
			return Auth.getUser().session_id;
		}
	}

    /**
     * Профиль пользователя.
     *
     * @type {Account}
     */
    static Account = Account;

    /**
     * Изменить пароль.
     *
     * @type {AuthChangePassword}
     */
    static ChangePassword = AuthChangePassword;

    /**
     * Форма авторизации.
     *
     * @type {AuthLogin}
     */
    static Login = AuthLogin;

    /**
     * Logout.
     *
     * @type {AuthLogout}
     */
    static Logout = AuthLogout;

    /**
     * Форма регистрации.
     *
     * @type {AuthRegister}
     */
    static Register = AuthRegister;

    /**
     * Форма восттановить пароль.
     *
     * @type {AuthForgot}
     */
    static Forgot = AuthForgot;

}

Auth.prototype.user = Auth.getUser();
