import React from "react";
import { Button, Form, Grid, Header, Segment, Breadcrumb } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

/**
 * Class AuthForgot.
 */
export class AuthForgot extends React.Component {

    /**
     * Хлебные крошки для страницы восстановить пароль.
     *
     * @returns {[*,*]}
     */
    static getBreadcrumb() {
        return [
            { key: 'auth', content: 'Авторизация', as: Link, to: '/auth/login' },
            { key: 'forgot', content: 'Восстановить доступ', active: true }
        ];
    }

    /**
     * Render.
     *
     * @returns {XML}
     */
    render() {
        return (
            <div className='login-form'>

                <Breadcrumb icon='right angle' sections={ AuthForgot.getBreadcrumb() } />

                <Grid
                    textAlign='center'
                    style={{height: '100%'}}
                    verticalAlign='middle'
                >
                    <Grid.Column style={{maxWidth: 450}}>
                        <Header as='h2' color='teal' textAlign='center'>
                            ВОССТАНОВИТЬ ДОСТУП
                        </Header>
                        <Form size='large'>
                            <Segment stacked>
                                <Form.Input
                                    fluid
                                    icon='mail'
                                    iconPosition='left'
                                    type='email'
                                    placeholder='E-mail'
                                />
                                <Button color='teal' fluid size='large'>Восстановить</Button>
                            </Segment>
                        </Form>
                    </Grid.Column>
                </Grid>
            </div>
        );
    }
}