import React from "react";
import { Api, Auth, BaseRoutes, FormMessages } from 'components';
import { Button, Form, Grid, Header, Breadcrumb, Message, Segment } from 'semantic-ui-react';
import { Link, Redirect } from 'react-router-dom';

/**
 * Class AuthLogin.
 */
export class AuthLogin extends React.Component {

    state = {
        redirectToReferrer: false,

        form: {
            name: '',
            password: '',
        },
    };

    /**
     * Хлебные крошки для страницы авторизации.
     *
     * @returns {[*]}
     */
    static getBreadcrumb() {
        return [
            { key: 'auth', content: 'Авторизация', active: true }
        ];
    }

    handleChange = (e, { name, value }) => {
        this.setState({ form: { ...this.state.form, [name]: value } });
    };

    handleSubmit = () => {
        let self = this;
        self.setState({ form: this.state.form });

        Api.makeRequest({
            url: window.laroute.route('api.v1.auth.login'), // window.laroute.route('api.v1.auth.register'),
            method: 'POST',
            data: { data: this.state.form },
            dataType: 'json',
            success: function (r) {
                if (r.result === 'success') {
                    Auth.authenticate(() => {
                        self.setState({ redirectToReferrer: true });
                    });
                }
            },
            error: function (xhr) {
                switch (xhr.status) {
                    case 422:
                        if (xhr.responseJSON.result === 'error') {
                            self.refs.formMessages.show(xhr.responseJSON.meta.errors);
                        }
                        break;
                    default:
                        self.refs.formMessages.show('Произошла ошибка.. Попробуйте еще раз!');
                }
            }
        });
    };

    /**
     * Render.
     *
     * @returns {XML}
     */
    render() {
        const { from } = this.props.location.state || { from: { pathname: '/' } };
        const { name, password } = this.state.form;
        const { redirectToReferrer, formErrors={} } = this.state;

        if (redirectToReferrer) {
            return (
                <Redirect to={ from } />
            );
        }

        return (
            <div className='login-form'>

                <Grid
                    textAlign='center'
                    style={{height: '100%'}}
                    verticalAlign='middle'
                >
                    <Grid.Column style={{maxWidth: 450}}>
                        <Header as='h2' color='teal' textAlign='center'>
                            Авторизация
                        </Header>
                        <Form size='large' onSubmit={ this.handleSubmit } >
                            <Segment stacked>
                                <Form.Input
                                    fluid
                                    icon='user'
                                    iconPosition='left'
                                    placeholder='Логин'
                                    name='name'
                                    value={ name }
                                    onChange={ this.handleChange }
                                    error={ 'name' in formErrors && !!formErrors.name }
                                />
                                <Form.Input
                                    fluid
                                    icon='lock'
                                    iconPosition='left'
                                    placeholder='Пароль'
                                    type='password'
                                    name='password'
                                    value={ password }
                                    onChange={ this.handleChange }
                                    error={ 'password' in formErrors && !!formErrors.password }
                                />

                                <Button color='teal' fluid size='large'>Войти</Button>

                                <FormMessages ref="formMessages" parent={ this } />

                            </Segment>
                        </Form>
                        <Message>
                            Еще не зарегистрированы?
                            {' '}<Link to="/auth/register">Регистрация</Link>
                            <br />
                            <Link to="/auth/forgot">Забыли пароль?</Link>
                        </Message>
                    </Grid.Column>
                </Grid>
            </div>
        );
    }
}