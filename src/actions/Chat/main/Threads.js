import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Breadcrumb, Grid, Segment, Card, Menu, Comment} from "semantic-ui-react";
import {CR, Chat} from "components";
import ChatThread from './../ChatThread';
import { Scrollbars } from 'react-custom-scrollbars';

export default class Threads extends ChatThread
{
    
    constructor(props) {
        super(props);
    }
    
    render(){
        const { items, num = 0 } = this.props;
        let self = this;
        
        if (! items.hasOwnProperty('data')) return null;

        return (
            <div className='threads'>
                <Comment.Group size={ 'large' }>
                    <Scrollbars
                        ref='scrollbar'
                        onScroll={ this.handlerOnScroll }
                    >
                    {items.toArray().map(function(data, i2){
                        return (
                            <Comment key={i2} onClick={self.changeActive.bind(self, data)}>
                                <Comment.Avatar src={ data.avatar } />
                                <Comment.Content>
                                    <Comment.Author as='a'>{data.user}</Comment.Author>
                                    <Comment.Metadata>
                                        <div>{ data.subject }</div>
                                        <div>{ data.time }</div>
                                    </Comment.Metadata>
                                    <Comment.Text
                                        dangerouslySetInnerHTML={{
                                            __html: Chat.SmileConverter.htmlFromText(data.body)
                                        }}
                                    />
                                </Comment.Content>
                            </Comment>
                        );
                    })}
                    <p onClick={this.paginate.bind(this, num)}>Показать еще ({items.getRemainder()})</p>
                    </Scrollbars>
                </Comment.Group>
            </div>
        );

    }
}
