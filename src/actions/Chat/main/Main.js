import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Breadcrumb, Grid, Segment, Card, Menu, Icon, Input} from "semantic-ui-react";
import {CR, Chat, Auth} from "components";
import Threads from "./Threads";
import Messages from "./Messages";
import ChatLayout from "./../ChatLayout";
import { Scrollbars } from 'react-custom-scrollbars';


/**
 * Class Main.
 */
export default class Main extends ChatLayout
{

    constructor(props) {
        super(props);
    }

    threads = () => {
        return [
            { num: 0, header: 'Все диалоги', isSearch: true, tabIndex: 1, refName: 'scrollNum0' },
            { num: 1, header: 'Ожидающие', isSearch: true, tabIndex: 2, refName: 'scrollNum1' }
        ];
    };

    getThreads = (item) => {
        const { items, searchOnSubject } = this.state;

        return (
            <Card key={ item.num }>
                <Card.Content>
                    <Card.Header>
                        { item.header }
                        <Input
                            size={ 'mini' }
                            tabIndex={ item.tabIndex }
                            placeholder={ 'Введите тему' }
                            onChange={ this.handleSearchOnSubject }
                            name='subject'
                            loading={ searchOnSubject[item.num].loader }
                            num={ item.num }
                        />
                    </Card.Header>
                        <Threads
                            changeActive={ this.changeActive.bind(this) }
                            items={ items.hasOwnProperty(item.num) ? items[item.num] : []}
                            num={ item.num }
                        />
                  </Card.Content>
            </Card>
        );
    };

    render() {
        const { active = {} } = this.state;
        const { subject = '' } = active;
        let self = this;

        return (
            <div className="container">
                <Card.Group className="chat-container" itemsPerRow={3}>
                    {this.threads().map((item) => {
                        return self.getThreads(item);
                    })}
                    <Card>
                        <Card.Header>
                            Разговор
                            <Input
                                size={ 'mini' }
                                tabIndex={ 3 }
                                onChange={ this.handleSubjectEdit }
                                value={ subject }
                                name='subject'
                            />
                        </Card.Header>
                        <hr/>
                        <Messages active={ active } ref="messages" />
                    </Card>
                </Card.Group>
            </div>
        );
    }
}