import React, { Component } from 'react';
import {Breadcrumb, Grid, Segment, Card, Menu} from "semantic-ui-react";
import {CR, Chat} from "components";
import classNames from "classnames";
import ChatMessages from './../ChatMessages';
import { Scrollbars } from 'react-custom-scrollbars';

export default class Messages extends ChatMessages
{
    constructor(props) {
        super(props);
    }
    
    render(){
        let self = this;
        const { isTypingMessage } = this.state;
        let meta = '';
        let chatClasses = classNames(this.props.className, {
            chat: true,
        });

        if(this.state.messages.hasActive()){
            meta = (<p onClick={this.paginate.bind(this)}>Показать еще ({this.state.messages.getActiveReminder()})</p>);
        }

        return (
            <div className={chatClasses} >

                <div className="messages">
                    <Scrollbars
                        ref='scrollbar'
                        onScroll={ this.handlerOnScroll }
                    >
                        {meta}
                        {this.state.messages.toArray().map(function(data, i){
                            return (
                                <div key={i}>
                                    <div>Сообщение:</div>
                                    <div dangerouslySetInnerHTML={ {
                                        __html: Chat.SmileConverter.htmlFromText(data.body)
                                    }}>
                                    </div>
                                <p>User: {data.name}</p>
                                <p>Data: {data.time}</p>
                                <hr/>
                            </div>);
                        })}
                    </Scrollbars>
                    <div className="typing-message">
                        { isTypingMessage && (
                            <p className="saving">Собеседник набирает сообщение <span>.</span>
                                <span>.</span><span>.</span>
                            </p>
                        )}
                    </div>
                </div>

                <div className="bottom-panel">
                    <div className="message-container">
                        <div className='message-input' onClick={this.focus}>
                            <Chat.Editor
                                editorState={this.state.message}
                                onChange={this.handleChange}
                                keyBindingFn={this.handleKeyPress}
                                plugins={ [Chat.Emoji.Plugin] }
                                ref={(element) => { this.editor = element; }}
                            />
                        </div>
                        <div className="emoji-handle">
                            <Chat.Emoji.Select />
                        </div>
                    </div>
                    <div>
                        <div className='recent-emoji'></div>
                        <button onClick={this.handleSubmit.bind(this)}>Отправить</button>
                    </div>
                </div>
            </div>
        );
    }
}
