//import ChatLayout from './ChatLayout';
//import Thread from './Thread';
//import Messages from './Messages';
//import Caret from './helper/Caret';
import SmileConverter from './helper/SmileConverter';
import Emoji from './helper/Emoji';
import Editor from 'draft-js-plugins-editor';
import Main from './main/Main';
import Widget from './widget/Widget';
import { Socket } from 'components';

export default class Chat
{
	static SmileConverter = new SmileConverter();
    static Emoji = new Emoji();
	static Editor = Editor;
	static Socket = Socket;
	static Main = Main;
	static Widget = Widget;
}
