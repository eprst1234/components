import React from 'react';
import { CR } from 'components';
import Message from './helper/MessageRowable';
import { EditorState, ContentState } from 'draft-js';

/**
 * Class ChatMessages.
 */
export default class ChatMessages extends React.Component {

    constructor(props) {
        super(props);
        let self = this;

        this.actions = {
            messagesGet: function(data) {
                self.setState(function(prv){
                    prv.messages.push(data);
                    return prv;
                });
            },
            messagesPaginate: function(data){
                const { messages } = self.state;

                self.setState(function(prv) {
                    prv.messages.concatActive(data);
                    return prv;
                });

                if (messages.getActiveLength() !== messages.getActive().count) {
                    self.paginateStatus = true;
                    self.scrollToLastPosition();
                }
            },
            messageGet: function(data) {
                self.setState(function(prv){
                    prv.messages.pushMessage(data);
                    return prv;
                });
            },
            messageTypingStatus: function(data) {
                if (typeof data.status !== 'undefined') {
                    self.setState({ isTypingMessage: data.status });
                }
                console.log('messageTypingStatus', data);
            },
        };

        this.state = {
            message: EditorState.createEmpty(),
            messages: new Message(),
            isTypingMessage: false
        };

        this.handleKeyPress = this.handleKeyPress.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.sendTypingStatus = false;
        this.paginateStatus = true;
        this.paginateLastLength = 0;
        this.paginatePrevScroll = {};
    }

    focus = () => {
        this.editor.focus();
    };

    scrollToBottom = () => {
        const { scrollbar } = this.refs;

        setTimeout(() => {
            scrollbar.scrollToBottom();
        }, 100);
    };

    scrollToLastPosition = () => {
        const { scrollbar } = this.refs;

        if (this.paginatePrevScroll.scrollHeight !== scrollbar.getScrollHeight()) {
            let top = scrollbar.getScrollHeight() - this.paginatePrevScroll.scrollHeight -
                this.paginatePrevScroll.scrollTop;
            scrollbar.scrollTop(top);
        }
    };

    clearMessage = () => {
        let message = EditorState.push(
            this.state.message,
            ContentState.createFromText('')
        );

        this.setState({ message: EditorState.moveFocusToEnd(message) });
    };

    handlerOnScroll = () => {
        const { scrollbar } = this.refs;
        if (scrollbar.getScrollTop() <= 150) {
            this.paginate();
        }
    };

    componentWillMount(){
        Object.assign(CR.get('socket').actions, this.actions);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.active.id !== this.props.active.id && this.props.active.id !== 0) {
            this.state.messages.setActive(this.props.active.id);
            this.getMessages();
            this.clearMessage();
            this.scrollToBottom();
        }
    }

    getMessages() {
        console.log('fff');
        console.log(this.props.active);
        if (!this.state.messages.threadExist(this.props.active.id)) {
            CR.get('socket').send('messenger.connectMessage', [this.props.active.id, 0]);
        }
    }

    handleChange(message) {
       this.setState({ message });
    }

    checkIsTypingMessage() {
        let self = this;
        clearTimeout(self.typingTimer);

        if (! self.sendTypingStatus) {
            self.sendTypingStatus = true;
            CR.get('socket').send('messenger.typingStatus', [self.props.active.id], {
                status: true,
            });
        }

        self.typingTimer = setTimeout(() => {
            self.sendTypingStatus = false;
            CR.get('socket').send('messenger.typingStatus', [self.props.active.id], {
                status: false,
            });
        }, 3000);
    }

    handleKeyPress(e) {
        let code = (e.keyCode ? e.keyCode : e.which);

        if (! (code > 1 && code < 48)) {
            this.checkIsTypingMessage();
        }

        if (code === 13 && e.shiftKey) {
            return ;
        } else if (code === 13) {
            console.log('click enter');
            this.handleSubmit();
            return false;
        }
    }

    handleSubmit() {
        const { message } = this.state;
        let content = message.getCurrentContent().getPlainText();
        if (content.length === 0) {
            return;
        }

        CR.get('socket').send(
            'messenger.storeMessage', [this.props.active.id], {
                message: content
            }
        );

        this.clearMessage();
        this.scrollToBottom();
    }

    paginate() {
        const { messages } = this.state;
        const { scrollbar } = this.refs;
        const messagesLength = messages.getActiveLength();

        if (this.paginateStatus
            && (messagesLength !== this.paginateLastLength)
            && messages.getActive().count !== messagesLength
        ) {
            this.paginateStatus = false;
            this.paginateLastLength = messagesLength;
            this.paginatePrevScroll = scrollbar.getValues();

            CR.get('socket').send(
                'messenger.messagePaginate',
                [this.props.active.id, messagesLength]
            );
        }
    }
}
