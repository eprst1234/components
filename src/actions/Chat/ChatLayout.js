import React from 'react';
import { CR, Socket } from 'components';
import Thread from './helper/ThreadRowable';

/**
 * Class ChatLayout.
 */
export default class ChatLayout extends React.Component
{

    constructor(props) {
        super(props);

        let self = this;
        self.doneTypingInterval = 1500;

        let socket = new Socket(function(){
            socket.send('messenger.connect', [0]);
            socket.send('messenger.connect', [1]);
        });

        CR.set(socket, 'socket');

        this.actions = {
            threadsGet: function(data){
                self.setState(function(prv){
                    prv.items[data.num] = new Thread(data.items);
                    return prv;
                });
            },
            threadsSearch: function(data){
                self.setState(function(prv){
                    prv.items[data.num] = new Thread(data.items);
                    return prv;
                });
            },
            threadsPaginate: function(data){
                self.setState(function(prv){
                    prv.items[data.num].concat(data.items);
                    return prv;
                });
            },
            threadGet: function(data){
                self.setState(function(prv){
                    prv.items[data.num].push(data.item);
                    return prv;
                });
            }
        };

        this.state = {
            socket: '',
            active: {
                id: 0,
            },
            items: [],
            searchOnSubject: {
                0: { subject: '', loader: false},
                1: { subject: '', loader: false},
            }
        };
    }

    componentWillMount(){
        Object.assign(CR.get('socket').actions, this.actions);
    }

    handleSearchOnSubject = (e, {num, name, value}) => {
        let self = this;
        let searchOnSubject = { ...self.state.searchOnSubject };
        searchOnSubject[num][name] = value;
        searchOnSubject[num]['loader'] = true;

        self.setState({ searchOnSubject });
        clearTimeout(self.typingTimerSearch);

        self.typingTimerSearch = setTimeout(() => {
            CR.get('socket').send('messenger.connect', [num], {
                search: self.state.searchOnSubject[num]['subject']
            });

            let searchOnSubject = { ...self.state.searchOnSubject };
            searchOnSubject[num]['loader'] = false;
            self.setState({ searchOnSubject });
        }, self.doneTypingInterval);
    };

    handleSubjectEdit = (e) => {
        const { active } = this.state;
        let self = this;

        active.subject = e.target.value;
        self.setState({ active })
        clearTimeout(self.typingTimerSubject);

        self.typingTimerSubject = setTimeout(() => {
            CR.get('socket').send('messenger.putSubject', [ active.id ], {
                subject: self.state.active.subject,
            });
            self.setState({ searchOnSubjectStatus: false });
        }, self.doneTypingInterval);
    };

    changeActive(thread) {
        this.setState({active: thread});
    }
    
    render() {
        return (
            <div>

            </div>
        )
    }
}
