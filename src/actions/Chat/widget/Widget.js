import React from "react";
import classNames from 'classnames';
import { Button, Comment, Icon, Image, Menu } from 'semantic-ui-react';
import Threads from './Threads';
import Messages from './Messages';
import ChatLayout from './../ChatLayout';

export default class Widget extends ChatLayout
{
	
	constructor(props) {
        super(props);
	}

	handleContacts() {
        this.setState({backToContacts: true, showDialogList: false})
    }

    handleChatShows() {
        this.setState({backToContacts: false, showDialogList: true})
    }

    handleDialog() {
        this.setState({showDialog: !this.state.showDialog})
    }

	changeActive(data){
		super.changeActive(data);
		this.setState({showDialog: !this.state.showDialog});
	}
	
	render() {
        let self = this;
        const { items } = this.state;

        let mainContainerClasses = classNames(this.props.className, {
            'ui': true,
            'chatcontainer': true,
            'dialoglist': this.state.showDialogList,
            'dialog': this.state.showDialog,
        });

        return (
            <div className={mainContainerClasses}>
                <div className="ui chatbox">
                    <div className="ui chatroom contacts">
                        <div className="actions">
                            <div className="message">
                                Контакты
                            </div>
                            <Button.Group color='black' size='mini' floated='right'>
                                <Button icon='font'/>
                                <Button icon='bold'/>
                            </Button.Group>
                        </div>
                        <div className="room">
                            <Comment.Group>
                                {/*
                                    this.state.tmpData.contactList.map(function (contact, i) {
                                        return (
                                            <Comment key={i} onClick={self.handleDialog.bind(self)}>
                                                <Comment.Avatar src={contact.avatarSrc}/>
                                                <Comment.Content>
                                                    <Comment.Author as='a'>{contact.name}</Comment.Author>
                                                    <Comment.Text>{contact.time}</Comment.Text>
                                                </Comment.Content>
                                            </Comment>
                                        );
                                    })
                                */}
                            </Comment.Group>
                        </div>
                    </div>
					
                    <Threads
                        changeActive={this.changeActive.bind(this)} 
                        items={ items.hasOwnProperty('0') ? items[0] : []}
                        num={0}
                    />

					<Messages changeActive={this.changeActive.bind(this)} active={this.state.active}/>
					
                    <div className="ui boxmenu">
                        <Menu secondary fluid widths={3} icon='labeled'>
                            <Menu.Item name='contacts' active={!this.state.showDialogList} onClick={this.handleContacts.bind(this)}>
                                <Icon name='users'/>
                                Контакты
                            </Menu.Item>
                            <Menu.Item name='chatshows' active={this.state.showDialogList} onClick={this.handleChatShows.bind(this)}>
                                <Icon name='comments'/>
                                Чаты
                            </Menu.Item>
                            <Menu.Item name='settings'>
                                <Icon name='settings'/>
                                Настройки
                            </Menu.Item>
                        </Menu>
                    </div>
                </div>
            </div>
        )
    }
}