import React from 'react';
import {Button, Comment, Icon, Image, Menu} from "semantic-ui-react";
import {CR, Chat} from "components";
import classNames from "classnames";
import ChatMessages from './../ChatMessages';
import { Scrollbars } from 'react-custom-scrollbars';

export default class Messages extends ChatMessages
{
    
    constructor(props) {
        super(props);
    }

    render() {
        let self = this;
        const { isTypingMessage } = this.state;
        let chatroomDialogClasses = classNames(this.props.className, {
            'ui': true,
            'chatroom': true,
            'dialog': true,
            'emoji': this.state.showEmoji,
        });

        let meta = '';
        if(this.state.messages.hasActive()){
            meta = (<p onClick={this.paginate.bind(this)}>Показать еще ({this.state.messages.getActiveReminder()})</p>);
        }

        return(
            <div className={chatroomDialogClasses}>
                <div className="actions">
                    <div className="back" onClick={ () => this.props.changeActive({id: 0}) }>
                        <Icon size="mini" name="angle left"/>
                        {'Чаты'}
                    </div>
                    <div className="title">
                        <div className="header">{this.props.active.subject}</div>
                        <div className="subheader">Status: I'am the Terminator, and I'll be back</div>
                    </div>
                    <div className="ui right floated avatar">
                        <Image mini="true" src="/images/avatar/small/terminator.png"/>
                    </div>
                </div>
                <div className="room">
                    <Comment.Group>
                        <Scrollbars
                            ref='scrollbar'
                            onScroll={ this.handlerOnScroll }
                        >
                        {meta}
                        {
                            this.state.messages.toArray().map(function(data, i) {
                                let messageClass = data.my == 1 ? 'my' : 'interlocutor';
                                return (
                                    <Comment key={i} className={messageClass}>
                                        <Comment.Metadata>
                                            <Comment.Author as='a'>{data.name}</Comment.Author>
                                            <div>{data.time}</div>
                                        </Comment.Metadata>
                                        <Comment.Content>
                                            <Comment.Text
                                                dangerouslySetInnerHTML={{
                                                    __html: Chat.SmileConverter.htmlFromText(data.body)
                                                }}
                                            />
                                        </Comment.Content>
                                    </Comment>
                                );
                            })
                        }
                        </Scrollbars>
                    </Comment.Group>
                    <div className="typing-message">
                        { isTypingMessage && (
                            <p className="saving">Собеседник набирает сообщение <span>.</span>
                                <span>.</span><span>.</span>
                            </p>
                        )}
                    </div>
                </div>
                <div className="talk">
                    <div className='message-input' onClick={this.focus}>
                        <Chat.Editor
                            editorState={this.state.message}
                            onChange={this.handleChange}
                            keyBindingFn={this.handleKeyPress}
                            plugins={ [Chat.Emoji.Plugin] }
                            ref={(element) => { this.editor = element; }}
                        />
                    </div>
                    <div className="emoji-handle">
                        <Chat.Emoji.Select />
                    </div>
                    <div className="ui send button" onClick={this.handleSubmit}>
                        <i className="ui icon comment"></i>
                    </div>
                </div>
            </div>
        );
    }
    
    
    
    /*render(){
        var meta = '';
        if(this.state.messages.hasActive()){
            meta = (<p onClick={this.paginate.bind(this)}>Показать еще ({this.state.messages.getActiveReminder()})</p>);
        }
        return (
            <div>
                {meta}
                {this.state.messages.toArray().map(function(data){
                    return (<div>
                        <p>Сообщение: {data.body}</p>
                        <p>User: {data.name}</p>
                        <p>Data: {data.time}</p>
                        <hr/>
                    </div>);
                })}
                <textarea onChange={this.handleChange.bind(this)} value={this.state.message}/>
                <button onClick={this.handleSubmit.bind(this)}>Отправить</button>
            </div>
        );
    }*/
}
