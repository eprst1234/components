import React from 'react';
import PropTypes from 'prop-types';
import { Button, Comment, Icon, Image, Menu } from "semantic-ui-react";
import { CR, Chat } from "components";
import ChatThread from './../ChatThread';
import { Scrollbars } from 'react-custom-scrollbars';

export default class Threads extends ChatThread
{
    
    constructor(props) {
        super(props);
    }
    
    //changeActive(id){
    //  super.changeActive(id);
    //  this.setState({showDialog: this.state.showDialog ? false : true});
    //}
    
    render() {
        const { items } = this.props;
        let self = this;

        if (! items.hasOwnProperty('data')) return null;

        return(
            <div className="ui chatroom dialoglist">
                <div className="actions">
                    <div className="message">
                        Чаты
                    </div>
                    <Button.Group color='black' size='mini' floated='right'>
                        <Button icon='italic'/>
                        <Button icon='underline'/>
                    </Button.Group>
                </div>
                <div className="room">
                    <div className="container">
                        <Comment.Group>
                            <Scrollbars
                                ref='scrollbar'
                                onScroll={ this.handlerOnScroll }
                            >
                            {
                                items.toArray().map(function (data, i) {
                                    return (
                                        <Comment key={i} onClick={self.changeActive.bind(self, data)}>
                                            <Comment.Avatar src={ data.avatar } />
                                            <Comment.Content>
                                                <Comment.Author as='a'>{data.user}</Comment.Author>
                                                <Comment.Metadata>
                                                    <div>{data.time}</div>
                                                </Comment.Metadata>
                                                <Comment.Text
                                                    dangerouslySetInnerHTML={{
                                                        __html: Chat.SmileConverter.htmlFromText(data.body)
                                                    }}
                                                />
                                            </Comment.Content>
                                        </Comment>
                                    );
                                })
                            }
                            </Scrollbars>
                        </Comment.Group>
                    </div>
                </div>
            </div>
        )
    }
    
    /*render(){
        var self = this;
        var rows = [];
        for(var i = 0; i < this.props.rows; i++){
            rows.push(<div key={i} className="container">
                {this.state.items[i].toArray().map(function(data, i2){
                    return (<div key={i2} onClick={self.changeActive.bind(self, data.id)}>
                        <p>Тема: {data.subject}</p>
                        <p>Сообщение: {data.body}</p>
                        <p>User: {data.user}</p>
                        <p>Data: {data.time}</p>
                        <hr/>
                    </div>);
                })}
                <p onClick={this.paginate.bind(this, i)}>Показать еще ({this.state.items[i].getRemainder()})</p>
            </div>);
        }
        
        return (
            <div>
                {rows}
            </div>
        );
    }*/
}
