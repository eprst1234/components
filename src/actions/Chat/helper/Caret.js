/**
 * Class Caret.
 */
export default class Caret
{
    /**
     * Create range recursively.
     *
     * @param node
     * @param chars
     * @param range
     * @returns {*}
     */
    createRange(node, chars, range) {
        if (!range) {
            range = document.createRange();
            range.selectNode(node);
            range.setStart(node, 0);
        }

        if (chars.count === 0) {
            range.setEnd(node, chars.count);
        } else if (node && chars.count > 0) {
            if (node.nodeType === Node.TEXT_NODE) {
                if (node.textContent.length < chars.count) {
                    chars.count -= node.textContent.length;
                } else {
                    range.setEnd(node, chars.count);
                    chars.count = 0;
                }
            } else {
                for (let lp = 0; lp < node.childNodes.length; lp++) {
                    range = this.createRange(node.childNodes[lp], chars, range);

                    if (chars.count === 0) {
                        break;
                    }
                }
            }
        }

        return range;
    }

    /**
     * Check.
     *
     * @param node
     * @param parentId
     * @returns {boolean}
     */
    isChildOf(node, parentId) {
        while (node !== null) {
            if (node.id === parentId) {
                return true;
            }
            node = node.parentNode;
        }

        return false;
    }

    isElement(element) {
        return (
            typeof HTMLElement === "object" ? element instanceof HTMLElement :
                element && typeof element === "object" && element !== null &&
                element.nodeType === 1 && typeof element.nodeName === "string"
        );
    }

    /**
     * Get caret position.
     *
     * @param parentId
     * @returns {number}
     */
    getPosition(parentId) {
        let selection = window.getSelection(),
            charCount = -1,
            node;

        if (selection.focusNode) {
            if (this.isChildOf(selection.focusNode, parentId)) {
                node = selection.focusNode;
                charCount = selection.focusOffset;

                while (node) {
                    if (node.id === parentId) {
                        break;
                    }

                    if (node.previousSibling) {
                        node = node.previousSibling;
                        charCount += node.textContent.length;
                    } else {
                        node = node.parentNode;
                        if (node === null) {
                            break
                        }
                    }
                }
            }
        }

        return charCount;
    }

    /**
     * Insert text to position.
     *
     * @param elementId
     * @param position
     * @param text
     */
    insertTextToPosition(elementId, position, text) {
        this.setPosition(position, elementId);
        let doc = new DOMParser().parseFromString(text, 'text/html');
        let node = doc.body.firstChild;

        if (window.getSelection) {
            let sel = window.getSelection();
            if (sel.getRangeAt && sel.rangeCount) {
                let range = sel.getRangeAt(0);
                range.deleteContents();
                range.insertNode(node);
            }
        } else if (document.selection && document.selection.createRange) {
            document.selection.createRange().text = text;
        }

        //this.setPosition(2, node);
    }

    /**
     * Set caret.
     *
     * @param chars
     * @param parentId
     */
    setPosition(chars, parentId) {
        if (chars >= 0) {
            let selection = window.getSelection();
            let parentNode;

            if (this.isElement(parentId)) {
                parentNode = parentId;
            } else {
                parentNode = document.getElementById(parentId).parentNode;
            }

            let range = this.createRange(
                parentNode, { count: chars }
            );

            if (range) {
                range.collapse(false);
                selection.removeAllRanges();
                selection.addRange(range);
            }
        }
    }

    /**
     * Set caret to end.
     *
     * @param el
     */
    placeCaretAtEnd(el) {
        el.focus();
        if (typeof window.getSelection != 'undefined'
            && typeof document.createRange != 'undefined')
        {
            let range = document.createRange();
            range.selectNodeContents(el);
            range.collapse(false);
            let sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
        } else if (typeof document.body.createTextRange != 'undefined') {
            let textRange = document.body.createTextRange();
            textRange.moveToElementText(el);
            textRange.collapse(false);
            textRange.select();
        }
    }
}