import {Arr} from "components";

export default class MessageRowable{
	
	constructor(data = []){
       this.data = data;
	   this.active = 0;
    }
	
	setActive(id){
		this.active = id;
	}
	
	getIndex(id = false){
		return Arr.propIndexOf(this.data, id === false ? this.active : id, 'thread_id');
	}
	
	threadExist(){
		if(this.getIndex(this.active) === -1)
			return false;
		else
			return true;
	}
	
	push(item){
		if(!this.threadExist(item.thread_id)){
			this.data.push(item);
		}
	}
	
	pushMessage(item){
		var index = this.getIndex(item.item.thread_id);
		this.data[index].items.push(item.item);
		this.data[index].count++;
        this.data[index].page++;
	}
	
	concatActive(data){
		this.getActive().items = this.getActive().items.concat(data.items);
	}
	
	getActive(){
		return this.data[this.getIndex(this.active)];
	}
	
	getActiveLength(){
		return this.getActive().items.length;
	}
	
	getActiveReminder(){
		return this.getActive().count - this.getActiveLength();
	}
	
	hasActive(){
		if(typeof this.getActive() !== 'undefined')
			return true;
		else
			return false;
	}
	
	toArray(){
		if(this.hasActive()){
			this.getActive().items = Arr.sort(this.getActive().items, 'id');
			return this.getActive().items;
		}	
		return [];
	}
	
}
