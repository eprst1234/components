//import React from 'react';
//import { Emoji } from 'emoji-mart';
//import ReactDOM from 'react-dom';
//import Caret from './Caret';
//
//export default class MessageInput extends React.Component {
//
//    constructor(props) {
//        super(props);
//
//        this.emitChange = this.emitChange.bind(this);
//        this.elementId = 'messageInput';
//        this.Caret = new Caret();
//    }
//
//    shouldComponentUpdate(nextProps) {
//        return nextProps.html !== ReactDOM.findDOMNode(this).innerHTML;
//    }
//
//    componentDidUpdate() {
//        if (this.props.html !== ReactDOM.findDOMNode(this).innerHTML) {
//           ReactDOM.findDOMNode(this).innerHTML = this.props.html;
//        }
//    }
//
//    emitChange() {
//        let html = ReactDOM.findDOMNode(this).innerHTML;
//        if (this.props.onChange) {
//            this.props.onChange({
//                target: {
//                    value: html,
//                    lastCaretPosition: this.Caret.getPosition(this.elementId),
//                }
//            });
//        }
//    }
//
//
//
//    render() {
//        const { html = '', onKeyDown, ...rest } = this.props;
//        return (
//            <div
//                id={ this.elementId }
//                onInput={ this.emitChange }
//                onBlur={ this.emitChange }
//                onClick={ this.emitChange }
//                onKeyDown={ (e) => {
//                    this.emitChange();
//                    onKeyDown(e);
//                }}
//                contentEditable
//                dangerouslySetInnerHTML={ {__html: html } }
//                { ...rest }
//            >
//                { this.props.children }
//            </div>
//        )
//    }
//}
