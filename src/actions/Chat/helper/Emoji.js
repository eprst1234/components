import React from 'react';
import createEmojiPlugin from 'draft-js-emoji-plugin';

export default class Emoji {

    constructor() {
        this.Plugin = createEmojiPlugin({
            imagePath: `/images/emoji/emojione/png/`,
            imageType: 'png',
            selectButtonContent: <img src="/images/icons/emoji.png" />,
            allowImageCache: true,
            selectGroups: [
                {
                    title: 'Люди',
                    icon: '😀',
                    categories: ['people'],
                },
                {
                    title: 'Природа',
                    icon: '🐵',
                    categories: ['nature'],
                },
                {
                    title: 'Символы',
                    icon: '❤',
                    categories: ['symbols'],
                },
                {
                    title: 'Объекты',
                    icon: '☠️',
                    categories: ['objects'],
                }
            ]
        });

        this.Select = this.Plugin.EmojiSelect;
    }
}