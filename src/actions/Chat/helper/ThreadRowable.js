import {Arr} from "components";

export default class ThreadRowable{
	
	constructor(data){
       this.data = data;
    }
	
	push(item){
		var index = Arr.propIndexOf(this.data.data, item.id, 'id');
		if(index !== -1){
			this.data.data[index] = item;
		}else{
			this.data.data.push(item);
			this.data.count++;
		}
	}
	
	concat(items){
		this.data.data = this.data.data.concat(items.data);
	}
	
	toArray(){
		return this.data.data;
	}
	
	getLength(){
		return this.data.data.length;
	}
	
	getRemainder(){
		return this.data.count - this.getLength();
	}
}
