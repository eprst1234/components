import React from "react";
import {CR} from "components";

/**
 * Class ChatThread.
 */
export default class ChatThread extends React.Component {

    constructor(props) {
        super(props);
        
        this.state = {
            active: 0,
        };

        this.paginateStatus = true;
        this.paginateLastLength = 0;
    }

    componentWillReceiveProps(nextProps) {
        if (typeof nextProps.items.data !== 'undefined') {
            if (nextProps.items.getLength() !== nextProps.items.data.count) {
                this.paginateStatus = true;
            }
        }
    }

    handlerOnScroll = () => {
        const { scrollbar } = this.refs;
        if (scrollbar.getValues().top >= 0.7) {
            this.paginate(this.props.num);
        }
    };

    changeActive(id) {
        this.setState({active: id});
        this.props.changeActive(id);
    }
    
    paginate(num) {
        const { items } = this.props;
        const itemsLength = items.getLength();

        if (this.paginateStatus
            && (itemsLength !== this.paginateLastLength)
            && items.data.count !== itemsLength
        ) {
            this.paginateStatus = false;
            this.paginateLastLength = itemsLength;

            CR.get('socket').send('messenger.paginate', [num], {
                page: itemsLength
            });
        }
    }
}
