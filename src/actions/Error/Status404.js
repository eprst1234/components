import React from "react";
import { Button } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

/**
 * Class Status404.
 */
export class Status404 extends React.Component {
    /**
     * Render.
     *
     * @returns {XML}
     */
    render() {
        return (
            <div className="wrap">
                <div className="logo">
                    <p>Ой! - Страницу не найдено!</p>
                    <img src="/css/themes/default/assets/images/404-1.png" />

                    <div className="sub">
                        <Button as={ Link } color='black' to="/">На главную</Button>
                    </div>
                </div>
            </div>
        );
    }
}