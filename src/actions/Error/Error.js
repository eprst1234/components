import { Status404 } from './Status404';

/**
 * Class Auth.
 */
export default class Error
{
    /**
     * Page 404.
     *
     * @type {Status404}
     */
    static Status404 = Status404;
}
