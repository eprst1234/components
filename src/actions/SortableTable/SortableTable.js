import React from "react";
import { Api, BasePagination, BaseHeader, Button as HeaderButton } from "components";
import { Link } from "react-router-dom";
import { SortableContainer, SortableElement, SortableHandle, arrayMove } from 'react-sortable-hoc';
import { List, Icon, Popup, Segment, Button, Checkbox, Table, Message } from 'semantic-ui-react';
import { SortableBody } from './SortableBody';
import { SortableDragHandle } from './SortableDragHandle';
import { SortableRow } from './SortableRow';
import PropTypes from 'prop-types';

/**
 * Class SortableTable.
 */
export default class SortableTable extends React.Component
{
    static Body = SortableBody;
    static Row = SortableRow;
    static DragHandle = SortableDragHandle;

    static propTypes = {
        header: PropTypes.array,
        availableFields: PropTypes.array,
        items: PropTypes.array,

        allowSorting: PropTypes.bool,
        allowSelecting: PropTypes.bool,
        saveSorted: PropTypes.bool,
    };

    static defaultProps = {
        parent: this,
        header: [],
        availableFields: [],
        items: [],
        allowSorting: true,
        allowSelecting: false,
        saveSorted: false,
    };
    
    sortedElements = {};

    constructor(props) {
        super(props);

        this.state = {
            items: this.props.items,
            selected: [],
        }
    }

    onSortEnd = ({oldIndex, newIndex}) => {
        let items = { ...this.state.items };
        this.sortedElements[items[oldIndex]['id']] = items[newIndex]['order'];
        this.sortedElements[items[newIndex]['id']] = items[oldIndex]['order'];
        items = arrayMove(this.state.items, oldIndex, newIndex);
        this.setState({ items });

        //if (this.props.saveSorted) {
            this.handleSave();
        //}
    };

    handleSave() {
        let j = 0;
        let elements = this.sortedElements;
        for (let i in elements) {
            let hide = j++ % 2 === 0;

            Api.putArg(this.props.parent.target, 'update', i, {
                order: this.sortedElements[i]
            })
                .call()
                .withNotify('Елемент успешно перемещен!', 'success', hide)
                .withNotify('Не удалось переместить елемент!', 'error', hide);
        }

        //this.sortedElements = {};
    }

    /**
     * Render.
     *
     * @returns {XML}
     */
    render() {
        const { header, parent, items: itemsProps, ...rest } = this.props;
        const bodyItems = this.props.allowSelecting ? this.props.items : this.state.items;

        return (
            <Table celled selectable>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell style={{ width: '1px' }}/>
                        { header.map((item, key) => {
                            return (
                                <Table.HeaderCell
                                    textAlign={ 'action' in item && item.action ? 'right' : 'left' }
                                    key={ key }
                                >
                                    { item.content }
                                </Table.HeaderCell>
                            );
                        })}
                    </Table.Row>
                </Table.Header>

                <SortableBody
                    onConfirmRemove={this.props.onConfirmRemove.bind(this)}
                    parent={ parent }
                    items={ bodyItems }
                    onSortEnd={ this.onSortEnd }
                    useDragHandle={ true }
                    { ...rest }
                />
            </Table>
        );
    }
}


