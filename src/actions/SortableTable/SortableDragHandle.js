import React from "react";
import { Api, BasePagination, BaseHeader, Button as HeaderButton } from "components";
import { Link } from "react-router-dom";
import { SortableContainer, SortableElement, SortableHandle, arrayMove } from 'react-sortable-hoc';
import { List, Icon, Popup, Segment, Button, Checkbox, Table, Message } from 'semantic-ui-react';

/**
 * Class SortableDragHandle.
 */
@SortableHandle
export class SortableDragHandle extends React.Component
{
    constructor(props) {
        super(props);
    }

    /**
     * Render.
     *
     * @returns {XML}
     */
    render() {
        return (
            <Icon name="sort" className="sortable-icon"/>
        );
    }
}

