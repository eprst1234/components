import React from "react";
import { Api, BasePagination, BaseHeader, Button as HeaderButton } from "components";
import { Link } from "react-router-dom";
import { SortableContainer, SortableElement, SortableHandle, arrayMove } from 'react-sortable-hoc';
import { List, Icon, Popup, Segment, Button, Checkbox, Table, Message } from 'semantic-ui-react';
import { SortableRow } from './SortableRow';

/**
 * Class SortableBody.
 */
@SortableContainer
export class SortableBody extends React.Component
{
    constructor(props) {
        super(props);
    }

    /**
     * Render.
     *
     * @returns {XML}
     */
    render() {
        const { items, parent = {}, ...rest } = this.props;

        return (
            <Table.Body>
                {items.map((value, index) => {
                    return (
                        <SortableRow
                            onConfirmRemove={this.props.onConfirmRemove.bind(this)}
                            key={`item-${index}`}
                            index={index}
                            item={value}
                            parent={parent}
                            { ...rest }
                        />
                    );
                })}
            </Table.Body>
        );
    }
}

