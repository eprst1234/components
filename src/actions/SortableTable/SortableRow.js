import React from "react";
import { Api, BasePagination, BaseHeader, Button as HeaderButton } from "components";
import { Link } from "react-router-dom";
import { SortableContainer, SortableElement, SortableHandle, arrayMove } from 'react-sortable-hoc';
import { List, Icon, Popup, Segment, Button, Checkbox, Table, Message } from 'semantic-ui-react';
import { SortableDragHandle } from './SortableDragHandle';

/**
 * Class SortableRow.
 */
@SortableElement
export class SortableRow extends React.Component
{
    constructor(props) {
        super(props);
    }

    putDelete(e) {
        this.props.onConfirmRemove(e)
    }
    /**
     * Render.
     *
     * @returns {XML}
     */
    render() {
        const { item, parent, allowSelecting, allowSorting } = this.props;

        return (
            <Table.Row>
                <Table.Cell>
                    { allowSelecting && (
                        <Checkbox
                            onChange={ parent.selectItem.bind(parent, item.id) }
                            checked={ parent.state.selected.indexOf(item.id) !== -1 }
                        />
                    )}
                    { allowSorting && (
                        <SortableDragHandle />
                    )}
                </Table.Cell>
                <Table.Cell>{ item.title }</Table.Cell>
                <Table.Cell>
                    { item.content.substring(0, 100) }
                </Table.Cell>
                <Table.Cell className='actions' textAlign='right'>
                    <Popup
                        trigger={
                            <Button
                                className='small'
                                as={ Link }
                                to={ this.props.parent.props.location.pathname+'/' + item.id + '/edit' }
                                primary
                            >
                                <Icon name="edit"/>
                            </Button>
                        }
                        content='Edit this city'
                    />
                    <HeaderButton.Remove
                        title='Удалить выбранный название продукта'
                        onConfirm={ this.putDelete.bind(this,item.id) }
                    />
                </Table.Cell>
            </Table.Row>
        );
    }
}
