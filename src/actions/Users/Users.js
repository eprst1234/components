import React from 'react'
import { Api, Button as HeaderButton } from 'components';
import { Table, Popup, Button, Icon } from 'semantic-ui-react';
import { Link } from "react-router-dom";
import BaseTable from './../../base/BaseTable';
import UserCreate from './UserCreate';
import UserEdit from './UserEdit';
import UserShow from './UserShow';


export default class Users extends BaseTable {

    /**
     * Добавление пользователя.
     *
     * @type {UserCreate}
     */
    static Create = UserCreate;

    /**
     * Подробный просмотр.
     *
     * @type {UserView}
     */
    static Edit = UserEdit;


    /**
     * Подробный просмотр.
     *
     * @type {UserView}
     */
    static Show = UserShow;


    constructor(props) {
        super(props);
        this.defaultParamsUsers = {
			ceils: [
				{name: 'Позывной сотрудника', field: 'nickname'},
				{name: 'Баланс', field: 'credits'},
				{name: 'Онлайн', field: this.getOnlineField},
				{name: 'Города', field: this.getCitiesField},
				{name: 'Создан', field: 'created_at'},
				{name: 'Действия', field: this.getActions.bind(this)},
			],
		};
		this.state = {
            items: {data: []},
        };
    }

	blocked(id) {
		Api.putArg(this.params.urls.control, 'blocked', id).call(()=>this.getData()).withNotify();
	}

	getQuery(request){
		request.with('cities');
	}

	getOnlineField(data){
		return (<div>{data.online === true ? 'Да' : 'Последний раз был:' + data.online}</div>);
	}

	getCitiesField(data){
		if(data.cities.length === 0)
			return <span>Нет городов</span>;

		return (
		    <div>
                {data.cities.map((data2, key) => (<p key={key}>{data2.name}</p>))}
            </div>
        );
	}

	getActions(data) {
        return (
            <div className='actions'>
                { this.buttonView({ to: this.type + '/' + data.id }) }
                <HeaderButton.Default
                    onClick={ this.blocked.bind(this, data.id) }
                    title={ data.blocked_at == null ? 'Заблокировать' : 'Розблокировать' }
                    icon={ data.blocked_at == null ? 'lock' : 'unlock' }
                />
                { this.getButtonEdit(this.type + '/' + data.id + '/edit') }
                { this.getButtonDelete(data) }
            </div>
        )
	}
}
