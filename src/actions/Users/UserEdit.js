import React from 'react'
import {Api, BaseDropdown} from "components";
import { Segment, Container, Header, Form, Input, Button, Icon, Grid, Checkbox } from 'semantic-ui-react';
import UserForm from "./helpers/UserForm";

export default class UserEdit extends UserForm
{
    constructor(props){
        super(props);
    }

    handleCheckChangePass() {
        this.setState((prv) => {
            prv.form.changePassword = !prv.form.changePassword;
            return prv;
        });
    }

	getPasswordField() {
		const { changePassword } = this.state.form;

		return (
            <div>
                <Form.Field>
                    <Checkbox
                        toggle
                        label={ { children: 'Сменить пароль?' } }
                        checked={ changePassword }
                        onChange={ this.handleCheckChangePass.bind(this) }
                    />
                </Form.Field>
                { super.getPasswordField({ disabled: !changePassword }) }
            </div>
        );
    }
};