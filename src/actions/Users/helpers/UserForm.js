import React from 'react'
import {Api, BaseDropdown, Button as BaseButton} from "components";
import BaseForm from "./../../../base/BaseForm";
import { Segment, Container, Header, Form, Input, Button, Icon, Grid } from 'semantic-ui-react';

export default class UserForm extends BaseForm
{
    constructor(props){
        super(props);
        this.state = {
            form: {},
			cities: [],
            inputPasswordType: 'password',
            inputPasswordIcon: 'unhide'
        };

        Api.get('city', 'index').all().bind(this, 'cities');
    }

    getEditData() {
        let self = this;

        Api.get(this.getParams().uri, 'index')
            .with('cities')
            .where('id', this.getParams().id)
            .first((r) => {
                if (r.data.cities !== 'undefined') {
                    let cities = r.data.cities.map((i) => { return i.id });
                    self.setState({ form: { ...this.state.form, ['cities']: cities } });
                }
            })
            .bind(this, 'form');
    }

    handleShowPassword = () => {
        this.setState({
            inputPasswordType: this.state.inputPasswordType === 'password' ? 'text' : 'password',
            inputPasswordIcon: this.state.inputPasswordIcon === 'unhide' ? 'hide' : 'unhide',
        });
    };

    handleGeneratePassword = ({ password }) => {
        this.setState({ form: { ...this.state.form, [ 'password' ]: password } });
    };

    getLoginField() {
        return this.getField({
            type: 'input',
            name: 'name',
            placeholder: 'Логин',
            label: 'Введите логин',
        });
    }

    getNicknameField() {
        return this.getField({
            type: 'input',
            name: 'nickname',
            placeholder: 'Позывной',
            label: 'Введите позывной',
        });
    }

	getCitiesAddField() {
        const {form, cities, errors = {}} = this.state;
        const userCities = typeof form.cities !== 'undefined' && Number.isInteger(form.cities[0]) ? form.cities : [];

        return (
            <Form.Field error={ 'cities' in errors }>
                <label>Выберите города</label>
                <BaseDropdown
                    fluid={ true }
                    multiple={ true }
                    onChange={ this.handleChangeDropdown.bind(this) }
                    placeholder={ 'Выберите город' }
                    name={ 'cities' }
                    value={ userCities }
                    options={ { data: cities, value: 'id', text: 'name' } }
                />
            </Form.Field>
        );
	}

    getPasswordField(params = {}) {
        const { form, inputPasswordIcon, inputPasswordType, errors = {} } = this.state;
        const { disabled = false } = params;

        return (
            <Form.Field
                inline
                disabled={ disabled }
                error={ 'password' in errors }
            >
                <label>Введите пароль</label>
                <Input
                    type={ inputPasswordType }
                    value={ typeof form.password !== 'undefined' ? form.password : '' }
                    disabled={ disabled }
                    name='password'
                    onChange={ this.handleChange.bind(this) }
                    placeholder='пароль'
                    action
                >
                    <input/>
                    <BaseButton.Default
                        title={ inputPasswordIcon === 'unhide' ? 'Показать ' +
                            'пароль' : 'Скрыть пароль' }
                        onClick={ this.handleShowPassword }
                        icon={ inputPasswordIcon }
                        type={ 'button' }
                        background={ 'teal' }
                        className={ 'copy' }
                        style={ { padding: '0 12px' } }
                    />
                    <BaseButton.Copy
                        title={ 'Скопировать пароль' }
                        titleSuccess={ 'Пароль успешно скописовано!' }
                        copyText={ form.password }
                    />
                    <BaseButton.GeneratePassword
                        disabled={ disabled }
                        onGeneratePassword={ this.handleGeneratePassword }
                    />
                </Input>
            </Form.Field>
        );
    }

	getFields(){
		return (
			<div>
                { this.getLoginField() }
                { this.getNicknameField() }
                { this.getPasswordField() }
                { this.getCitiesAddField() }
            </div>
		)
	}
};