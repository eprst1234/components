import DropZone from './../../base/DropZone';
import React from 'react';
import { ImagesUploadPreview } from './ImagesUploadPreview';

/**
 * Class ImagesUpload.
 */
export default class ImagesUpload extends React.Component
{
    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            images: [],
        };

        this.finishReadFile = this.finishReadFile.bind(this);
    }

    static defaultProps = {
        multi: true,
        // allowType:
    };

    static Preview = ImagesUploadPreview;

    getImages() {
        if (this.props.multi) {
            return this.state.images;
        } else {
            return this.state.images[0];
        }
    }

    finishReadFile(file) {
        let self = this;

        if (this.props.multi) {
            this.setState(
                function (prv) {
                    prv.images.push(self.prepareItem(file));
                }
            );
        } else {
            this.setState({ images: [self.prepareItem(file)] });
        }

        this.props.onSuccess();
    }

    prepareItem(image) {
        return {
            content: image.content,
            name: image.name,
            type: image.type,
            size: image.size,
        };
    }

    render() {
        return (
            <DropZone finishRead={this.finishReadFile} />
        )
    }
};
