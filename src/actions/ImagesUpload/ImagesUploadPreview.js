import React from "react";
import PropTypes from 'prop-types';
import { Image } from 'semantic-ui-react';

/**
 * Class ImagesUploadPreview.
 */
export class ImagesUploadPreview extends React.Component {
    /**
     * Constructor.
     *
     * @param props
     */
    constructor(props){
        super(props);

        this.state = {
            images: this.props.images,
        };
    }

    static propTypes = {
        images: PropTypes.oneOfType([
            PropTypes.array,
            PropTypes.object
        ]),
        removeButton: PropTypes.bool,
        showPhotoName: PropTypes.bool,
    };

    static defaultProps = {
        images: [],
        removeButton: true,
        showPhotoName: false,
    };

    handleDelete(key) {
        let images = {...this.state.images};
        delete images[key];
        this.setState({ images });
    };

    render() {
        let self = this;
        const { images } = self.state;
        const { removeButton, showPhotoName } = self.props;

        if (! Array.isArray(images) || images.length == 0) {
            return null;
        }

        return (
            <div>
                {images.map(function(data, key) {
                    return (
                        <div key={key}>
                            {showPhotoName &&
                                <p>{data.name}</p>
                            }
                            <Image src={data.content} />
                            {removeButton &&
                                <button onClick={self.handleDelete.bind(self, key)}>Удалить</button>
                            }
                        </div>
                    );
                })}
            </div>
        );
    }

}