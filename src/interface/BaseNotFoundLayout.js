import React, {Component} from "react";

export default class BaseNotFoundLayout extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="not-found">
                { this.props.children && React.cloneElement(this.props.children, {parent: this}) }
            </div>
        );
    }

};
