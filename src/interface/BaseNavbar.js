import React from "react";
import { Link } from "react-router-dom";
import { Icon, Menu, Dropdown } from "semantic-ui-react";
import { CR, Auth } from "components";

export default class BaseNavbar extends React.Component {
    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {};
    }

    /**
     *
     */
    componentDidMount() {
        // $(".scroll-items").slimScroll({
        //     height: '200'
        // });
        //
        // this.setState({count: this.props.parent.state.user.notifications.length});
        //
        // this.toggleReminder();
    }

    userMenu = () => {
        const user = Auth.getUser();

        if (user !== null) {
            return (
                <Dropdown
                    item
                    icon={false}
                    trigger={
                        <div>
                            <Icon name='user'/> { user.name } <Icon name='dropdown'/>
                        </div>
                    }
                >
                    <Dropdown.Menu>
                        <Dropdown.Item as={ Link } name='account' to="/user/account">
                            <Icon name='user' /> Profile
                        </Dropdown.Item>
                        <Dropdown.Item as={ Link } name='change_password' to="/user/change-password">
                            <Icon name='key' /> Change password
                        </Dropdown.Item>
                        <Dropdown.Divider />
                        <Dropdown.Item as={ Link } name='logout' to="/auth/logout">
                            <Icon name='sign out'/> Sign out
                        </Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
            );
        }
    };

    /**
     *
     * @returns {XML}
     */
    render() {

        return (
            <Menu className='base-navbar' fixed='top' icon stackable inverted>
                <Menu.Menu position='left'>
                    <Menu.Item>
                        <Icon name='protect'/>
                    </Menu.Item>
                </Menu.Menu>
                <Menu.Menu position='right'>
                    <Menu.Item name='help'>
                        Help
                    </Menu.Item>
					<Menu.Item name='help'>
                        <Link to={'/messages'}>Сообщения</Link>
                    </Menu.Item>
                    {this.userMenu()}
                </Menu.Menu>
            </Menu>
        );
    }
}