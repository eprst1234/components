import NotificationSystem from 'react-notification-system';
import React, {Component} from "react";
import { BaseConfirm, BasePreload, BaseNotify } from 'components';
import { Container } from "semantic-ui-react";
import { Router, Route, IndexRoute, IndexLink, Link, browserHistory } from 'react-router';

/**
 * Class BaseAuthLayout.
 */
export default class BaseAuthLayout extends React.Component {

    /**
     * Constructor.
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {};
    }

    /**
     * Render.
     *
     * @returns {XML}
     */
    render() {
        return (
            <div>
                <BaseNotify ref="notify" parent={this} />
                <Container fluid>
                    <BasePreload ref="preload"/>
                    { this.props.children && React.cloneElement(this.props.children, {parent: this}) }
                </Container>
                <NotificationSystem ref="notify" allowHTML={true} />
                <BaseConfirm ref="confirm" parent={this} />
            </div>
        );
    }

};
