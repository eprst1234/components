import React from 'react';
import { CR } from "components";
import {Dimmer, Loader} from "semantic-ui-react";

export default class BasePreload extends React.Component {
    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            active: false,
            options: {},
            handleWatchVisible: true,
        };

        CR.setComponent(this, 'preload');
    }

    isActive() {
        return this.state.active;
    }

    show(options) {
        if (this.isActive()) return ;

        let self = this;
        this.setState({active: true, options: options});
        if (options && options.autoClose) {
            setTimeout(function () {
                self.setState({active: false});
            }, options.autoClose * 1000);
        }
    }

    hide() {
        if (this.isActive()) {
            this.setState({ active: false });
        }
    }

    /**
     *
     * @returns {XML}
     */
    render() {
        let options = this.state.options;

        let loaderText = (options && options.text) ? options.text : 'Пожалуйста, подождите...';
        let loaderInverted = !!(options && options.inverted);
        let loaderSize = (options && options.size) ? options.size : 'medium';

        return (
            <Dimmer active={this.state.active} inverted={loaderInverted}>
                <Loader indeterminate size={loaderSize}>{loaderText}</Loader>
            </Dimmer>
        );
    }
};

// How to use?

// Minimum set for use:
// If you call show() method without autoClose, you need call method hide() for closing preloader

// var preloader = this.refs.preload;
//
// preloader.show();
//
// setTimeout(function () {
//     preloader.hide();
// }, 2000)


// Extended set for use:

// var preloader = this.refs.preload;
//
// var options = {
//     text: 'Произвольный текст...',
//     inverted: false,
//     size: 'medium', //mini, tiny, small, medium, large, big, huge, massive
//     autoClose: 5 // in second
// };
//
// preloader.show(options);