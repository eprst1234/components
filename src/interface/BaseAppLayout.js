// import React from "react";
// import ReactRedirect from "react-redirect";
// import BaseSidebar from "./BaseSidebar";
// import BaseNavbar from "./BaseNavbar";
// import BaseNotify from "./BaseNotify";
// import BaseConfirm from "./BaseConfirm";
// import BasePreload from "./BasePreload";
// import NotificationSystem from 'react-notification-system';
// import moment from 'moment'
// import 'moment/locale/ru';
// moment.locale('ru');
// import { Router, Route, IndexRoute, IndexLink, Link, browserHistory } from 'react-router';
//
// export default class BaseAppLayout extends React.Component {
//
//     /**
//      *
//      * @param props
//      */
//     constructor(props) {
//         super(props);
//         this.state = {
//             user: null,
//             sidebarVisible: false
//         };
//         this.debug = true;
//     }
//
//
//     /**
//      *
//      */
//     componentDidMount() {
//         var self = this;
//
// 		this.putUser();
//
//     }
//
//     /**
//      *
//      */
//     toogleSidebar() {
//         var parent = this.props.parent;
//         parent.setState({sidebarVisible: !parent.state.sidebarVisible});
//     }
//
// 	putUser(){
// 		var self = this;
// 		this.setState({
// 			user: BaseAppLayout.getUser()
// 		}, function(){
// 			self.putUserCall();
// 		});
// 	}
//
// 	putUserCall(){
//
// 	}
//
//     /**
//      *
//      * @returns {null}
//      */
//     static getUser()
//     {
//         return BaseAppLayout.prototype.user;
//     }
//
//     /**
//      *
//      * @param user
//      */
//     static setUser(user)
//     {
// 		BaseAppLayout.prototype.user = user;
//     }
//
//     /**
//      *
//      * @returns {*}
//      */
//     getInstance(object = null) {
//         switch (this.props.location.pathname) {
// 			case "/backend/user-note/"+this.props.params.user_id:
//                 return "user_note";
// 			case "/backend/addresses":
//                 return "addresses";
//             case "/backend/prices/"+this.props.params.price_id+"/addresses":
//                 return "addresses";
// 			case "/backend/accruals-courier":
//                 return "report_courier";
// 			case "/backend/accruals-courier/" + this.props.params.user_id:
//                 return "report_courier";
// 			case "/backend/prices/"+this.props.params.price_id+"/shipment":
// 				return "addresses";
// 			case "/backend/prices/"+this.props.params.price_id+"/shipment/me":
// 				return "addresses";
// 			case "/backend/prices/"+this.props.params.price_id+"/shipment/my":
// 				return "addresses";
// 			case "/backend/addresses_check":
//                 return "addresses";
// 			case "/backend/bot":
//                 return "bot";
// 			case "/backend/explanatory":
//                 return "explanatory";
// 			case "/backend/users-pay":
//                 return "users";
// 			case "/backend/division-products/"+this.props.params.product_id:
//                 return "division_products_all";
// 			case "/backend/division-products-all":
//                 return "division_products_all";
// 			case "/backend/addresses":
//                 return "addresses";
// 			case "/backend/settings_curator":
//                 return "settings_curator";
// 			case "/backend/curators":
//                 return "curators";
// 			case "/backend/operators":
//                 return "operators";
// 			case "/backend/profile":
//                 return "users";
// 			case "/backend/ticket":
//                 return "ticket";
// 			case "/backend/account-ticket":
//                 return "ticket";
// 			case "/backend/couriers":
//                 return "couriers";
// 			case "/backend/couriers/"+ this.props.params.user_id:
//                 return "users";
// 			case "/backend/operators/"+ this.props.params.user_id:
//                 return "users";
// 			case "/backend/price_courier/"+ this.props.params.price_id:
//                 return "price_courier";
// 			case "/backend/accounts":
//                 return "accounts";
// 			case "/backend/news":
//                 return "news";
// 			case "/backend/premium":
//                 return "premium";
// 			case "/backend/news/"+ this.props.params.news_id:
//                 return "comments";
// 			case "/backend/bonuses":
//                 return "bonus";
// 			case "/backend/roles":
//                 return "roles";
//             case "/backend/orders":
//                 return "orders";
// 			case "/backend/orders-reservation":
//                 return "orders";
//             case "/backend/orders_types":
//                 return "order_types";
//             case "/backend/rates":
//                 return "rates";
//             case "/backend/cities":
//                 return "city_groups";
//             case "/backend/products":
//                 return "products";
//             case "/backend/wallets/qiwi":
//                 return "wallets.qiwi";
// 			case "/backend/wallets/qiwi/qiwi_transaction/" + this.props.params.qiwi_number:
//                 return "wallets.qiwi.qiwi_transaction";
// 			case "/backend/wallets/bitcoin/bitcoin_transaction/" + this.props.params.bitcoin_number:
//                 return "wallets.bitcoin.bitcoin_transaction";
//             case "/backend/wallets/bitcoin":
//                 return "wallets.bitcoin";
//             case "/backend/reviews":
//                 return "reviews";
// 			case "/backend/tasks":
//                 return "tasks";
// 			case "/backend/tasks/" + this.props.params.task_id:
//                 return "tasks";
//             case "/backend/pages":
//                 return "pages";
//             case "/backend/users":
//                 return "users";
//             case "/backend/customers":
//                 return "customers";
// 			case "/backend/customer/" + this.props.params.customer_id:
//                 return "orders";
//             case "/backend/settings":
//                 return "settings";
//             case "/backend/prices/" + this.props.params.product_id:
//                 return "prices";
//             case "/backend/regions/" + this.props.params.city_id:
//                 return "regions";
//             case "/backend/user_city/" + this.props.params.user_id:
//                 return "user_city";
//             case "/backend/user/" + this.props.params.user_id:
//                 return "users";
//             case "/backend/user/" + this.props.params.user_id + '/orders_types':
//                 return "order_types";
//             case "/backend/user/" + this.props.params.user_id + '/orders_types/' + this.props.params.order_type_id + '/products':
//                 return "prices";
// 			case "/backend/user/" + this.props.params.user_id + '/orders_types/' + this.props.params.order_type_id + '/products/'+this.props.params.product_id:
//                 return "user_rates";
// 			case "/backend/users_rates":
//                 return "user_rates";
//             case "/backend/recycle":
//                 if (object == null)
//                     return 'recycle';
//                 else
//                     return object.props.model;
//             case "/backend/threads":
//                 return 'threads';
// 			case "/backend/threads/all":
//                 return 'threads.all';
//             case "/backend/messages/" + this.props.params.thread_id:
//                 return 'messages';
//             case "/backend/profile":
//                 return 'profile';
// 			case "/backend/account_log":
//                 return 'account_log';
// 			case "/backend/account_pay_log":
//                 return 'account_log';
// 			case "/backend/notifications":
//                 return 'notifications';
// 			case "/backend/notifications/my":
//                 return 'notifications';
// 			case "/backend/account_pay_log/"+ this.props.params.account_number:
//                 return "account_log";
// 			case "/backend/wait_order":
//                 return 'wait_order';
// 			case "/backend/messages-ticket-approve":
//                 return 'messages_ticket_approve';
//             default:
//                 return "";
//         }
//     }
//
//     /**
//      *
//      * @returns {string}
//      */
//     getCurrentUrl()
//     {
//         return this.props.location.pathname;
//     }
//
//     /**
//      *
//      * @returns {string}
//      */
//     getQueryParams()
//     {
//         return this.props.location.query;
//     }
//
//     /**
//      *
//      * @returns {string}
//      */
//     getUserRole() {
//         return this.state.user.main_role.slug;
//     }
//
//
//     canMake() {
//         switch (this.state.user.main_role.slug) {
//             case 'admin':
//                 return true;
//             case 'curator':
//                 return false;
//             case 'auditor':
//                 return false;
//             default:
//                 return true;
//         }
//     }
//
//     /**
//      *
//      * @param action
//      * @param instance
//      * @returns {*}
//      */
//     getApiUrl(action, instance) {
//         switch (instance) {
//             case 'threads':
//                 switch (action) {
//                     case 'store':
//                         return window.laroute.route("api.v1." + this.getUserRole() + ".messages.store");
//                     case 'index':
//                         return window.laroute.route("api.v1." + this.getUserRole() + ".messages.all");
//                 }
// 			case 'tasks':
//                 switch (action) {
//                     case 'index':
//                         //return window.laroute.route("any.tasks.all");
// 					case 'show':
//                         //return window.laroute.route("show.task");
// 					case 'store':
//                         return window.laroute.route("api.v1." + this.getUserRole() + ".tasks." + action);
//                 }
// 			case 'threads.all':
//                     return window.laroute.route("api.v1." + this.getUserRole() + ".messages.all.admin");
// 			case 'prices2':
//                 return window.laroute.route("api.v1." + this.getUserRole() + ".prices." + action);
// 			case 'qiwi':
//                 return window.laroute.route("api.v1." + this.getUserRole() + ".wallets.qiwi." + action);
// 			case 'bitcoin':
//                 return window.laroute.route("api.v1." + this.getUserRole() + ".wallets.bitcoin." + action);
//             default:
//                 return window.laroute.route("api.v1." + this.getUserRole() + "." + instance + "." + action);
//         }
//     }
//
//     /**
//      *
//      * @param obj
//      * @returns {*}
//      */
//
//
//     /**
//      *
//      * @param object
//      * @param funcSuccess
//      * @param funcError
//      */
//     getModels(object, funcSuccess, funcError)
//     {
//         var instance = this.getInstance(object);
//
//         var data = {
//             page: object.state.current_page,
//             with: object.state.with,
// 			additional: object.state.additionalData,
//         };
//
//         try {
//             object.state.fields.forEach(function (item, i, arr) {
//                 data["fields[" + i + "][k]"] = item.k;
//                 data["fields[" + i + "][v]"] = item.v;
//             });
//         } catch (e) {}
//
//         try {
//             object.state.orderBy.forEach(function (item, i, arr) {
//                 data["orderby[" + item.order + "]"] = item.sort;
//             });
//         } catch (e) {}
//
// 		try {
//             object.state.like.forEach(function (item, i, arr) {
// 				if(item.val != '')
// 				{
// 					data["like[" + item.field + "]"] = item.val;
// 				}
//             });
//         } catch (e) {}
//
// 		try {
//             object.state.range.forEach(function (item, i, arr) {
// 				data["range["+item.field+"]"] = item.min + ',' + item.max;
//             });
//         } catch (e) {}
//
// 		try {
// 			if(moment(object.state.date_start).format('YYYY-MM-DD') != 'Invalid date'){
// 				data["date_start"] = moment(object.state.date_start).format('YYYY-MM-DD');
// 			}
//         } catch (e) {}
//
// 		try {
// 			if(moment(object.state.date_end).format('YYYY-MM-DD') != 'Invalid date'){
// 				data["date_end"] = moment(object.state.date_end).format('YYYY-MM-DD');
// 			}
//         } catch (e) {}
//
// 		try {
//             object.state.has.forEach(function (item, i, arr) {
// 				if(item.val != '')
// 				{
// 					data["has[" + item.field + "]"] = item.val;
// 				}
//             });
//         } catch (e) {}
//
// 		try {
// 			data["hasRole"] = object.state.hasRole;
// 		} catch (e) {}
//
//         try {
//             object.state.unsetRelations.forEach(function (item, i) {
//                 item.relations.forEach(function (relation, k) {
//                     data["unset_relations[" + item.model + "][" + k + "]"] = relation;
//                 });
//             });
//         } catch (e) {}
//
//         try {
//             browserHistory.push({pathname: this.props.location.pathname, query: data});
//         }catch(e){
//             console.log('не могу сохранить историю');
//         }
//
// 		//console.log('get-models');
//
//
//         this.baseGetModels(data, instance, function (response) {
// 			//console.log('get-models-callback');
//             object.setState(function (previousState, currentProps) {
//                 previousState.items = response.data;
//                 try {
//                     previousState.current_page = response.meta.pagination.current_page;
//                     previousState.total_pages = response.meta.pagination.total_pages;
//                 } catch (e) {
//                 }
//
// 				try {
//                     previousState.order_statuses = response.meta.order_statuses;
//                 } catch (e) {
//                 }
//
//                 return previousState;
//             });
//             funcSuccess(response);
//         });
//     }
//
//     /**
//      *
//      * @param object
//      * @param model
//      * @param data
//      * @param func
//      */
//     getDopModels(object, model, data = [], func = function () {
//     }, keyModel = 0) {
//
// 		try {
//             data.has.forEach(function (item, i, arr) {
// 				if(item.val != '')
// 				{
// 					data["has[" + item.field + "]"] = item.val;
// 				}
//             });
//         } catch (e) {}
//
// 		/*
// 		try {
//             data.like.forEach(function (item, i, arr) {
// 				if(item.val != '')
// 				{
// 					data["like[" + item.field + "]"] = item.val;
// 				}
//             });
//
//         } catch (e) {}*/
//
//         this.baseGetModels(data, model, function (response) {
//
//             object.setState(function (previousState, currentProps) {
// 				previousState.dopModel[keyModel == 0 ? model : keyModel].items = response.data;
//                 return previousState;
//             },function(){
// 				func(response);
// 			});
//         });
//     }
//
//     /**
//      *
//      * @param data
//      * @param instance
//      * @param func
//      */
//     baseGetModels(data, instance, func) {
//         var self = this;
//
//         try {
//             if (this.refs.preload.state.handleWatchVisible === true) {
//                 this.refs.preload.setState({visible: true, handleWatchVisible: false});
//             }
//
//         } catch (e) {
//
//         }
//
//
//         AppHelpers.makeRequest({
//             type: "GET",
//             // url: window.laroute.route("api.v1." + this.getUserRole() + "." + instance + ".index"),
//             url: self.getApiUrl('index', instance),
//             data: data,
//             // data: this.cryptoData(data),
//             success: function (response) {
//                 // try{
//                 self.refs.preload.setState({visible: false});
//                 if (response.meta.result === "success") {
//
//                     //for(key in response.data){
//                     //	response.data[key] = JSON.parse(CryptoJS.AES.decrypt(response.data.link, window.pass, {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8))
//                     //	}
//                     //console.log('get-models-callback');
//                     func(response);
//                 }
//                 /*}catch(e){
//                  console.log(data);
//                  console.log(headers);
//                  console.log(self.getApiUrl('index', instance));
//                  console.log('index');
//                  console.log(instance);
//                  console.log(e);
//                  }*/
//             }
//         });
//     }
//
//     /**
//      *
//      * @param object
//      * @param model
//      * @param withModel
//      * @param id
//      * @param func
//      */
//     getDopModel(object, model, withModel = [], id, func = function () {
//     }) {
//         var data = {
//             /*  fields: [
//              {k: 'id', v: id}
//              ], */
//             with: withModel
//         };
//
//         this.baseGetModel(data, model, id,
//             function (response) {
//                 object.setState(function (previousState, currentProps) {
// 					if(model == 'wallets.bitcoin')
// 						previousState.dopModel['bitcoin'].item = response.data;
// 					else if(model == 'wallets.qiwi')
// 						previousState.dopModel['qiwi'].item = response.data;
// 					else
// 						previousState.dopModel[model].item = response.data;
//
//                     return previousState;
//                 },function(){
// 					func(response);
// 				});
//
//             },
//             function (response) {
//
//             });
//     }
//
//     /**
//      *
//      * @param object
//      * @param id
//      * @param funcSuccess
//      * @param funcError
//      */
//     getModel(object, id, funcSuccess, funcError) {
//         var data = {
//             fields: [
//                 {k: 'id', v: id}
//             ],
//             with: object.state.with
//         };
//
//        // console.log(data);
//
//         this.baseGetModel(data, this.getInstance(), id, funcSuccess, funcError);
//     }
//
//     /**
//      *
//      * @param data
//      * @param instance
//      * @param id
//      * @param funcSuccess
//      * @param funcError
//      */
//     baseGetModel(data, instance, id, funcSuccess, funcError) {
//         var self = this;
//         var route = {};
//         route[instance] = id;
//
//         if(instance.split('.').length){
//             route[instance.split('.').pop()] = id;
//         }
//
//         AppHelpers.makeRequest({
//             type: "GET",
//             url: window.laroute.route("api.v1." + self.getUserRole() + "." + instance + ".show", route),
//             data: data,
//             // data: this.cryptoData(data),
//             success: function (response) {
//                 try{
//                     if (response.meta.result === "success") {
//                         return funcSuccess(response);
//                     }
//                     return funcError(response);
//                 }catch(e){
//                     console.log(data);
//                     console.log(laroute.route("api.v1." + self.getUserRole() + "." + instance + ".show", route));
//                     console.log("api.v1." + self.getUserRole() + "." + instance + ".show");
//                     console.log(route);
//                     console.log(e);
//                 }
//             },
//             error()
//             {
//
//             }
//         });
//     }
//
//     /**
//      *
//      * @param object
//      * @param id
//      * @param data
//      * @param headers
//      * @param funcSuccess
//      * @param funcError
//      */
//     updateModel(object, id, data, headers, funcSuccess, funcError, model = 0) {
// 		var instance = this.getInstance();
//
//         //TODO rewrite
// 		if(instance == 'wait_order')
//             instance = 'orders';
//
// 		if(model != 0){
// 			instance = model;
// 		}
//
//         var self = this;
//         var route = {};
//         route[instance] = id;
//
//         //this.refs.preload.setState({visible: true});
//
//         AppHelpers.makeRequest({
//             type: "PUT",
//             url: window.laroute.route("api.v1." + self.getUserRole() + "." + instance + ".update", route),
//             data: data,
//             headers: headers,
//             //data: this.cryptoData(data),
//             success: function (response) {
//                 try{
//                     //self.refs.preload.setState({visible: false});
//                     if (response.meta.result === "success") {
//                         try {
//                             object.handleClearForm();//TODO
//                         } catch (e) {
//                             console.log('form');
//                         }
//                         object.props.list.handleWatch();
//                         self.refs.notify.addNotification({
//                             message: response.meta.text,
//                             level: 'success'
//                         });
//                         self.refs.preload.setState({visible: false});
//                         return funcSuccess();
//                     }
//                     self.refs.notify.addNotification({
//                         message: response.meta.text,
//                         level: 'success'
//                     });
//                     return funcError();
//                 }catch(e){
//                     console.log(data);
//                     console.log(laroute.route("api.v1." + self.getUserRole() + "." + instance + ".update", route));
//                     console.log("api.v1." + self.getUserRole() + "." + instance + ".update");
//                     console.log(route);
//                     console.log(e);
//                 }
//             },
//             error: function (response) {
//                 self.refs.preload.setState({visible: false});
//                 var r = response.responseJSON;
//                 return funcError(r);
//             }
//         });
//     }
//
//     /**
//      *
//      * @param object
//      * @param data
//      * @param headers
//      * @param funcSuccess
//      * @param funcError
//      */
//     addModel(object, data, headers, funcSuccess, funcError)
//     {
//         var self = this;
//         this.refs.preload.setState({visible: true});
//
//         AppHelpers.makeRequest({
//             type: "POST",
//             //url: window.laroute.route("api.v1." + this.getUserRole() + "." + this.getInstance() + ".store"),
//             url: this.getApiUrl('store', self.getInstance(object)),
//             headers: headers,
//             data: data,
//             //data: this.cryptoData(data),
//             success: function (response) {
//                 try{
//                     self.refs.preload.setState({visible: false});
//                     if (response.meta.result === "success") {
//                         object.handleClearForm();
//                         object.props.list.handleWatch();
//                         self.refs.notify.addNotification({
//                             message: response.meta.text,
//                             level: 'success'
//                         });
//                         //self.refs.notify.handleNotify(response.meta.text);
//
//                         return funcSuccess(response);
//                     }
//
//                     self.refs.notify.addNotification({
//                         message: response.meta.text,
//                         level: 'success'
//                     });
//                     return funcError(response);
//                 }catch(e){
//                     console.log(data);
//                     console.log(headers);
//                     console.log(self.getApiUrl('store', self.getInstance(object)));
//                     console.log('store');
//                     console.log(self.getInstance(object));
//                     console.log(e);
//                 }
//             },
//             error: function (response) {
//                 console.log('error-ajax');
//                 self.refs.preload.setState({visible: false});
//                 var r = response.responseJSON;
//                 return funcError(r);
//             }
//         });
//     }
//
//
//     /**
//      *
//      * @param object
//      * @param id
//      * @param funcSuccess
//      * @param funcError
//      */
//     destroyModel(object, id, funcSuccess, funcError, model = 0) {
//         var self = this;
// 		var instance = this.getInstance();
//
// 		if(model != 0){
// 			instance = model;
// 		}
//
//         var route = {};
//         route[instance] = id;
//
//         AppHelpers.makeRequest({
//             type: "DELETE",
//             url: window.laroute.route("api.v1." + self.getUserRole() + "." + instance + ".destroy", route),
//             data: {id: id},
//             success: function (response) {
//                 try{
//
//                     if (response.meta.result === "success") {
//                         object.props.list.handleWatch();
//                         self.refs.notify.addNotification({
//                             message: response.meta.text,
//                             level: 'success'
//                         });
//                         return funcSuccess();
//                     }
//                     self.refs.notify.addNotification({
//                         message: response.meta.text,
//                         level: 'success'
//                     });
//                     return funcError();
//                 }catch(e){
//                     console.log(object);
//                     console.log(laroute.route("api.v1." + self.getUserRole() + "." + self.getInstance() + ".destroy", route));
//                     console.log("api.v1." + self.getUserRole() + "." + self.getInstance() + ".destroy");
//                     console.log(route);
//                     console.log(e);
//                 }
//             },
//             error()
//             {
//
//             }
//         });
//     }
//
//     /**
//      *
//      * @param object
//      * @param id
//      * @param field
//      * @param status
//      * @param type
//      * @param funcSuccess
//      * @param funcError
//      */
//     changeStatus(object, id, field, status, type, funcSuccess, funcError)
//     {
//         var fields = {};
//         fields['id'] = id;
//         fields['action'] = 'changeStatus';
//         fields['dataType'] = type;
//         fields['fieldName'] = field;
//         fields[field] = status;
//         fields['fields'] = [
//             {k: 'id', v: id}
//         ];
//         var headers = [];
//
//         this.updateModel(object, id, fields, headers, funcSuccess, funcError);
//     }
//
//     /**
//      *
//      * @param funcSuccess
//      * @param funcError
//      */
//     getAuthUser(funcSuccess, funcError)
//     {
//         AppHelpers.makeRequest({
//             type: "GET",
//             cache: false,
//             url: window.laroute.route("profile"),
//             success: function (response) {
//                 if (response.meta.result === "success") {
//                     return funcSuccess(response);
//                 }
//                 return funcError(response);
//             },
//             error()
//             {
//
//             }
//         });
//     }
//
//     /**
//      *
//      * @param object
//      */
// 	getRangePrice(object){
//         AppHelpers.makeRequest({
//             type: "GET",
//             cache: false,
//             //url: "/api/v1/base/custom_price_courier/range/get",
//             url: window.laroute.route("price.courier.range.get"),
//             success: function (response) {
//                 console.log(response);
//                 object.setState(function(prv){
//                     prv.rangePrice = response.data;
//                     return prv;
//                 })
//             },
//             error()
//             {
//
//             }
//         });
// 	}
//
//     /**
//      *
//      * @param id
//      * @param callbackSuccess
//      */
// 	getOrCreateThread(id, callbackSuccess)
//     {
//         AppHelpers.makeRequest({
//             type: "GET",
//             url: window.laroute.route('api.v1.'+this.getUserRole()+'.messages.get.or.create', {id: id}),
//             success: function (response) {
//                 callbackSuccess(response);
//             },
//             error()
//             {
//
//             }
//         });
// 	}
//
//
// 	/*
// 	cryptoData(data){
// 		var arr = {};
// 		for(var key in data){
// 			if(data[key] instanceof Object){
// 				for(var key2 in data[key]){
// 					if(data[key][key2] instanceof Object){
// 						for(var key3 in data[key][key2]){
// 							data[key][key2][key3] = CryptoJS.AES.encrypt(JSON.stringify(data[key][key2][key3]), window.pass, {format: this.getChip()}).toString();
// 						}
// 					}
// 					else{
// 						data[key][key2] = CryptoJS.AES.encrypt(JSON.stringify(data[key][key2]), window.pass, {format: this.getChip()}).toString();
// 					}
// 				}
// 			}
// 			else{
// 				data[key] = CryptoJS.AES.encrypt(JSON.stringify(data[key]), window.pass, {format: this.getChip()}).toString();
// 			}
// 		}
//
// 		return data;
// 	}
//
//
//
// 	getChip(){
// 		var CryptoJSAesJson = {
// 						stringify: function (cipherParams) {
// 							var j = {ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64)};
// 							if (cipherParams.iv) j.iv = cipherParams.iv.toString();
// 							if (cipherParams.salt) j.s = cipherParams.salt.toString();
// 							return JSON.stringify(j);
// 						},
// 						parse: function (jsonStr) {
// 							var j = JSON.parse(jsonStr);
// 							var cipherParams = CryptoJS.lib.CipherParams.create({ciphertext: CryptoJS.enc.Base64.parse(j.ct)});
// 							if (j.iv) cipherParams.iv = CryptoJS.enc.Hex.parse(j.iv);
// 							if (j.s) cipherParams.salt = CryptoJS.enc.Hex.parse(j.s);
// 							return cipherParams;
// 						}
// 					};
//
// 		return CryptoJSAesJson;
// 	}*/
//
//     /**
//      *
//      * @returns {XML}
//      */
//     render() {
//
//         if (this.state.user) {
//
// 			if(this.state.user.blocked_at != ''){
// 				return (
// 					<ReactRedirect location='/backend/logout'>
// 					</ReactRedirect>
// 				);
// 			}
//
//             return (
//                 <div className="forUnderstand">
//                     <BaseSidebar ref="sidebar" parent={this}/>
//                     <div className="pusher">
//                         <BaseNavbar ref="navbar" parent={this}/>
//                         <BasePreload ref="preload"/>
//                         { this.props.children && React.cloneElement(this.props.children, {parent: this}) }
//                     </div>
//                     <NotificationSystem ref="notify" allowHTML={true}/>
//                     <BaseNotify user={this.state.user} parent={this}/>
//                     <BaseConfirm ref="confirm" parent={this}/>
//                     <BaseAuth ref="auth" parent={this}/>
//                 </div>
//             );
//         } else {
//             return (<div>
//                 <BasePreload ref="preload"/>
//             </div>);
//         }
//     }
// };
//
// /**
//  *
//  * @type {null}
//  */
// BaseAppLayout.prototype.user = null;

import React from "react";
import { CR, BaseSidebar, BaseNavbar, BaseNotify, BaseConfirm, BasePreload, BaseChat } from 'components';
import NotificationSystem from 'react-notification-system';
import moment from 'moment'
import 'moment/locale/ru';
import { Router, Route, IndexRoute, IndexLink, Link, browserHistory } from 'react-router';
import { Container } from "semantic-ui-react";
import ReactDOM from 'react-dom';
moment.locale('ru');

export default class BaseAppLayout extends React.Component {

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            run: false
        };
        CR.setComponent(this, 'layout', true);
        this.debug = true;
    }

    componentDidMount() {
        this.setState({ run: true });
    }

    /**
     *
     * @returns {XML}
     */
    render() {
        let children = null;

        if (this.state.run && this.props.children) {
            children = React.cloneElement(this.props.children, {parent: this});
        }

        return (
            <div className="forUnderstand">
                <BaseSidebar ref="sidebar" parent={this} />
                <BaseNavbar ref="navbar" parent={this} />
                <Container fluid>
                    <BasePreload ref='preload' parent={this} />
                    { children }
                </Container>
                <BaseConfirm ref="confirm" parent={this}/>
                <BaseNotify ref="notify" parent={this}/>
                {/*<BaseChat ref="chat" parent={this}/>*/}
            </div>
        );
    }
};