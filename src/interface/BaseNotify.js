import React from "react";
import {Button, Icon} from "semantic-ui-react";
import ApiRequest from "./../api/ApiRequest";
import { CR } from "components";

export default class BaseNotify extends React.Component {

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);

        this.state = {
            count: 0,
			count_explanatory: 0,
            isRunning: false
        };

        CR.setComponent(this, 'notify');
    }

    /**
     *
     */
    componentDidMount()
    {
        let self = this;

        if (typeof this.props.user === 'undefined') return ;

        if (this.props.user.notifications.length) {
            this.setState({count: this.props.user.notifications.length});
            this.props.user.notifications.forEach(function (item, i, arr) {
                if (parseInt(item.is_viewed) === 0) {
                    self.handleNotify(item.id, item.text, true, 0, 'tc');
                }
            });
        }
		
		if (this.props.user.explanatorys.length) {
			this.setState({count_explanatory: this.props.user.explanatorys.length}, function(){
				self.handleExplain(0);
			});
			/*this.props.user.explanatorys.forEach(function (item, i, arr) {
               self.handleExplain(item);
            })*/
		}
    }

    show = (content, time = 0) => {
        let self = this;
        this.content = content;
        this.setState({ isRunning: true });

        setTimeout(() => {
            self.hide();
        }, time);
    };

    hide = () => {
        this.content = '';
        this.setState({ isRunning: false });
    };


    /**
     *
     * @param id
     * @param event
     */
    setAsRead(id, event) {
        var self = this;
        ApiRequest.makeRequest({
            type: "PUT",
            url: window.laroute.route("api.v1." + this.props.parent.getUserRole() + ".notifications.update", {notifications: id}),
            data: {id: id, is_viewed: 1},
            success: function (response) {
                if (response.meta.result === "success") {
                    var notifyCount = --self.state.count;
                    self.setState({count: notifyCount});
                    self.props.parent.refs.navbar.setState({count: notifyCount});
                }
            },
            error: function () {

            }
        });
    }

    /**
     *
     * @param ind
     */
	handleExplain(ind){
		var self = this;
		if(ind < this.state.count_explanatory){
			$('.explanatory-add').modal('show');
			this.refs.form.setState(function(prv){
				prv.title = self.props.user.explanatorys[ind].title;
				prv.ind = ind;
				prv.id = self.props.user.explanatorys[ind].id;
				prv.form.reason = '';
			});
		}
	}


    /**
     * 
     * @param id
     * @param text
     * @param level
     * @param seconds
     * @param position
     */
    handleNotify(id, text, level, seconds, position) {

        var self = this;

        var level = 'success';
        if (typeof level === 'undefined') {
            level = 'info';
        }

        if (typeof seconds === 'undefined') {
            seconds = 3000;
        }

        if (typeof position === 'undefined') {
            position = 'tr';
        }

        this.props.parent.refs.notify.addNotification({
            message: text,
            level: level,
            autoDismiss: seconds,
            position: position,
            action: {
                label: 'Принято.',
            },
            onRemove: function () {
                self.setAsRead(id);
            }
        });
    }

    renderNotify() {
        if (! this.state.isRunning) return ;

        return (
            <div>
                { this.content }
            </div>
        );
    }

    /**
     *
     * @returns {XML}
     */
    render() {
        let out = <div></div>;

        if (this.state.count) {
            out = <div className="not-work-pattern">{this.state.count}</div>
        }

        return (
            <div>
                { out }

                { this.renderNotify() }

            </div>
        );
    }
};