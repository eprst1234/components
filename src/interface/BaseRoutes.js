import React from 'react';
import {
    BrowserRouter as Router,
    Link,
    browserHistory,
    Route,
    Switch,
} from 'react-router-dom';
import { CR } from 'components';

export default class BaseRoutes extends React.Component
{
    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);

        CR.setComponent(this, 'app', true);
    }

    /**
     *
     * @param component
     */
    static setBaseComponent(component)
    {
        this.state.component = component;
    }

    /**
     *
     * @returns {null}
     */
    getBaseComponent()
    {
        return this.state.component;
    }

    /**
     *
     * @param query
     * @returns {*}
     */
    queryStringToHash(query) {

        if (query == '') return null;

        var hash = {};

        var vars = query.split("&");

        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            var k = decodeURIComponent(pair[0]);
            var v = decodeURIComponent(pair[1]);


            if (typeof hash[k] === "undefined") {

                if (k.substr(k.length-2) != '[]')
                    hash[k] = v;
                else
                    hash[k.substr(0, k.length-2)] = [v];


            } else if (typeof hash[k] === "string") {
                hash[k] = v;

            } else {
                hash[k.substr(0, k.length-2)].push(v);
            }
        }
        return hash;
    }


    /**
     *
     */
    componentDidMount() {
        var self = this;

        window.onerror = function(message, file, line, column, error) {
            var errorData = {
                message: message,
                file: file,
                line: line,
                column: column,
                trace: error.stack,
                user_agent: navigator.userAgent,
            };

            // AppHelpers.makeRequest({
            //     type: "POST",
            //     url: window.laroute.route("error.logs"),
            //     cache: false,
            //     data: errorData
            // });

            console.log(errorData);
        };


        function proxy(context, method, message) {
            return function() {
                var errorData = {
                    message: arguments[0],
                    trace: self.getStackTrace(),
                    user_agent: navigator.userAgent,
                };

                // AppHelpers.makeRequest({
                //     type: "POST",
                //     url: window.laroute.route("error.logs"),
                //     cache: false,
                //     data: errorData,
                // });

                method.apply(context, [message].concat(Array.prototype.slice.apply(arguments)))
            }
        }

        self.getAuthUser(
            function (response) {
                self.setState(function (previousState, currentProps) {
                    previousState.user = response.data;
                    previousState.auth = true;
                });
            },
            function (response) {

            }
        );
    }


    getStackTrace() {
        try {
            var obj = {};
            Error.captureStackTrace(obj);
            return obj.stack;
        }catch(err){
            return 'Not stack';
        }
    }

    /**
     *
     * @param text
     * @returns {string}
     */
    getUrlWithRole(text)
    {
        return "/backend/" + this.state.user.main_role.slug + "/" + text;
    }

    /**
     *
     */
    getAuthUser(funcSuccess, funcError)
    {
        // var self = this;
        // AppHelpers.makeRequest({
        //     type: "GET",
        //     url: window.laroute.route("profile"),
        //     cache: false,
        //     success: function(response)
        //     {
        //         if(response.meta.result === "success"){
        //             return funcSuccess(response);
        //         }
        //         return funcError(response);
        //     },
        //     error: function()
        //     {
        //         self.setState(function (previousState, currentProps) {
        //             previousState.auth = false;
        //         });
        //     }
        // });
    }

    getRoutes()
    {
        return this.props.routes;
    }

}

BaseRoutes.prototype.functRoutes = function(role){

};
BaseRoutes.prototype.funcIndex = function(role){

};
BaseRoutes.prototype.funcAppLayout = function(role){

};