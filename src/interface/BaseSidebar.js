import React from "react";
import {Image, Menu, Sidebar, Icon, Popup} from "semantic-ui-react";
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom';
import {CR} from "components";

export default class BaseSidebar extends React.Component {
    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            activeMenuIndex: null,
            visible: false
        };
        CR.setComponent('sidebar');
    }

    /**
     *
     * @returns {*|Array}
     */
    getMenus() {
        return this.props.parent.props.routes;
    }

    /**
     *
     * @param index
     * @param event
     */
    handleMenuClick(index, event) {
        this.setState({activeMenuIndex: index})
    }

    /**
     *
     */
    toggleSidebar() {
        this.setState({visible: !this.state.visible})
    }

    /**
     *
     * @param event
     * @param name
     */
    handleItemClick(event, {name}) {
        this.setState({activeItem: (name === this.state.activeItem ? null : name)})
        if (this.state.activeItem !== name) {
            this.setState({activeSubItem: null})
        }
    }

    /**
     *
     * @param event
     * @param name
     */
    handleSubItemClick(event, {name}) {
        this.setState({activeSubItem: name})
    }

    /**
     *
     * @param item
     * @returns {XML}
     */
    getMenuSubItems(item) {
        const {activeSubItem} = this.state;

        if (item.hasOwnProperty('subitem')) {
            return (
                <Menu.Menu>
                    {item.subitem.map((item, i) => {
                        return (
                            <Menu.Item key={i}>
                                <Menu.Item as={item.hasOwnProperty('uri') ? Link : 'div'}
                                           name={item.title}
                                           active={activeSubItem === item.title}
                                           to={item.hasOwnProperty('uri') ? item.uri : ''}
                                           onClick={this.handleSubItemClick.bind(this)}>
                                    {item.title}
                                </Menu.Item>
                            </Menu.Item>
                        );
                    })}
                </Menu.Menu>
            );
        }
    }

    /**
     *
     * @returns {Array}
     */
    getMenuItems() {
        let menu = this.getMenus();
        const {activeItem} = this.state;

        return (
            menu.sidebar.map((item, i) => {
                return (
                    <Menu.Item key={i}>
                        <Menu.Item as={item.hasOwnProperty('uri') ? Link : 'div'}
                                   name={item.title}
                                   active={activeItem === item.title}
                                   onClick={this.handleItemClick.bind(this)}
                                   to={item.hasOwnProperty('uri') ? item.uri : ''}
                                   className='parent'>
                            <Icon name={item.icon}/>
                            <span>{item.title}</span>
                        </Menu.Item>
                        {this.getMenuSubItems(item)}
                    </Menu.Item>
                );
            })
        )
    }

    /**
     *
     * @returns {XML}
     */
    render() {
        let menu = this.getMenus();

        return (
            <Sidebar className='base-sidebar' as={Menu} animation='push'
                     visible={this.state.visible} vertical inverted
            >
                <Menu.Item as='div' name='open-sidebar' className='open-sidebar'
                           onClick={this.toggleSidebar.bind(this)}>
                    <Icon name={menu.header.icon}/>
                </Menu.Item>
                <Menu.Header>{menu.header.title}</Menu.Header>
                {this.getMenuItems()}
            </Sidebar>
        );
    }
};