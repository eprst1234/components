import React from "react";
import { CR } from "components";
import {Button, Image, Modal} from "semantic-ui-react";

export default class BaseConfirm extends React.Component {
    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            options: {},
            deleted: false,
        };
        CR.setComponent(this, 'confirm');

    }

    show(question, yes, no) {

        var options = {
            content: question,
            actionButtons: [
                {
                    button: (<Button icon="checkmark" positive content="Да"/>),
                    callback: yes
                },
                {
                    button: (<Button icon="checkmark" negative content="Нет"/>),
                    callback: no
                }
            ]
        }

        this.setState({show: true, options: options});
    };

    showExtended(options) {
        this.setState({show: true, options: options});
    }

    execute(callback, autoClose, event) {
        callback();
        if (autoClose) {
            this.close();
        }
    }

    close() {
        this.setState({show: false});
    }

    render() {

        var self = this;
        var options = this.state.options;

        // Modal.Actions Part
        var actions = '';
        if (options.actionButtons && options.actionButtons.length > 0) {
            actions = options.actionButtons.map(function (data, i) {
                var button = '';
                var autoClose = data.autoClose ? data.autoClose : true;
                if (data.callback && typeof data.callback === "function" && typeof data.button === "object") {
                    button = React.cloneElement(
                        data.button,
                        {key: i, onClick: self.execute.bind(this, data.callback, autoClose)}
                    )
                } else if (data.callback && typeof data.callback === "function" && typeof data.button === "string") {
                    button = (
                        <Button key={i} onClick={self.execute.bind(this, data.callback, autoClose)}>{data.button}</Button>);
                } else {
                    button = (<Button key={i} onClick={this.close.bind(this)} primary>{data.button}</Button>)
                }
                return button;
            }, this);
        } else {
            actions = (<Button onClick={this.close.bind(this)} primary>Закрыть</Button>)
        }

        // Modal.Header Part
        var header = '';
        if (options.header) {
            header = (<Modal.Header>{options.header}</Modal.Header>);
        }

        // Modal.Description Part
        var hasImage = false;
        var content = '';
        var description = '';
        if (options.content) {
            description = options.content;
        }

        // Show Modal.Description with image
        if (options.image) {
            var image = '';
            if (typeof options.image === "string") {
                image = (<Image wrapped size="medium" src={options.image}/>);
            }
            if (typeof options.image === "object") {
                image = options.image
            }
            content = (
                <Modal.Content image>
                    {image}
                    <Modal.Description>
                        {description}
                    </Modal.Description>
                </Modal.Content>
            );
        } else {
            // Show standart Modal.Description
            content = (<Modal.Content>{description}</Modal.Content>);
        }

        // Size Settings
        var size = options.size ? options.size : 'large';

        // Basic Settings
        var basic = options.basic ? true : false;

        return (
            <Modal open={this.state.show} basic={basic} size={size} onClose={this.close.bind(this)}>
                {header}
                {content}
                <Modal.Actions>
                    {actions}
                </Modal.Actions>
            </Modal>
        );
    }

    // How to use?

    // Minimum set for use:
    //
    // var options = {
    //     content: 'Test confirm content',
    //     yes: function () {
    //         alert('You send "Yes"');
    //     },
    //     no: function () {
    //         alert('You send "No"');
    //     }
    // };
    //
    // confirm.show(options);

    // Extended set for use:
    //
    // var options = {
    //     basic: false,
    //     content: (<div><Header>Default Profile Image</Header><p>Is it okay to use this photo?</p></div>),
    //     size: 'fullscreen', // fullscreen, large, mini, smalltiny
    //     header: 'Test header',
    //     image: (<Image wrapped size="medium" src="http://via.placeholder.com/200x200"/>),
    //     actionButtons: [
    //         {
    //             button: (<Button color='violet'><Icon name='checkmark'/>Confirm</Button>),
    //             callback: function () {
    //                 alert('It\'s Work! =)');
    //             }
    //         }
    //     ]
    // };
    //
    // confirm.show(options);


};