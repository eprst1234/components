import React from 'react'
import moment from 'moment'
import DatePicker from 'react-datepicker'
import 'moment/locale/ru';
moment.locale('ru');

import {
    Grid,
    Segment,
    Breadcrumb,
    Container,
    Header,
    Icon,
    Menu,
    Divider,
    Button,
    Checkbox,
    Popup,
    Dropdown,
    Form
} from 'semantic-ui-react';


export default class BaseStateFilter extends React.Component {

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        var list = this.props.list;
		var city = '';
		if(list.state.filterCity != false){
			city = ( <select name="city_id" className="ui search fluid dropdown" placeholder="Город" value={list.state.city_id} onChange={list.cityFilter.bind(list)}>
                                <option className="default text" value="">
									Выберите город
                                </option>
                                {list.state.dopModel.cities.items.map(function (data, i) {
                                     return (<option key={i} value={data.id}>{data.name}</option>)
                                 })}
                            </select>);
		}
		
        return (

            <Menu secondary className="between-date">
                <Grid stackable className="state-filter">
                    <Grid.Row verticalAlign='bottom'>
                        <Grid.Column mobile={2} tablet={3} computer={2}>
                            <label>Начиная с</label>
                            <DatePicker className="test" selected={list.state.date_start}
                                        onChange={list.handleChangeDate.bind(list, 'date_start')}
                                        placeholderText="Дата Оплаты"/>
                        </Grid.Column>
                        <Grid.Column mobile={2} tablet={3} computer={2}>  
                            <label>по</label>
                            <DatePicker selected={list.state.date_end}
                                        onChange={list.handleChangeDate.bind(list, 'date_end')}
                                        placeholderText="Дата Оплаты"/>
                        </Grid.Column>
                        <Grid.Column mobile={2} tablet={3} computer={2}>
                            <Button size="mini" fluid basic primary className="moment" size="small"
                                    onClick={list.handleWatch.bind(list)}>
                                Показать
                            </Button>
                        </Grid.Column>

                        <div className="visible-tablet">
                            <Divider horizontal>Быстрый фильтр</Divider>
                        </div>

                        <Grid.Column mobile={2} tablet={3} computer={2}>
                            <Button size="mini" fluid basic primary className="moment" size="small"
                                    onClick={list.handleFastSelectDate.bind(list, 0)}>
                                Сегодня
                            </Button>
                        </Grid.Column>
                        <Grid.Column mobile={2} tablet={4} computer={2}>
                            <Button size="mini" fluid basic primary className="moment" size="small"
                                    onClick={list.handleFastSelectDate.bind(list, 7)}>
                                Последние 7 дней
                            </Button>
                        </Grid.Column>
                        <Grid.Column mobile={2} tablet={4} computer={2}>
                            <Button size="mini" fluid primary className="moment" size="small"
                                    onClick={list.handleFastSelectDate.bind(list, 30)}>
                                Последние 30 дней
                            </Button>
                        </Grid.Column>
						<Grid.Column mobile={2} tablet={4} computer={2}>
							{city}
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Menu>
        );
    }

}