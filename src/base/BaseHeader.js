import React from 'react';
import PropTypes from 'prop-types';
import {
    Breadcrumb,
    Header,
    Segment
} from 'semantic-ui-react';
import { Link } from 'react-router-dom';

export default class BaseHeader extends React.Component
{

    static propTypes = {
        title: PropTypes.oneOfType([
            PropTypes.node,
            PropTypes.string,
        ]),
        breadcrumb: PropTypes.oneOfType([
            PropTypes.node,
            PropTypes.array,
            PropTypes.string,
        ]),
        buttons: PropTypes.node
    };

    static defaultProps = {
        title: '',
        breadcrumb: '',
        buttons: ''
    };

    names = {
        products: 'продукты',
        prices: 'цены',
    };

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
    }

    customBreadcrumb = (breadcrumb) => {
        return (
            <Breadcrumb>
                { breadcrumb.map((item) => {
                    let { divider = null, content, ...rest } = item;

                    let result = (
                        <Breadcrumb.Section {...rest} >
                            { content }
                        </Breadcrumb.Section>
                    );

                    if (divider !== null) {
                        result = <Breadcrumb.Divider key={item.key} icon={ divider } />;
                    }

                    return result;
                })}
            </Breadcrumb>
        );
    };

    generateBreadcrumbs = () => {
        let brc = [];
        let path = document.location.pathname.split('/');
        let loc = document.location.pathname;
        let p = '';

        if (path[path.length - 1]) {
            brc.push(
                <Breadcrumb.Section
                    key={`header_${path.length / 1.2}`}
                    active>{this.names[path[path.length - 1]] || path[path.length - 1]}
                 </Breadcrumb.Section>
            );
        }

        for (let i = path.length - 1; i > 1; i--) {
            p = '/' + path[i] + p;
            brc.push(<Breadcrumb.Divider key={`header_${i}`} icon='right arrow'/>);
            brc.push(
                <Breadcrumb.Section key={`header_${i * 1.1}`} as={Link} link to={loc.replace(p, '')}>
                    {this.names[path[i - 1]] || path[i - 1]}
                </Breadcrumb.Section>
            );
        }

        brc.push(<Breadcrumb.Divider key={'main_' + path.length} icon='right arrow'/>);
        brc.push(
            <Breadcrumb.Section key={'main_' + path.length - 1} as={Link} link to='/'>
                Главное
            </Breadcrumb.Section>
        );

        brc.reverse();

        return (
            <Breadcrumb key={'header_' + this.props.title}>
                { brc }
            </Breadcrumb>
        );
    };

    render() {
        let { title, breadcrumb, buttons } = this.props;

        let bread = null;
        if (breadcrumb instanceof Array && breadcrumb.length > 0) {
            bread = this.customBreadcrumb(breadcrumb);
        } else if ((breadcrumb instanceof Array && breadcrumb.length === 0)
                   || typeof breadcrumb === 'undefined') {
            bread = this.generateBreadcrumbs();
        } else {
            bread = breadcrumb;
        }

        return (
            <Segment className="page-header" clearing basic vertical={true}>
                <div className="content">
                    <Header floated='right'>
                        { buttons }
                    </Header>
                    <Header as='h2' floated='left'>
                        { title }
                    </Header>
                    <Header floated='left' style={{ margin: 0, lineHeight: 1.7 }}>
                        { bread }
                    </Header>
                </div>
            </Segment>
        );
    }
}
