import React from 'react'


export default class BaseSort extends React.Component
{
    
    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            sort: false,
        };
    }
    /*
    componentWillReceiveProps(nextProps)
    {
        
        if(nextProps.list.state.orderBy.order !== this.props.field)
            this.setState({visibleSort: false});
    }*/
    
    sort()
    {
        switch(this.state.sort)
        {
            case false:
            
                    this.props.list.sort(this.props.field, 'asc');
                    this.setState({sort: 'asc'}); 
                    break;
                    
            
            case 'asc':
                    
                    this.props.list.sort(this.props.field, 'desc');
                    this.setState({sort: 'desc'}); 
                    break;
                    
                    
            case 'desc':
                    
                    this.props.list.sort(this.props.field, false);
                    this.setState({sort: false}); 
                    break;
        }
    }
    
    startSort()
    {
        this.setState({visibleSort: true});
        this.sort();
    }
    
    render()
    {
        
        return (
            <p onClick={this.startSort.bind(this)}>
                <span>{this.props.fieldName}</span>
                <span className={this.state.sort ? '' : 'hide'}> 
                    <i onClick={this.sort.bind(this)} className={this.state.sort === 'asc' ? 'long arrow up icon' : 'long arrow down icon'}></i>
                </span>
            </p>
        );
    }
    
    
    
}