import React from 'react'

export default class BaseRow extends React.Component {
    /**
     * 
     * @param props
     * @param context
     */
    constructor(props, context) {
        super(props);
        this.state = {
            with: []
        };
    }

    /**
     * 
     * @param event
     */
    handleChangeStatus(id, event)
    {
		var id2 = '';
		if(typeof id == 'undefined'){
			id2 = this.props.data.id;
		}else{
			id2 = id;
		}
        var checkBox = $(event.target);
        var checked = checkBox.is(':checked');
        var type = $(event.target).data('type');

        this.props.list.props.parent.changeStatus(this, id2, event.target.getAttribute("name"), checked ? 1 : 0, type ? type : 'bool',
            function (response) {

            },
            function (response) {

            }
        );
    }

    /**
     * 
     */
    handleDelete()
    {
        var confirm = this.props.list.props.parent.refs.confirm;
        var self = this;
        
        var props = '';
        if('name' in self.props.data)
            props = 'name';
        else
            props = 'title';
            
        confirm.setState({item: self.props.data[props]});
        
        $('.confirm.modal').modal({
            onApprove: function(element){
                self.props.list.props.parent.destroyModel(self, self.props.data.id, function () {

                    }, function () {

                });
            }
        }).modal('show');
    }

    /**
     * 
     */
    handleUpdate()
    {
        var self = this;
        this.props.list.props.parent.getModel(this, this.props.data.id, function (response) {
            self.props.list.refs.form.handleFillForm(response.data);
            self.props.list.handleUpdate();
        });
    }
    
    /**
     * 
     */
    handleView()
    {
        var self = this;
        this.props.list.props.parent.getModel(this, this.props.data.id, function (response) {
            self.props.list.refs.form.handleFillForm(response.data);
            self.props.list.handleView();
        });
    }
    
    
}