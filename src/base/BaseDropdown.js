import React from 'react'
import {
    Dropdown
} from "semantic-ui-react";

export default class BaseDropdown extends React.Component
{
    /**
     * 
     * @param props
     */
    constructor(props){
        super(props);
        this.state = {
			open: false,
        };
    }
	
	onChange(event, data){
		var value = '';
		this.setState(function(prv){
			prv.open = false;
			return prv;
		});
		if(typeof data.value == 'object' && this.props.multiple === false){
			value = event.target.value;
		}else{
			value = data.value;
		}
		this.props.onChange(value, data, event);
	}
	
	
	getOptions(){
		var arr = [], value = '', text = '';
		if(typeof this.props.options.data == 'object'){
			for(var i = 0; i < this.props.options.data.length; i++){
				if(typeof this.props.options.value === 'function'){
					value = this.props.options.value(this.props.options.data[i]);
				}else{
					value = this.props.options.data[i][this.props.options.value];
				}
				if(typeof this.props.options.text === 'function'){
					text = this.props.options.text(this.props.options.data[i]);
				}else{
					text = this.props.options.data[i][this.props.options.text];
				}

				arr.push({
					value: value,
					text: text,
				});
			}
			
			return arr;
		}
		
		return [];
		
	}
	
	onClick(){
		this.setState(function(prv){
			prv.open = true;
			return prv;
		})
		
	}
	

    render(){
        return (
            <Dropdown multiple={this.props.multiple} selection open={this.state.open} value={this.props.value} onClick={this.onClick.bind(this)} className={'ui dropdown item'} placeholder={this.props.placeholder} name={this.props.name} options={this.getOptions()} onChange={this.onChange.bind(this)}/>
        )
    }
};

BaseDropdown.defaultProps = {	
	className: 'ui dropdown item',
	onChange: function(){},
}