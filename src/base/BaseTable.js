import React from "react";
import {Api, BasePagination, BaseHeader, Button as HeaderButton, Arr} from "components";
import {Icon, Popup, Table, Segment, Button, Container, Header} from 'semantic-ui-react';
import {Link} from "react-router-dom";

export default class BaseTable extends React.Component {

    constructor(props) {
        super(props);
		this.defaultParams = {
            actions: {
                create: true,
            }
        };
        this.state = {
            items: {data: []},
        };
    }

    getItems(toDate = true) {
        if (toDate) {
            return this.state.items.data;
        } else {
            return this.state.items;
        }
    }

    getParams() {
		return {...this.defaultParams, ...this.params};
    }

    componentDidMount() {
        this.getData();
    }

    getData(page = 1) {
        let request = Api.get(this.getParams().urls.control, 'index');
        this.getQuery(request);
        request.paginate(page).bind(this, 'items');
        console.log(this.state);
    }

    getQuery(request) {

    }

    putDelete(id) {
        let self = this;

        Api.delete(this.getParams().urls.control, 'destroy', id)
            .call((r) => {
                if (r.result === 'success') {
                    self.getData(self.state.items.current_page);
                }
            })
            .withNotify();
    }

    //////////////////////////////////////////////////////////////////////////////////////

    breadcrumb = () => {
        return [
            {key: 'home', content: 'Главная', as: Link, to: '/'},
            {key: 'divider-2', divider: 'right arrow'},
            {key: 'page', content: this.getParams().titles.main, active: true},
        ];
    };

    getButtonDelete(value) {
        return (
            <HeaderButton.Remove
                title='Удалить'
                onConfirm={this.putDelete.bind(this, value.id)}
            />
        );
    }

    buttonView = ({ title, to }) => {
        return <HeaderButton.View title={title} to={to} />
    };

    getButtonEdit(url) {
        return (
            <HeaderButton.Edit to={url} />
        );
    }

    getRow(index, value) {
        return (<Table.Cell key={index}>{value}</Table.Cell>);
    }

    getRows(value) {
        let self = this;
        let cells = [];
        this.getParams().ceils.map((data, index) => {
            if (typeof data.field === 'function') {
                cells.push(<Table.Cell key={index}>{data.field(value)}</Table.Cell>);
            } else {
                let dataField = data.field !== false ? Arr.getPropRecursive(value, data.field) : '';
                cells.push(self.getRow(index, dataField));
            }
        });

        return cells;
    }


    getPaginator() {
        return (
            <Table.Footer>
                <Table.Row>
                    <Table.HeaderCell colSpan={this.params.ceils.length}>
                        <BasePagination
                            paginator={this.state.items}
                            change={(page) => this.getData(page)}
                        />
                    </Table.HeaderCell>
                </Table.Row>
            </Table.Footer>
        )
    }

    getTableBody() {
        let rows = '';
        if (this.getItems().length > 0) {
            rows = this.getItems().map((value, index) => (
                <Table.Row key={index}>
                    {this.getRows(value)}
                </Table.Row>
            ));
        } else {
            rows = (
                <Table.Row>
                    <Table.Cell>
                        Нет данных
                    </Table.Cell>
                </Table.Row>)
        }
        return (
            <Table.Body>
                {rows}
            </Table.Body>
        );
    }

    getTableHeaders() {
        let headers = [];
        this.getParams().ceils.map(function (data, index) {
            headers.push(
                <Table.HeaderCell key={index}>{data.name}</Table.HeaderCell>
            )
        });

        return (
            <Table.Header>
                <Table.Row>
                    {headers}
                </Table.Row>
            </Table.Header>
        )
    }
	
	getHeader(){
        let buttons = null;
        if (typeof this.getParams().actions !== 'undefined'
            && this.getParams().actions.create === true
        ) {
            buttons = (
                <div className="actions">
                    <HeaderButton.Create
                        title={this.getParams().titles.create}
                        to={this.getParams().urls.create}
                    />
                </div>
            );
        }

        return (
            <BaseHeader
                title={this.getParams().titles.main}
                breadcrumb={this.breadcrumb()}
                buttons={ buttons }
            />
        );
    }



    render() {
        return (
            <Container className="page">
                {this.getHeader()}
                <Header as='h4' attached='top' block>
                    <Icon name='list'/> {this.getParams().titles.main}
                </Header>
                <Segment attached>
                    <Table celled selectable>
                        {this.getTableHeaders()}
                        {this.getTableBody()}
                        {this.getPaginator()}
                    </Table>
                </Segment>
            </Container>
        );
    }
}