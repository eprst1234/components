import React from 'react'

export default class Timer extends React.Component
{
    /**
     * 
     * @param props
     */
    constructor(props){
        super(props);
        this.state = {
			start: false,
            countdown:false,
            seconds: 0,
			timer: {
				days: 0,
				hours: 0,
				minutes: 0,
				seconds: 0,
			},
        };
    }
	
	componentDidMount(){
		if(this.props.start == true){
			this.startTimer(this.props.seconds);
		}
	}
	
	componentWillUnmount(){
		this.loadInterval && clearInterval(this.loadInterval);
		this.loadInterval = false;
	}
	
	startTimer(seconds){
		var self = this;
		if(this.state.start == false){
			this.setState(function(prv){
				prv.seconds = seconds;
				prv.start = true;
				return prv;
			}, function(){
				self.loadInterval = setInterval(self.setTime.bind(self), 1000);
			});
		}else{
			this.setState(function(prv){
				prv.seconds = seconds;
				return prv;
			});
		}
	}
	
	setTime(){
		var self = this;
		this.setState(function(prv){
			var s = Math.floor(self.state.seconds%60);
			var m = Math.floor((self.state.seconds/60) % 60);
			var h = Math.floor((self.state.seconds/(60*60)) % 24);
			var d = Math.floor(self.state.seconds/(60*60*24));
			
			prv.timer.seconds = s < 10 ? '0' + s : s; 
			prv.timer.minutes = m < 10 ? '0' + m : m;
			prv.timer.hours = h < 10 ? '0' + h : h;
			prv.timer.days = d < 10 ? '0' + d : d;
            if(this.props.countdown)
                prv.seconds -= 1;
            else
                prv.seconds += 1;
			
            if(self.state.seconds<=0){
                clearInterval(this.loadInterval);
            }
			return prv;
		});
	}
	

    render(){
		if(typeof this.props.countdownSlug !== 'undefined' && this.state.seconds <= 0){
			return (<div>{this.props.countdownSlug}</div>);
		}
        return (
            <p>{this.state.timer.days}:{this.state.timer.hours}:{this.state.timer.minutes}:{this.state.timer.seconds}</p>
        )
    }
};
Timer.defaultProps = {
  start: true,
};