import React from 'react'
import {Modal, Button, Icon} from "semantic-ui-react";

export default class BaseFormAction extends React.Component
{
    render(){
		var self = this;
        return (
            <Modal.Actions>
				  <Button size="mini" onClick={function(){
					 self.props.form.props.list.closeModal(self.props.form.props.listControl);
				  }} color='red'>
					<Icon name='remove' /> Нет
				  </Button>
				  <Button size="mini" onClick={this.props.form.handleApprove.bind(this.props.form)} color='green'>
					<Icon name='checkmark' /> Да
				  </Button>
			</Modal.Actions>
        )
    }
};