import React from 'react'

export default class BaseCheckbox extends React.Component {

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
    }

    render() {

        var list = this.props.list;
        var input = <input value="1" type="checkbox" disabled/>

        if (!this.props.fieldDisabled) {
            input = <input name={this.props.field}
                           data-type={this.props.fieldType}
                           value={this.props.fieldValue}
                           checked={this.props.fieldChecked}
                           onChange={this.props.change}
                           type="checkbox"
            />
        }

        return (
            <div className={"ui fitted checkbox " + this.props.fieldClass}>
                {input}
                <label/> 
            </div>
        );
    }

}