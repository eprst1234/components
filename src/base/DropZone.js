import React from 'react'
import Dropzone from 'react-dropzone'

export default class DropZone extends React.Component
{
    /**
     * 
     * @param props
     */
    constructor(props){
        super(props);
        this.state = {
			
        };
    }
	
	readImageFile(file){
		var self = this;
		var loader = new FileReader();	
		loader.onloadend = function (event) {		
			file.content = event.target.result;
			self.props.finishRead(file);
		};
					
		loader.readAsDataURL(file);
	}
	
	onDrop(files) {
        let self = this;
		files.forEach((file)=> {
		  self.readImageFile(file); 
		});
    }

    render() {
        return (
            <Dropzone className="dropzone-product"
                      accept="image/jpeg, image/png"
                      onDrop={this.onDrop.bind(this)}
            >
                <div className="ui stackable five column grid product imgs">
                   <div className="sixteen wide column">
                      <div className="center aligned">
                         <div className="upload images">
                            <div className="dz-message">
                                Перетащите сюда изображения
                            </div>
                            <div className="dz-message">
                                (Или нажмите тут чтобы <strong>выбрать</strong> файлы стандартным способом.)
                            </div>
                        </div>
                     </div>
                   </div>
                </div>
           </Dropzone>
        )
    }
};