import React from 'react';
import {
    Route,
    Link,
    Redirect
} from 'react-router-dom';
import { Auth } from 'components';

export default class PrivateRoute extends React.Component
{
    loginPage = '/auth/login';
    
    render()
    {
        const { component: Component, ...rest } = this.props;

        return (
            <Route {...rest} render={props => (
                Auth.isAuth() ? (
                        <Component {...props}/>
                    ) : (
                        <Redirect to={{
                            pathname: this.loginPage,
                            state: { from: props.location }
                        }}/>
                    )
            )}/>
        );
    }
}