import React from "react";
import {CR} from "components";
import classNames from "classnames";
import {Button, Comment, Icon, Image, Menu} from "semantic-ui-react";

export default class BaseChat extends React.Component {
    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            backToContacts: true,
            showDialogList: false,
            showDialog: false,
            showEmoji: false,
            /*---------------- ▼ Delete for production ▼ ---------------------------*/
            tmpData: {
                contactList: [],
                dialogList: []
            }
            /*---------------- ▲ Delete for production ▲ ---------------------------*/
        };
        CR.setComponent('chat');
    }

    handleContacts() {
        this.setState({backToContacts: true, showDialogList: false})
    }

    handleChatShows() {
        this.setState({backToContacts: false, showDialogList: true})
    }

    handleDialog() {
        this.setState({showDialog: this.state.showDialog ? false : true})
    }

    handleEmoji() {
        this.setState({showEmoji: this.state.showEmoji ? false : true});
    }

    /*---------------- ▼ Delete for production ▼ ---------------------------*/

    componentDidMount() {
        var self = this;
        this.setState({tmpData: {contactList: self.generateContactList(), dialogList: self.generateDialogList()}});
    }

    doingSomething(){
        alert('Doing something');
    }

    getRandomArbitary(min, max) {
        return Math.round(Math.random() * (max - min) + min);
    }

    generateDialogList() {

        var self = this;
        var dialogArray = [];
        var avatars = ['ade.jpg', 'chris.jpg', 'christian.jpg', 'daniel.jpg', 'elliot.jpg', 'helen.jpg', 'jenny.jpg', 'joe.jpg', 'justen.jpg', 'laura.jpg', 'lena.png', 'lindsay.png', 'mark.png', 'matt.jpg', 'matthew.png', 'molly.png', 'nan.jpg', 'nom.jpg', 'rachel.png', 'steve.jpg', 'stevie.jpg', 'tom.jpg', 'veronika.jpg', 'zoe.jpg'];
        var names = ['Ade', 'Chris', 'Christian', 'Daniel', 'Elliot', 'Helen', 'Jenny', 'Joe', 'Justen', 'Laura', 'Lena', 'Lindsay', 'mark', 'matt', 'matthew', 'molly', 'Nan', 'Nom', 'rachel', 'steve', 'stevie', 'tom', 'veronika', 'zoe'];
        var words = ['Interdum', 'et', 'malesuada', 'fames', 'ac', 'ante', 'ipsum', 'primis', 'in', 'faucibus', 'Vivamus', 'ex', 'nibh', 'cursus', 'molestie', 'neque', 'vel', 'placerat', 'maximus', 'quam', 'Mauris', 'tempus', 'luctus', 'enim', 'Vestibulum', 'blandit', 'finibus', 'euismod', 'Curabitur', 'vulputate', 'massa', 'vel', 'ipsum', 'tempor', 'sed', 'laoreet', 'risus', 'tincidunt', 'Duis', 'tempor', 'congue', 'maximus', 'Aenean', 'ac', 'ultricies', 'nibh', 'Interdum', 'et', 'malesuada', 'fames', 'ac', 'ante', 'ipsum', 'primis', 'in', 'faucibus', 'Aliquam', 'erat', 'volutpat', 'Nulla', 'facilisi', 'Curabitur', 'ultricies', 'vel', 'risus', 'ac', 'scelerisque', 'In', 'hac', 'habitasse', 'platea', 'dictumst', 'Donec', 'accumsan', 'vel', 'risus', 'a', 'vestibulum', 'Vestibulum', 'vehicula', 'tellus', 'quis', 'erat', 'rutrum', 'consequat', 'Mauris', 'eu', 'tempor', 'nibh', 'ut', 'pharetra', 'lorem', 'Maecenas', 'id', 'elementum', 'augue', 'Mauris', 'elementum', 'arcu', 'enim', 'at', 'rutrum', 'justo', 'malesuada', 'ut', 'Duis', 'tempus', 'nunc', 'at', 'metus', 'eleifend', 'tristique', 'Curabitur', 'semper', 'gravida', 'est', 'quis', 'venenatis', 'Sed', 'cursus', 'non', 'lorem', 'eu', 'venenatis', 'Sed', 'tempor', 'lectus', 'sit', 'amet', 'finibus', 'ultricies', 'libero', 'massa', 'condimentum', 'ante', 'nec', 'ullamcorper', 'magna', 'tortor', 'a', 'tellus', 'Cras', 'eleifend', 'fermentum', 'felis', 'et', 'accumsan', 'massa', 'tempus', 'id', 'Vivamus', 'nec', 'ipsum', 'fringilla', 'auctor', 'mi', 'at', 'cursus', 'dolor', 'Nulla', 'id', 'rhoncus', 'urna', 'vitae', 'convallis', 'massa', 'Cras', 'consectetur', 'felis', 'sit', 'amet', 'elit', 'ultricies', 'rhoncus', 'Duis', 'ut', 'augue', 'lorem', 'Ut', 'porttitor', 'commodo', 'lectus', 'et', 'finibus', 'Donec', 'quis', 'turpis', 'in', 'nisi', 'varius', 'porta', 'Mauris', 'posuere', 'purus', 'ac', 'purus', 'vulputate', 'eu', 'fermentum', 'quam', 'consectetur', 'Fusce', 'molestie', 'sagittis', 'erat', 'condimentum', 'iaculis'];

        var sentence = function () {
            var string = '';
            for (var i = 0; i < self.getRandomArbitary(2, 30); i++) {
                string += words[Math.floor(Math.random() * words.length)] + ' ';
            }
            return string.trim() + '.';
        }

        for (var i = 0; i < 100; i++) {
            var avatarSrc = '/images/avatar/small/' + avatars[Math.floor(Math.random() * avatars.length)];
            var time = 'Today at ' + this.getRandomArbitary(0, 1) + '' + this.getRandomArbitary(1, 9) + ':' + this.getRandomArbitary(0, 5) + '' + this.getRandomArbitary(0, 9);
            var className = 'my';
            if (self.getRandomArbitary(0, 1) == 0) {
                className = 'interlocutor';
                if (self.getRandomArbitary(0, 1) == 0) {
                    className += ' admin';
                }
            }
            dialogArray.push({
                avatarSrc: avatarSrc,
                name: names[Math.floor(Math.random() * names.length)],
                sentence: sentence(),
                time: time,
                class: className
            });
        }
        return dialogArray;
    }

    generateContactList() {

        var self = this;
        var contactArray = [];
        var avatars = ['ade.jpg', 'chris.jpg', 'christian.jpg', 'daniel.jpg', 'elliot.jpg', 'helen.jpg', 'jenny.jpg', 'joe.jpg', 'justen.jpg', 'laura.jpg', 'lena.png', 'lindsay.png', 'mark.png', 'matt.jpg', 'matthew.png', 'molly.png', 'nan.jpg', 'nom.jpg', 'rachel.png', 'steve.jpg', 'stevie.jpg', 'tom.jpg', 'veronika.jpg', 'zoe.jpg'];
        var names = ['Ade', 'Chris', 'Christian', 'Daniel', 'Elliot', 'Helen', 'Jenny', 'Joe', 'Justen', 'Laura', 'Lena', 'Lindsay', 'mark', 'matt', 'matthew', 'molly', 'Nan', 'Nom', 'rachel', 'steve', 'stevie', 'tom', 'veronika', 'zoe'];

        for (var i = 0; i < 100; i++) {
            var avatarSrc = '/images/avatar/small/' + avatars[Math.floor(Math.random() * avatars.length)];
            var time = 'He(she) was online ' + this.getRandomArbitary(0, 1) + '' + this.getRandomArbitary(1, 9) + ' hours ago ';
            contactArray.push({
                avatarSrc: avatarSrc,
                name: names[Math.floor(Math.random() * names.length)],
                time: time
            });
        }
        return contactArray;
    }

    /*---------------- ▲ Delete for production ▲ ---------------------------*/

    render() {

        var self = this;

        var mainContainerClasses = classNames(this.props.className, {
            'ui': true,
            'chatcontainer': true,
            'dialoglist': this.state.showDialogList,
            'dialog': this.state.showDialog,
        });

        var chatroomDialogClasses = classNames(this.props.className, {
            'ui': true,
            'chatroom': true,
            'dialog': true,
            'emoji': this.state.showEmoji
        });

        /*---------------- ▼ Delete for production ▼ ---------------------------*/
        var icons = ['search', 'mail outline', 'signal', 'setting', 'home', 'inbox', 'browser', 'tag', 'tags', 'image', 'calendar', 'comment', 'shop', 'comments', 'external', 'privacy', 'settings', 'trophy', 'payment', 'feed', 'alarm outline', 'tasks', 'cloud', 'lab', 'mail', 'dashboard', 'comment outline', 'comments outline', 'sitemap', 'idea', 'alarm', 'terminal', 'code', 'protect', 'calendar outline', 'ticket', 'external square', 'bug', 'mail square', 'history', 'options', 'text telephone', 'find', 'wifi', 'alarm mute', 'alarm mute outline', 'copyright', 'at', 'eyedropper', 'paint brush', 'heartbeat', 'mouse pointer', 'hourglass empty', 'hourglass start', 'hourglass half', 'hourglass end', 'hourglass full', 'hand pointer', 'trademark', 'registered', 'creative commons', 'add to calendar', 'remove from calendar', 'delete calendar', 'checked calendar', 'industry', 'shopping bag', 'shopping basket', 'hashtag', 'percent', 'address book', 'address book outline', 'address card', 'address card outline', 'id badge', 'id card', 'id card outline', 'podcast', 'window close', 'window close outline', 'window maximize', 'window minimize', 'window restore', 'wait', 'download', 'repeat', 'refresh', 'lock', 'bookmark', 'print', 'write', 'adjust', 'theme', 'edit', 'external share', 'ban', 'mail forward', 'share', 'expand', 'compress', 'unhide', 'hide', 'random', 'retweet', 'sign out', 'pin', 'sign in', 'upload', 'call', 'remove bookmark', 'call square', 'unlock', 'configure', 'filter', 'wizard', 'undo', 'exchange', 'cloud download', 'cloud upload', 'reply', 'reply all', 'erase', 'unlock alternate', 'write square', 'share square', 'archive', 'translate', 'recycle', 'share alternate', 'share alternate square', 'add to cart', 'in cart', 'add user', 'remove user', 'object group', 'object ungroup', 'clone', 'help circle', 'info circle', 'warning circle', 'warning sign', 'announcement', 'help', 'info', 'warning', 'birthday', 'help circle outline', 'user', 'users', 'doctor', 'handicap', 'student', 'child', 'spy', 'user circle', 'user circle outline', 'user outline', 'female', 'male', 'woman', 'man', 'non binary transgender', 'intergender', 'transgender', 'lesbian', 'gay', 'heterosexual', 'other gender', 'other gender vertical', 'other gender horizontal', 'neuter', 'genderless', 'universal access', 'wheelchair', 'blind', 'audio description', 'volume control phone', 'braille', 'asl', 'assistive listening systems', 'deafness', 'sign language', 'low vision', 'block layout', 'grid layout', 'list layout', 'zoom', 'zoom out', 'resize vertical', 'resize horizontal', 'maximize', 'crop', 'cocktail', 'road', 'flag', 'book', 'gift', 'leaf', 'fire', 'plane', 'magnet', 'lemon', 'world', 'travel', 'shipping', 'money', 'legal', 'lightning', 'umbrella', 'treatment', 'suitcase', 'bar', 'flag outline', 'flag checkered', 'puzzle', 'fire extinguisher', 'rocket', 'anchor', 'bullseye', 'sun', 'moon', 'fax', 'life ring', 'bomb', 'soccer', 'calculator', 'diamond', 'sticky note', 'sticky note outline', 'law', 'hand peace', 'hand rock', 'hand paper', 'hand scissors', 'hand lizard', 'hand spock', 'tv', 'thermometer empty', 'thermometer full', 'thermometer half', 'thermometer quarter', 'thermometer three quarters', 'bath', 'snowflake outline', 'crosshairs', 'asterisk', 'square outline', 'certificate', 'square', 'quote left', 'quote right', 'spinner', 'circle', 'ellipsis horizontal', 'ellipsis vertical', 'cube', 'cubes', 'circle notched', 'circle thin', 'checkmark', 'remove', 'checkmark box', 'move', 'add circle', 'minus circle', 'remove circle', 'check circle', 'remove circle outline', 'check circle outline', 'plus', 'minus', 'add square', 'radio', 'minus square', 'minus square outline', 'check square', 'selected radio', 'plus square outline', 'toggle off', 'toggle on', 'film', 'sound', 'photo', 'bar chart', 'camera retro', 'newspaper', 'area chart', 'pie chart', 'line chart', 'arrow circle outline down', 'arrow circle outline up', 'chevron left', 'chevron right', 'arrow left', 'arrow right', 'arrow up', 'arrow down', 'chevron up', 'chevron down', 'pointing right', 'pointing left', 'pointing up', 'pointing down', 'arrow circle left', 'arrow circle right', 'arrow circle up', 'arrow circle down', 'caret down', 'caret up', 'caret left', 'caret right', 'angle double left', 'angle double right', 'angle double up', 'angle double down', 'angle left', 'angle right', 'angle up', 'angle down', 'chevron circle left', 'chevron circle right', 'chevron circle up', 'chevron circle down', 'toggle down', 'toggle up', 'toggle right', 'long arrow down', 'long arrow up', 'long arrow left', 'long arrow right', 'arrow circle outline right', 'arrow circle outline left', 'toggle left', 'tablet', 'mobile', 'battery full', 'battery high', 'battery medium', 'battery low', 'battery empty', 'power', 'trash outline', 'disk outline', 'desktop', 'laptop', 'game', 'keyboard', 'plug', 'trash', 'file outline', 'folder', 'folder open', 'file text outline', 'folder outline', 'folder open outline', 'level up', 'level down', 'file', 'file text', 'file pdf outline', 'file word outline', 'file excel outline', 'file powerpoint outline', 'file image outline', 'file archive outline', 'file audio outline', 'file video outline', 'file code outline', 'qrcode', 'barcode', 'rss', 'fork', 'html5', 'css3', 'rss square', 'openid', 'database', 'server', 'usb', 'bluetooth', 'bluetooth alternative', 'microchip', 'heart', 'star', 'empty star', 'thumbs outline up', 'thumbs outline down', 'star half', 'empty heart', 'smile', 'frown', 'meh', 'star half empty', 'thumbs up', 'thumbs down', 'music', 'video play outline', 'volume off', 'volume down', 'volume up', 'record', 'step backward', 'fast backward', 'backward', 'play', 'pause', 'stop', 'forward', 'fast forward', 'step forward', 'eject', 'unmute', 'mute', 'video play', 'closed captioning', 'pause circle', 'pause circle outline', 'stop circle', 'stop circle outline', 'marker', 'coffee', 'food', 'building outline', 'hospital', 'emergency', 'first aid', 'military', 'h', 'location arrow', 'compass', 'space shuttle', 'university', 'building', 'paw', 'spoon', 'car', 'taxi'];
        /*---------------- ▲ Delete for production ▲ ---------------------------*/

        return (
            <div className={mainContainerClasses}>
                <div className="ui chatbox">
                    <div className="ui chatroom contacts">
                        <div className="actions">
                            <div className="message">
                                Контакты
                            </div>
                            <Button.Group color='black' size='mini' floated='right'>
                                <Button onClick={this.doingSomething.bind(this)} icon='font'/>
                                <Button onClick={this.doingSomething.bind(this)} icon='bold'/>
                            </Button.Group>
                        </div>
                        <div className="room">
                            <Comment.Group>
                                {
                                    this.state.tmpData.contactList.map(function (contact, i) {
                                        return (
                                            <Comment key={i} onClick={self.handleDialog.bind(self)}>
                                                <Comment.Avatar src={contact.avatarSrc}/>
                                                <Comment.Content>
                                                    <Comment.Author as='a'>{contact.name}</Comment.Author>
                                                    <Comment.Text>{contact.time}</Comment.Text>
                                                </Comment.Content>
                                            </Comment>
                                        );
                                    })
                                }
                            </Comment.Group>
                        </div>
                    </div>
                    <div className="ui chatroom dialoglist">
                        <div className="actions">
                            <div className="message">
                                Чаты
                            </div>
                            <Button.Group color='black' size='mini' floated='right'>
                                <Button onClick={this.doingSomething.bind(this)} icon='italic'/>
                                <Button onClick={this.doingSomething.bind(this)} icon='underline'/>
                            </Button.Group>
                        </div>
                        <div className="room">
                            <div className="container">
                                <Comment.Group>
                                    {
                                        this.state.tmpData.dialogList.map(function (dialog, i) {
                                            return (
                                                <Comment key={i} onClick={self.handleDialog.bind(self)}>
                                                    <Comment.Avatar src={dialog.avatarSrc}/>
                                                    <Comment.Content>
                                                        <Comment.Author as='a'>{dialog.name}</Comment.Author>
                                                        <Comment.Metadata>
                                                            <div>{dialog.time}</div>
                                                        </Comment.Metadata>
                                                        <Comment.Text>{dialog.sentence}</Comment.Text>
                                                    </Comment.Content>
                                                </Comment>
                                            );
                                        })
                                    }
                                </Comment.Group>
                            </div>
                        </div>
                    </div>
                    <div className={chatroomDialogClasses}>
                        <div className="actions">
                            <div className="back" onClick={this.handleDialog.bind(this)}>
                                <Icon size="mini" name="angle left"/>
                                { this.state.backToContacts ? 'Назад' : 'Чаты'}
                            </div>
                            <div className="title">
                                <div className="header">Schwarzenegger Arnold Noguzaderedrishenkovich</div>
                                <div className="subheader">Status: I'am the Terminator, and I'll be back</div>
                            </div>
                            <div className="ui right floated avatar">
                                <Image mini="true" src="/images/avatar/small/terminator.png"/>
                            </div>
                        </div>
                        <div className="room">
                            <Comment.Group>
                                {
                                    this.state.tmpData.dialogList.map(function (dialog, i) {
                                        return (
                                            <Comment key={i} className={dialog.class}>
                                                <Comment.Metadata>
                                                    <Comment.Author as='a'>{dialog.name}</Comment.Author>
                                                    <div>{dialog.time}</div>
                                                </Comment.Metadata>
                                                <Comment.Content>
                                                    <Comment.Text>{dialog.sentence}</Comment.Text>
                                                </Comment.Content>
                                            </Comment>
                                        );
                                    })
                                }
                            </Comment.Group>
                        </div>
                        <div className="talk">
                            <div className="emoji-handler" onClick={this.handleEmoji.bind(this)}>
                                <img src="/images/icons/emoji.png"/>
                            </div>
                            <input type="text" maxLength="200"/>
                            <div className="ui send button">
                                <i className="ui icon comment"></i>
                            </div>
                        </div>
                        <div className="emoji-box">
                            <div className="emoji-list">
                                {
                                    icons.map(function (icon, i) {
                                        return (
                                            <Icon key={i} name={icon}/>
                                        );
                                    })
                                }
                            </div>
                            <div className="emoji-list-triangle"></div>
                        </div>
                    </div>
                    <div className="ui boxmenu">
                        <Menu secondary fluid widths={3} icon='labeled'>
                            <Menu.Item name='contacts' active={!this.state.showDialogList} onClick={this.handleContacts.bind(this)}>
                                <Icon name='users'/>
                                Контакты
                            </Menu.Item>
                            <Menu.Item name='chatshows' active={this.state.showDialogList} onClick={this.handleChatShows.bind(this)}>
                                <Icon name='comments'/>
                                Чаты
                            </Menu.Item>
                            <Menu.Item name='settings'>
                                <Icon name='settings'/>
                                Настройки
                            </Menu.Item>
                        </Menu>
                    </div>
                </div>
            </div>
        )
    }

}