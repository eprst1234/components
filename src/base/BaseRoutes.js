import React from 'react';
import { Router, Route, IndexRoute, IndexLink, Link, browserHistory } from 'react-router';
import Api from './../api/Api';
import Profile from './../helper/Profile';
// import BaseAppLayout from './BaseAppLayout';
// import BaseCrypto from './BaseCrypto';
// import AppHelpers from "./AppHelpers";

export default class BaseRoutes extends React.Component
{
    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
		this.routes = [];
        this.state = {
            component: null,
            user: null,
			auth: null,
        };
    }

    /**
     *
     * @param component
     */
    static setBaseComponent(component)
    {
        this.state.component = component;
    }

    /**
     *
     * @returns {null}
     */
    getBaseComponent()
    {
        return this.state.component;
    }
	
    /**
     *
     */
    componentDidMount() {
        var self = this;
		
		//Profile.request(function(){
		//	self.forceUpdate();
		//}, function(){
		//	self.forceUpdate();
		//});

		window.onerror = function(message, file, line, column, error) {
			var errorData = {
				message: message,
				file: file,
				line: line,
				column: column,
				trace: error.stack,
				user_agent: navigator.userAgent,
			};

			Api.makeRequest({
				type: "POST",
				url: window.laroute.route("error.logs"),
				cache: false,
				data: errorData
			});

			console.log(errorData);
		};
			
			
		function proxy(context, method, message) {
		  return function() {
			var errorData = {
					message: arguments[0],
					trace: self.getStackTrace(),
					user_agent: navigator.userAgent,
				};

			  Api.makeRequest({
				  type: "POST",
				  url: window.laroute.route("error.logs"),
				  cache: false,
				  data: errorData,
			  });

			method.apply(context, [message].concat(Array.prototype.slice.apply(arguments)))
		  }
		}
	}
	
	
	getStackTrace() {
	  try {
		  var obj = {};
		  Error.captureStackTrace(obj);
		  return obj.stack;
	  }catch(err){
		  return 'Not stack';
	  }
	}

    /**
     * 
     * @returns {XML}
     */
    render()
    {
        var self = this;
		var index = '';

        if(this.state.user){
            BaseAppLayout.setUser(this.state.user);
			//console.log('user', BaseAppLayout.getUser());
			var role = this.state.user.main_role.slug;
            this.routes = BaseRoutes.functRoutes(role);
			this.index = BaseRoutes.funcIndex(role);
			var component = BaseRoutes.funcAppLayout(role);

            return (
                <Router history={browserHistory}>
                    <Route path="/backend" component={component}>
                        {this.index}
                        {this.routes.map(function (item, i) {
                            return (<Route key={i} name={item.name} path={item.path} component={item.component}/>);
                        })}
                    </Route>
                </Router>
            );
        }else{
           return (<div></div>);
        }
    }
}