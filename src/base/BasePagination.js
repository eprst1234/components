import React from 'react'

export default class BasePagination extends React.Component {

    /**
     *
     * @param page
     */
    handlePage(page)
    {   
        this.props.change(page);
    }

    /**
     * 
     * @returns {XML}
     */
    render() 
    {
        var paginator = this.props.paginator;
        var items = [];

        if(paginator.last_page < 2){
            return (
                <div className="item right"></div>
            );
        }else{
            for(var i = 1; i <= paginator.last_page; i++)
            {
                var half_total_links = Math.floor(5 / 2);
                var from = paginator.current_page - half_total_links;
                var to = paginator.current_page + half_total_links;

                if (paginator.current_page < half_total_links) {
                    to += half_total_links - paginator.current_page;
                }
                if (paginator.last_page - paginator.current_page < half_total_links) {
                    from -= half_total_links - (paginator.last_page - paginator.current_page) - 1;
                }

                if(paginator.current_page === i){
                    items.push(<div className="item active" key={i}>{i}</div>);
                    continue;
                }

                if (from < i && i < to) {
                    items.push(<a className={paginator.current_page == i ? 'active item' : 'item'} key={i} onClick={this.handlePage.bind(this, i)}>{i}</a>);
                    continue;
                }
            }

            return (
                <div className="item right">
                    <div className="ui pagination menu">
                        <a className={(paginator.current_page == 1) ? 'item disabled' : 'item' } onClick={this.handlePage.bind(this, 1)}>Первая</a>
                        {items}
                        <a className={(paginator.current_page == paginator.last_page) ? 'item disabled' : 'item'} onClick={this.handlePage.bind(this, paginator.last_page)}>Последняя</a>
                    </div>
                </div>
            );
        }
    }
}