import React from 'react'

export default class InputSmiles extends React.Component
{
    /**
     * 
     * @param props
     */
    constructor(props){
        super(props);
        this.state = {
			kemoji: '',
        };
    }
	
	componentDidMount(){
		var self = this;
		this.setState(function(prv){
			prv.kemoji = KEmoji.init(self.props.idTarget, {
                            height: 200,
                            smileContainerWidth: 280, 
                            smileContainerHeight: 150,
                            smiles: ["D83DDE0A","D83DDE03","D83DDE09","D83DDE06","D83DDE1C","D83DDE0B","D83DDE0D","D83DDE0E","D83DDE12","D83DDE0F","D83DDE14","D83DDE22","D83DDE2D","D83DDE29","D83DDE28","D83DDE10","D83DDE0C","D83DDE04","D83DDE07","D83DDE30","D83DDE32","D83DDE33","D83DDE37","D83DDE02","2764","D83DDE1A","D83DDE15","D83DDE2F","D83DDE26","D83DDE35","D83DDE20","D83DDE21","D83DDE1D","D83DDE34","D83DDE18","D83DDE1F","D83DDE2C","D83DDE36","D83DDE2A","D83DDE2B","263A","D83DDE00","D83DDE25","D83DDE1B","D83DDE16","D83DDE24","D83DDE23","D83DDE27","D83DDE11","D83DDE05","D83DDE2E","D83DDE1E","D83DDE19","D83DDE13","D83DDE01","D83DDE31","D83DDE08","D83DDC7F","D83DDC7D","D83DDC4D","D83DDC4E","261D","270C","D83DDC4C","D83DDC4F","D83DDC4A","270B","D83DDE4F","D83DDC43","D83DDC46","D83DDC47"]
                        });
		});
	}
	
	

    render(){
		var hide = 'block';
		if(this.props.hide == true)
			hide = 'none';
		
        return (
            <div style={{display: hide}} onKeyPress={typeof this.props.onKeyPress !== 'undefined' ? this.props.onKeyPress.bind(this) : ''} id={this.props.idTarget}></div>
        )
    }
};