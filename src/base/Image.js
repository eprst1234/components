import React from 'react'
import {
    Image as BaseImage
} from 'semantic-ui-react';

export default class Image extends React.Component
{
    /**
     * 
     * @param props
     */
    constructor(props){
        super(props);
        this.state = {
			
        };
    }

    render(){
        return (
            <BaseImage {...this.props} src={process.env.MARKET_HOST + '/' + this.props.src}/>
        )
    }
};