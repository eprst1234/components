import React from 'react';
import PropTypes from 'prop-types';
import { Message } from 'semantic-ui-react';

/**
 * Class FormMessages.
 */
export default class FormMessages extends React.Component
{
    /**
     * Constructor.
     *
     * @param props
     */
    constructor(props){
        super(props);

        this.state = {
            messages: {
                header: '',
                type: 'error',
                style: { display: 'block' },
                list: [],
            }
        };
    }

    static propTypes = {
        parent: PropTypes.object,
        withInputValidation: PropTypes.bool,
        errors: PropTypes.object,
    };

    static defaultProps = {
        withInputValidation: false,
        errors: {},
    };


    /**
     * Return header form.
     *
     * @param header
     * @returns {string}
     */
    getMessagesHeader(header = '') {
        const { type } = this.state.messages;
        let result = '';

        if (header.length > 0) {
            result = header;
        } else {
            switch (type) {
                case 'success':
                    result = 'Успех!';
                    break;
                case 'error':
                    result = 'Ошибки в форме:';
                    break;
            }
        }

        return result;
    }

    /**
     * Show form messages.
     *
     * @param texts
     * @param header
     * @param type
     */
    show(texts, header = '', type = 'error') {
        let messages = { ...this.state.messages };
        let formErrors = {};

        messages.type = type;
        messages.list = [];
        messages.header = this.getMessagesHeader(header);
        //Object.keys(formErrors).forEach(key => formErrors[key] = false);

        if (typeof texts === 'string' || texts instanceof String) {
            messages.list.push(texts);
        } else if (texts instanceof Array) {
            messages.list = texts;
        } else {
            Object.entries(texts).forEach(
                ([key, value]) => {
                    if (this.props.withInputValidation) formErrors[key] = true;
                    messages.list.push(value[0]);
                }
            );
        }

        if (this.props.withInputValidation && typeof this.props.parent !== 'undefined') {
            this.props.parent.setState({ formErrors });
        }
        this.setState({ messages });
    }

    /**
     * Render.
     *
     * @returns {XML}
     */
    render() {
        const { style = {}, type, header, list } = this.state.messages;
        const { errors } = this.props;

        let message = null;
        let errorList = list.concat(Object.values(errors).map(item => item[0]));

        if (typeof errorList !== 'undefined' && 0 in errorList) {
            message = <Message
                style={{ ...style }}
                error={ type === 'error' }
                positive={ type === 'success' }
                header={ header.length > 0 ? header : this.getMessagesHeader() }
                list={ errorList }
            />;
        }

        return (
            <div>
                { message }
            </div>
        )
    }
};