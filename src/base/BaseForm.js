import React from "react";
import {Api, BaseHeader, FormMessages,BaseDropdown,ImagesAdd} from "components";
import {Segment, Container, Header, Form, Input, Button, Icon, Grid,TextArea} from 'semantic-ui-react';
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';

export default class BaseForm extends React.Component {

    constructor(props) {
        super(props);
        this.defaultParams = {
            constFields: {},
            redirect: this.props.history.goBack,
            inline: true,
            isEdit: false,

            header: {
                content: '',
                breadcrumb: [],
            }
        };

        this.state = {};
    }

    getParams() {
        return {...this.defaultParams, ...this.params};
    }

    getFormData() {
        return {...this.state.form, ...this.getParams().constFields};
    }

    componentDidMount() {
        this.getData();
    }

    getEditData(){
        Api.get(this.getParams().uri, 'index')
            .where('id', this.getParams().id)
            .first()
            .bind(this, 'form');
    }

    getData(){
        if (this.getParams().isEdit) {
            this.getEditData();
        } else {
            let form = {};
            this.getParams().editedFields.map(item => {
                form[item['name']] = '';
            });

            this.setState({...this.state, ...{form: {...form}}});
        }
    }

    handleChange = (e, {name, value}) => {
        this.setState({form: {...this.state.form, [name]: value}});
        this.setState({errors: {}});
    };

    handleChangeDropdown = (value, data) => {
        let {name} = data;
        this.setState({form: {...this.state.form, [name]: value}});
    };

    handleSubmit = () => {
        let self = this;
        if (!this.getParams().isEdit) {
            Api.post(this.getParams().uri, 'store', this.getFormData()).call(() => this.getParams().redirect()).withNotify().withValidateForm(this);
        } else {
            Api.putArg(this.getParams().uri, 'update', this.state.form.id, this.getFormData()).call(() => this.getParams().redirect()).withNotify().withValidateForm(this);
        }
    };

    breadcrumb = () => {
        const { header = {} } = this.getParams();

        let breadcrumbs = [];
        if (typeof header.breadcrumbs !== 'undefined' && Array.isArray(header.breadcrumbs)) {
            let arrayLength = header.breadcrumbs.length;

            for (let i = 0; i < arrayLength; i++) {
                let obj = { key: 'k-' + i, content: header.breadcrumbs[i]['content'] };

                if ('to' in header.breadcrumbs[i] && header.breadcrumbs[i]['to'].length > 0) {
                    obj['as'] = Link;
                    obj['to'] = header.breadcrumbs[i]['to'];
                }

                if (i !== 0 && i !== (arrayLength - 1)) {
                    breadcrumbs.push({key: 'd-' + i, divider: 'right chevron'});
                } else if (i !== 0) {
                    breadcrumbs.push({key: 'd-' + i, divider: 'right arrow'},);
                }

                breadcrumbs.push(obj);
            }
        }

        return breadcrumbs;
    };

    getType(item) {
        const {form} = this.state;
        let inp = '';
        let self = this;
        let value = '';

        if (typeof form[ item[ 'name' ] ] !== 'undefined' && form[ item[ 'name' ] ] !== null) {
            value = form[ item[ 'name' ] ];
        }

        switch (item.type) {
            case 'input':
                inp = (
                    <Input placeholder={item.placeholder ? item.placeholder : ''}
                           name={item.name}
                           value={ value }
                           onChange={self.handleChange}
                    />
                );
                break;
            case 'textarea':
                inp = (
                    <TextArea
                        onChange={self.handleChange}
                        name={item.name}
                        value={ value }
                        placeholder={item.placeholder}
                        type={item.type}/>
                );
                break;
            case 'select':
                inp = (
                    <BaseDropdown
                        value={ value }
                        placeholder={item.placeholder}
                        name={item.name}
                        onChange={self.handleChangeDropdown.bind(self)}
                        options={{
                            data: this.state[item.data],
                            value: 'id',
                            text: 'name'
                        }}
                    />
                );
                break;
            case 'image':
                inp = (
                    <ImagesAdd
                        name={item.name}
                        propsPreview={{call: item.propsPreview}}
                        preview={item.preview}
                        ref="images"
                    />
                );
                break;
        }
        return inp;
    }

    getField(item){
        const { errors = {} } = this.state;

        return(<Form.Field
            inline={this.getParams().inline}
            error={item['name'] in errors}
            required={item.required}
        >
            <label>{ item.label ? item.label : '' }</label>
            {this.getType(item)}
        </Form.Field>);
    }

    getFields(){
        let self = this;
        const {editedFields, panelIcon, head: title, inline} = this.getParams();
        const {form, errors = {}} = this.state;

        return (<div>
            {editedFields.map((item, key) => {
                if (item['name'] in form) {
                    return (<div key={key}>
                            {self.getField(item)}
                        </div>
                    );
                }
            })}
        </div>)
    }

    form = () => {
        let self = this;
        const {header, editedFields, panelIcon, inline} = this.getParams();
        const {form, errors = {}} = this.state;

        if (!('form' in this.state)) {
            return null;
        }

        return (
            <div>
                <Header as='h4' attached='top' block>
                    <Icon name={panelIcon}/> {header.content}
                </Header>
                <Segment attached>
                    <Form>
                        <Grid centered>
                            <Grid.Column width={15}>

                                <FormMessages errors={errors}/>

                                {this.getFields()}
                            </Grid.Column>
                        </Grid>
                    </Form>
                </Segment>
            </div>
        );

    };

    render() {
        const { header } = this.getParams();

        return (
            <Container className="page">

                <BaseHeader
                    title={header.content}
                    breadcrumb={this.breadcrumb()}
                    buttons={
                        <Button
                            onClick={this.handleSubmit.bind(this)}
                            positive
                        >
                            <Icon name="save"/> Save
                        </Button>
                    }
                />

                {this.form()}

            </Container>
        )
    }
}

/**
 *
 * Пример использование header, breadcrumbs

constructor(props) {
    super(props);
    this.params = {
        header: {
            content: 'Добавить администратора',
            breadcrumbs: [
                { content: 'Главная', to: '/' },
                { content: 'Администраторы', to: '/users/administrators' },
                { content: 'Добавить администратора' },
            ]
        },
        ...
    };
}

*/


/*
example

    inline
для basedropdown неоходимо запрос для данных необходимо выполнить в конструкторе файла

    constructor(props) {
        super(props);
        this.state = {
            cities: []
        }


        Api.get('city', 'index')
            .all()
            .bind(this, 'cities');


    const editedField = [
        { type: 'input', name: 'name', label: 'Название' },
    ];


   <BaseForm
        uri='region'
        head='This my Form'
        isEdit={true}
        id={this.props.match.params.id}
        goBack={this.props.history.goBack}
        editedFields={ editedField }
    />
 */