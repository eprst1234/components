import React from 'react'
import {Api, Image} from 'components'
import Slider from 'react-slick'
import {Container} from "semantic-ui-react";


export default class Carousel extends React.Component {

    constructor(props) {
        super(props)
        this.state = {}
    }


    componentDidMount() {
        Api.get('product-image', 'index')
            .where('product_id', this.props.id)
            .all().bind(this, 'images')
    }


    render() {
        var self = this
        if (this.state.images != undefined) {
            const settings = {
                customPaging: function (i) {
                    return <a><Image src={self.state.images[i].file}/></a>
                },
            dots: true,
                dotsClass: 'slick-dots slick-thumb',
            infinite: true,
            speed: 500,
            slidesToShow: 1,
                slidesToScroll: 1
        };
            return (
                <Container>
                        <Slider  {...settings}>
                            {this.state.images.map(function (data) {
                                return (<div key={data.id}><Image src={data.file}/></div>)
                            })}
                        </Slider>
                </Container>
            )
        } else {
            return (<div></div>)
        }
    }

}
