﻿import React from 'react'

export default class BaseCityLoader extends React.Component
{
	
	/**
     *
     * @param props
     */
    constructor(props){
        super(props);
        this.state = {
            
        };
    }
	
	
	componentDidMount()
	{
		
		//TODO
		var self = this;
		var url = window.laroute.route("api.v1." + this.props.list.props.parent.getUserRole() + ".orders.index");
        url = url + '?like[id]={query}%25&paginate=false';
		$('.ui.dropdown.search').dropdown({ 
				minCharacters : 1, 
				cache : false, 
				apiSettings: { 
					cache : false, 
					action: 'search', 
					url: url,
					onResponse: function(response) {
						var data = { results: [] }; 
						$.each(response.data, function(index, item) { 
							data.results.push({name: item.id, value: item.id}); 
						}); 
						return data; 
					}, 
				}, 
				fields: { 
					remoteValues : 'results',
					values : 'values', 
					name : 'value',
					value : 'value' 
				}
				
			}).dropdown('bind touch events');
	}
	
	
	render() {
		return(<div></div>);
	}
	
	
}