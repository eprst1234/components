import React from 'react'
import moment from 'moment'
import 'moment/locale/ru';
moment.locale('ru');

export default class BaseState extends React.Component {

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {};
    }
	
	componentWillMount() {
		if(this.state.filterCity != false){
			this.props.parent.getDopModels(this, 'cities', {paginate: false});
		}
    }

    componentDidMount() {
        this.handleWatch();
		$('.moment').addClass('basic');
		$('.moment').first().removeClass('basic');
    }

    handleChangeDate(field, date) {
        this.setState(function (previousState, currentProps) {
            previousState[field] = date;
            return;
        });
    }

    handleFastSelectDate(day) {
        var self = this;
        this.setState(function (previousState, currentProps) {
            previousState.date_start = moment().subtract(day, 'days');
            previousState.date_end = moment();
            return;
        }, function () {
            self.statistiks();
        });

    }

    sumArr(field) {
        var sum = 0;
        this.state.items.map(function (data, i) {
            sum += parseFloat(data[field]);
        });
        return sum;
    }

    handleWatch() {
        this.statistiks();

        $('.moment').click(function(e){

            $('.moment').removeClass('basic').addClass('basic');
            $(e.currentTarget).removeClass('basic');

        });

    }
	
	cityFilter(event){
		var self = this;
		var val = event.target.value;
		this.setState(function(previousState, currentProps) {
			previousState.city_id = val;
			return previousState;
		},function(){
			self.statistiks();
			self.callbackCityFilter();
		})
	}
	
	callbackCityFilter(){
		
	}

    render() {

        return (
            <div>
            </div>
        );
    }


}