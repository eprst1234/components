import React from 'react'
import BaseIndexCharts from './BaseIndexCharts'
import OrderStateChart from './Charts/OrderStateChart'
import WorkStateChart from './Charts/WorkStateChart'
import CuratorStateChart from './Charts/CuratorStateChart'

export default class BaseIndex extends React.Component 
{
	constructor(props) {
        super(props);
        this.state = {
            header: "Панель",
			charts: [],
        };
    }
	
    render() 
    {
		var self = this;
        return (
            <div>
                <h3 className="ui header">{this.state.header}</h3>
				{this.state.charts.map(function(data, i){
					var chart = '';
					var ref='';
					switch(data.slug){
						case 'order': ref = "orderChart"; chart = (
							<OrderStateChart ref={ref}/>
						); break;
						
						case 'work': ref = "workChart"; chart = (
							<WorkStateChart ref={ref}/>
						); break;
						
						case 'curator': ref = "curatorChart"; chart = (
							<CuratorStateChart ref={ref}/>
						); break;
					}
					
					return (<div key={i}>
						<h3>{data.title}</h3>
						<BaseIndexCharts main={self} refChart={ref}>
							{chart}
						</BaseIndexCharts>
					</div>)
				})}
					{/*<h3>График заказов</h3>
				<BaseIndexCharts main={this} refChart='orderChart'>
					<OrderStateChart ref="orderChart"/>
				</BaseIndexCharts>
				<h3>График отработаных часов</h3>
				<BaseIndexCharts main={this} refChart='workChart'>
					<WorkStateChart ref="workChart"/>
				</BaseIndexCharts>
				<h3>График кураторов</h3>
				<BaseIndexCharts main={this} refChart='curatorChart'>
					<CuratorStateChart ref="curatorChart"/>
					</BaseIndexCharts>*/}
            </div>
        );
    }
}
