import React from 'react'
import {
    Button, 
    Header, 
    Icon, 
    Modal 
} from 'semantic-ui-react'

import BaseDropdown from "../base/BaseDropdown";
import BaseFormAction from "../base/BaseFormAction";

export default class BaseAdd extends React.Component {
    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            status: "create",
            visible: false,
            form: {

            },
            formFields: {
                
            },
            headers: []
        };
    }
	defaultProps(){
		
	}
	
	handleChangeDropdown(value, data, event){
		
		if(event.currentTarget.closest('.field').classList.contains('error'))
        {
            event.currentTarget.closest('.field').classList.remove('error');
            if(event.currentTarget.closest('.field').nextElementSibling.classList.contains('message')) {
                event.currentTarget.closest('.field').nextElementSibling.remove();
            }
        }
        event.persist();
		
		this.setState(function(previousState, currentProps) {
            return previousState.form[data.name] = value;
        });
	}
	
	handleChangeCustomFields(object, field, event){
		var val = event.target.value;
		this.setState(function(previousState, currentProps) {
			object[field] = val;
        });
	}
	
	pushObject(arr, object){
		this.setState(function(previousState, currentProps) {
			arr.push(Object.create(object));
		});
	}
	
	deleteObject(arr, i){
		this.setState(function(previousState, currentProps) {
			arr.splice(i, 1);
		});
	}

    /**
     *
     * @param event
     */
    handleClearForm(event) {
		
		$('.ui.dropdown').dropdown('restore defaults');
		this.clearErrorValidate();
        var self = this;
        this.setState(function(previousState, currentProps) {
            return previousState.form = Object.create(self.state.formFields);
        });
		try{
			tinyMCE.activeEditor.setContent('');
		}catch(e){};
		this.clearFormArray();
    }
	
	
	
	clearFormArray(){
		
	}

    /**
     *
     * @param data
     */
    handleFillForm(data)
    {
        this.clearErrorValidate();
        this.setState(function(previousState, currentProps) {
            previousState.form = data;
            previousState.status = "update";
			return;
        });
		
		try{
			tinyMCE.activeEditor.setContent(data.content);
		}catch(e){};
		
        this.fillDropdown();
    }
	
    fillDropdown()
    {
        //emptyff2212
    }

    /**
     *
     * @param event
     */
    handleApprove(event) {
        event.preventDefault();
        var self = this;
		var form = this.state.form;
		form = this.formatDate(form);
		
        if(this.state.status === "create"){
            this.props.list.props.parent.addModel(this, form, this.state.headers,
                function () {
                   // self.handleClearForm();
					self.props.list.setState(function(previousState, currentProps) {
                       previousState[self.props.listControl] = false;
                    });
                    $('.ui.add.modal').modal('hide');
                    self.fixDimmer();
					self.approveCallback();
                }, function (r) {
                    self.errorValidate(r);
                });
        }else if(this.state.status === "update"){
			
            this.props.list.props.parent.updateModel(this, this.state.form.id, form, this.state.headers,
                function () {
                    self.handleClearForm();
					self.props.list.setState(function(previousState, currentProps) {
                       previousState[self.props.listControl] = false;
                    }, function(){
						console.log(self.props.list);
					});
					
                    $('.ui.add.modal').modal('hide');
                    self.fixDimmer();
					self.approveCallback();
                }, function (r) {
                    self.errorValidate(r);
                });
        }
    }
	
	addCustomModal(modal){
		//event.preventDefault();
        var self = this;
		var form = this.state.form;
		form = this.formatDate(form);
        if(this.state.status === "create"){
			AppHelpers.makeRequest({
				type: "POST",
				url: window.laroute.route("api.v1." + this.props.list.props.parent.getUserRole() + "."+modal+".store"),
				data: form,
				success: function (response){
					self.props.list.setState(function(previousState, currentProps) {
						previousState[self.props.listControl] = false;
					});
					self.handleClearForm();
					self.props.list.handleWatch();
					self.props.list.props.parent.refs.notify.addNotification({
						message: response.meta.text,
						level: 'success'
					});
				},
				error: function(r){
					self.errorValidate(r.responseJSON);
				}
			});
		}
		else{
			var route = {};
			route[modal] = this.state.form.id;
			
			AppHelpers.makeRequest({
				type: "PUT",
				url: window.laroute.route("api.v1." + this.props.list.props.parent.getUserRole() + "."+modal+".update", route),
				data: form,
				success: function (response){
					self.props.list.setState(function(previousState, currentProps) {
						previousState[self.props.listControl] = false;
					});
					self.handleClearForm();
					self.props.list.handleWatch();
					self.props.list.props.parent.refs.notify.addNotification({
						message: response.meta.text,
						level: 'success'
					});
				},
				error: function(r){
					self.errorValidate(r.responseJSON);
				}
			});
		}
	}
	
	approveCallback(){
		
	}
	
	formatDate(form){
		return form;
	}

    /**
     *
     * @param event
     */
    handleChangeProperty (event) {
       
        if($(event.currentTarget).closest('.field').hasClass('error'))
        {
            event.currentTarget.closest('.field').classList.remove('error');
            if(event.currentTarget.closest('.field').nextElementSibling.classList.contains('message')) {
                event.currentTarget.closest('.field').nextElementSibling.remove();
            }
        }
        event.persist();
		
		
		
        this.setState(function(previousState, currentProps) {
            return previousState.form[event.target.getAttribute("name")] = event.target.value;
        });
    }
	
	handleChangeTime(field, val){
		this.setState(function(previousState, currentProps) {
            return previousState.form[field] = val;
        });
	}
    
	getFormSelector(){
		return '.ui.add.form';
	}
    /**
     *
     * @param r
     */
    errorValidate(r)
    {
        var self = this;
        $(this.getFormSelector()).addClass('error');
        for(var key in r.text)
        {
            if(!$(self.selectorError(key)).closest('.field').hasClass('error'))
            {
                $(self.selectorError(key)).closest('.field').addClass('error');
                $(self.selectorError(key)).closest('.field').after('<div class="ui error message"><ul></ul></div>')
             
				r.text[key].forEach(function(item, i, arr) {
                    $($($(self.selectorError(key)).closest('.field')[0].nextElementSibling).find('ul')).append('<li>'+item+'</li>');
                });
            }
        }
    }

    /**
     *.addClass('error')
     */
    clearErrorValidate()
    {
		document.querySelector('.ui.add.form').classList.remove('error');
		var message = document.querySelectorAll('.ui.error.message');
		for(var i = 0; i < message.length; i++){
			message[i].remove();
		}
		var error = document.querySelectorAll('.ui.add.form .field.error');
		for(var i = 0; i < error.length; i++){
			error[i].classList.remove('error');
		}
		
       // $('.ui.add.form').removeClass('error');
      //  $('.ui.error.message').remove();
     //   $('.ui.add.form .field.error').removeClass('error');
    }

    /**
     *
     * @param key
     * @returns {string}
     */
    selectorError(key)
    {
        return '.ui.add.form input[name="'+key+'"], .ui.add.form textarea[name="'+key+'"], .ui.add.form select[name="'+key+'"]';
    }

    fixDimmer(){
        if($('.dimmer').length && $('.dimmer').hasClass('visible')){
            $('.dimmer').transition('hide');
        }
    }
	
	
	render(){
		var self = this;
		
		return(
			<Modal open={this.props.open} trigger={this.props.trigger} onClose={this.props.onClose} onOpen={this.props.onOpen} closeIcon='close'>
				<Header content={this.state.status == 'create' ? 'Создание ' + this.state.headerContent : 'Обновление ' + this.state.headerContent} />
				<Modal.Content>
				<form className="ui add form">
					{this.state.formRender.map(function(data, i){
						
						// var label = data.label ? (<label>{data.label}</label>) : '';
						var inp = '';
                        
						switch(data.input){
							case 'input':  inp = (
                                <input id={data.id != '' ? data.id : ''} value={self.state.form[data.field]} onKeyUp={self.handleChangeProperty.bind(self)} onChange={self.handleChangeProperty.bind(self)} name={data.field} placeholder={data.placeholder} type={data.type} />
                            ); break;
							case 'textarea': inp = (
                                <textarea id={data.id != '' ? data.id : ''} value={self.state.form[data.field]} onKeyUp={self.handleChangeProperty.bind(self)} onChange={self.handleChangeProperty.bind(self)} name={data.field} placeholder={data.placeholder} type={data.type} />
                            ); break;
							case 'select':
								inp = (
									<BaseDropdown value={self.state.form[data.field]} placeholder={data.placeholder} name={data.field} onChange={self.handleChangeDropdown.bind(self)} options={{data: data.data.items/*self.props.list.state.dopModel[data.data].items*/, value: 'id', text: 'name'}}/>
								);
							break;
						}
		
						return(
							<div key={i} className="field">
								<label>{data.label}</label>
								{inp}
							</div>
						)
					})}
                </form>
				</Modal.Content>
				<BaseFormAction form={this}/>
			 </Modal>
		)
	}
}
BaseAdd.defaultProps = {listControl: 'open'};