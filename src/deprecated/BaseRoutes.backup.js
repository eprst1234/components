import React from 'react';
import { Router, Route, IndexRoute, IndexLink, Link, browserHistory } from 'react-router';
import laroute from './../laroute';
import BaseAppLayout from './BaseAppLayout';


import IndexAdmin from './../admin/Content/IndexAdmin';
import IndexCurator from './../curator/Content/IndexCurator';
import IndexOperator from './../operator/Content/IndexOperator';
import IndexCourier from './../courier/Content/IndexCourier';
import IndexCustomer from './../customer/Content/IndexCustomer';
import IndexBuhgalter from './../buhgalter/Content/IndexBuhgalter';
import IndexOperatorCurrency from './../operatorCurrency/Content/IndexOperatorCurrency';

import AuditorRoutes from './../auditor/AuditorRoutes';
import AdminRoutes from './../admin/AdminRoutes';
import CuratorRoutes from './../curator/CuratorRoutes';
import OperatorRoutes from './../operator/OperatorRoutes';
import CourierRoutes from './../courier/CourierRoutes';
import CustomerRoutes from './../customer/CustomerRoutes';
import BuhgalterRoutes from './../buhgalter/BuhgalterRoutes';
import OperatorCurrencyRoutes from './../operatorCurrency/OperatorCurrencyRoutes';

import AuditorAppLayout from './../auditor/AuditorAppLayout';
import AdminAppLayout from './../admin/AdminAppLayout';
import CuratorAppLayout from './../curator/CuratorAppLayout';
import OperatorAppLayout from './../operator/OperatorAppLayout';
import CourierAppLayout from './../courier/CourierAppLayout';
import CustomerAppLayout from './../customer/CustomerAppLayout';
import BuhgalterAppLayout from './../buhgalter/BuhgalterAppLayout';
import OperatorCurrencyAppLayout from './../operatorCurrency/OperatorCurrencyAppLayout';

import Ticket from './Content/Ticket/Ticket';
import AccountTicket from './Content/AccountTicket/AccountTicket';
import MessagesAccountTicket from './Content/MessagesAccountTicket/MessagesAccountTicket';

import MessagesTicket from './Content/MessagesTicket/MessagesTicket';
import MyNotification from './Content/MyNotification/MyNotification';
import Tasks from './Content/Tasks/Tasks';
import TaskView from './Content/Tasks/TaskView';
import Messages from './Content/Messages/Messages';
import Threads from './Content/Threads/Threads';
import Profile from './Content/Profile/Profile';

import CustomerMessagesTicket from './../customer/Content/MessagesTicket/MessagesTicket';

import { BaseNotFound } from './BaseNotFound';
import BaseCrypto from './BaseCrypto';

export default class BaseRoutes extends React.Component
{
    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            component: null,
            user: null
        };
    }

    /**
     *
     * @param component
     */
    setBaseComponent(component)
    {
        this.state.component = component;
    }

    /**
     *
     * @returns {null}
     */
    getBaseComponent()
    {
        return this.state.component;
    }
	
	queryStringToHash(query) {

	  if (query == '') return null;

	  var hash = {};

	  var vars = query.split("&");

	  for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split("=");
		var k = decodeURIComponent(pair[0]);
		var v = decodeURIComponent(pair[1]);

		
		if (typeof hash[k] === "undefined") {

		  if (k.substr(k.length-2) != '[]')  
			hash[k] = v;
		  else
			hash[k.substr(0, k.length-2)] = [v];

		
		} else if (typeof hash[k] === "string") {
		  hash[k] = v;  

		} else {
		  hash[k.substr(0, k.length-2)].push(v);
		}
	  } 
	  return hash;
	}
	

    /**
     *
     */
    componentDidMount() {
        var self = this;
		
		/*Object.prototype.customCl = function(){
			console.log(this);
			return JSON.parse(JSON.stringify(this));
		}*/
		
		
		if(window.cryptoRequest){
			
			$.ajaxPrefilter(function(options, originalOptions, jqXHR){
				if(originalOptions.type == 'GET'){
					if(typeof originalOptions.data != 'undefined'){
						var data = originalOptions.data;
						BaseCrypto.cryptoData(data, 1);
						options.data = $.param(data);
					}
				}
			});

			$.ajaxSetup({
				beforeSend: function(xhr, settings){
					if(settings.type == 'DELETE'){
						
					}
					else if(settings.type == 'GET'){
						
					}
					else{
						if(typeof settings.data !== 'undefined'){
							var data = self.queryStringToHash(settings.data);
							if(data != null){
								BaseCrypto.cryptoData(data, 1);
								settings.data = $.param(data);
							}
						}
						return true;
					}
				},
				dataFilter: function(data, dataType) {
					var filteredData = $.parseJSON(data);
					if(filteredData.hasOwnProperty('ct') && filteredData.hasOwnProperty('iv')){
						filteredData = BaseCrypto.cryptoFunc(JSON.stringify(filteredData));
					}else{
						BaseCrypto.cryptoData(filteredData.data);
					}
					return JSON.stringify(filteredData);
				}
			});
		}
		
		window.onerror = function(message, file, line, column, error) {
                    var errorData = {
						message: message,
						file: file,
						line: line,
						column: column,
						trace: error.stack,
						user_agent: navigator.userAgent,
					};

                    BaseAppLayout.makeRequest({
                        type: "POST",
                        url: window.laroute.route("error.logs"),
                        cache: false,
                        data: errorData
                    });

                    console.log(errorData);
            };
			
			
			function proxy(context, method, message) { 
			  return function() {
				// console.log(arguments);
				 // self.getStackTrace();
				//console.log(self.getStackTrace());
				
				var errorData = {
						message: arguments[0],
						trace: self.getStackTrace(),
						user_agent: navigator.userAgent,
					};

                  BaseAppLayout.makeRequest({
                      type: "POST",
                      url: window.laroute.route("error.logs"),
                      cache: false,
                      data: errorData,
                  });

				method.apply(context, [message].concat(Array.prototype.slice.apply(arguments)))
			  }
			}

			// let's do the actual proxying over originals
		
			console.error = proxy(console, console.error, 'Error:')
			console.warn = proxy(console, console.warn, 'Warning:')

			// let's test
			//console.log('im from console.log', 1, 2, 3);
		//	console.error('im from console.error', 1, 2, 3);
		//	console.warn('im from console.warn', 1, 2, 3);
			
		
		//setInterval(function(){
			self.getAuthUser(
				function (response) {
					self.setState(function (previousState, currentProps) {
						previousState.user = response.data;
					});
				},
				function (response) {

				}
			);
		//}, 5000);
		
		//self.authUser();
	
	}
	
	
	getStackTrace() {
	  try {
		  var obj = {};
		  Error.captureStackTrace(obj);
		  return obj.stack;
	  }catch(err){
		  return 'Not stack';
	  }
	}
	
	/*authUser(){
		var self = this;
		self.getAuthUser(
				function (response) {
					self.setState(function (previousState, currentProps) {
						previousState.user = response.data;
					});
				},
				function (response) {

				}
			);
		
		}*/

    /**
     *
     * @param text
     * @returns {string}
     */
    getUrlWithRole(text)
    {
        return "/backend/" + this.state.user.main_role.slug + "/" + text;
    }

    /**
     *
     */
    getAuthUser(funcSuccess, funcError)
    {
        BaseAppLayout.makeRequest({
            type: "GET",
            url: window.laroute.route("profile"),
            cache: false,
            success: function(response)
            {
                if(response.meta.result === "success"){
                    return funcSuccess(response);
                }
                return funcError(response);
            },
            error()
            {

            }
        });
    }
    
    employRroutes()
    {
        return [
            {name: "ticket", path: "/backend/ticket", component: Ticket},
			{name: "account-ticket", path: "/backend/account-ticket", component: AccountTicket},
			{name: "messages-account-ticket", path: "/backend/messages-account-ticket/:thread_id", component: MessagesAccountTicket},
            {name: "messages-ticket", path: "/backend/messages-ticket/:thread_id", component: MessagesTicket},
            {name: "notification", path: "/backend/notifications/my", component: MyNotification},
            {name: "tasks", path: "/backend/tasks", component: Tasks},
            {name: "tasks", path: "/backend/tasks/:task_id", component: TaskView},
            {name: "messages", path: "/backend/messages/:thread_id", component: Messages},
            {name: "threads", path: "/backend/threads", component: Threads},
            {name: "profile", path: "/backend/profile", component: Profile},
            {name: "not_found", path: "*", component: BaseNotFound},
        ]
    }

    customerRoutes()
    {
        return [
            {name: "ticket", path: "/backend/ticket", component: Ticket},
            {name: "messages-ticket", path: "/backend/messages-ticket/:thread_id", component: CustomerMessagesTicket},
            {name: "notification", path: "/backend/notifications/my", component: MyNotification},
            {name: "messages", path: "/backend/messages/:thread_id", component: Messages},
            {name: "threads", path: "/backend/threads", component: Threads},
            {name: "profile", path: "/backend/profile", component: Profile},
            {name: "not_found", path: "*", component: BaseNotFound},
        ]
    }

    /**
     * 
     * @returns {XML}
     */
    render()
    {
        var self = this;
		var index = '';

        if(this.state.user){
            BaseAppLayout.setUser(this.state.user);
            
            var routes = [];

            switch(this.state.user.main_role.slug){
                case 'auditor':
                    routes = AuditorRoutes.routes().concat(this.employRroutes());
                    index = (<IndexRoute name="index" component={IndexAdmin} />);
                    self.setBaseComponent(AuditorAppLayout);
                    break;
                case 'admin':
                    routes = AdminRoutes.routes().concat(this.employRroutes());
                    index = (<IndexRoute name="index" component={IndexAdmin} />);
                    self.setBaseComponent(AdminAppLayout);
                    break;
                case 'curator':
                    routes = CuratorRoutes.routes().concat(this.employRroutes());
                    index = (<IndexRoute name="index" component={IndexCurator} />);
                    self.setBaseComponent(CuratorAppLayout);
                    break;
                case 'operator':
                    routes = OperatorRoutes.routes().concat(this.employRroutes());
                    index = (<IndexRoute name="index" component={IndexOperator} />);
                    self.setBaseComponent(OperatorAppLayout);
                    break;
                case 'courier':
                    routes = CourierRoutes.routes().concat(this.employRroutes());
                    index = (<IndexRoute name="index" component={IndexCourier} />);
                    self.setBaseComponent(CourierAppLayout);
                    break;
                case 'customer':
                    routes = CustomerRoutes.routes().concat(this.customerRoutes());
                    index = (<IndexRoute name="index" component={IndexCustomer} />);
                    self.setBaseComponent(CustomerAppLayout);
                    break;
                case 'buhgalter':
                    routes = BuhgalterRoutes.routes().concat(this.employRroutes());
                    index = (<IndexRoute name="index" component={IndexBuhgalter} />);
                    self.setBaseComponent(BuhgalterAppLayout);
                    break;
                case 'operatorcurrency':
                    routes = OperatorCurrencyRoutes.routes().concat(this.employRroutes());
                    index = (<IndexRoute name="index" component={IndexOperatorCurrency} />);
                    self.setBaseComponent(OperatorCurrencyAppLayout);
                    break;
            }


            return (
                <Router history={browserHistory}>
                    <Route path="/backend" component={this.getBaseComponent()}>
                        {index}
                        {routes.map(function (item, i) {
                            return (<Route key={i} name={item.name} path={item.path} component={item.component}/>);
                        })}
                    </Route>
                </Router>
            );
        }else{
            return (<div></div>);
        }
    }
}