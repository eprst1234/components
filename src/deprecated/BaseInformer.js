import React from 'react'

export default class BaseInformer extends React.Component
{
    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            sort: false,
        };
    }
    
    render()
    {
        
        return (
            <div className="informer">
				{this.props.redText != '' ? <p>Красный - {this.props.redText}</p> : ''}
				{this.props.yellowText != '' ? <p>Желтый - {this.props.yellowText}</p> : ''}
				{this.props.greenText != '' ? <p>Зеленый - {this.props.greenText}</p> : ''}
				{this.props.blackText != '' ? <p>Черный - {this.props.blackText}</p> : ''}
				{this.props.brownText != '' ? <p>Коричневый - {this.props.brownText}</p> : ''}
				{this.props.cityReadText != '' ? <p>Красный город - {this.props.cityReadText}</p> : ''}
				{this.props.cityYellowText != '' ? <p>Желтый город - {this.props.cityYellowText}</p> : ''}
			</div>
        );
    }
    
    
    
}