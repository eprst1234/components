import React from 'react'
import BaseState from './BaseState'
import BaseStateFilter from './BaseStateFilter'
import moment from 'moment'
import 'moment/locale/ru';
moment.locale('ru');

export default class BaseIndexCharts extends BaseState
{
	
	constructor(props) {
        super(props);
        this.state = {
			date_start: moment().subtract(7, "days"),
			date_end: moment(),
			filterCity: false,
			city_id: '',
        };
    }
	
	statistiks(){
		var self = this;
		this.props.main.refs[this.props.refChart].getData(this.state.date_start, this.state.date_end, this.state.city_id, function(response){

		});
	}
	
    render() 
    {
        return (
            <div>
                <BaseStateFilter list={this}/>
				{this.props.children}
            </div>
        );
    }
}
