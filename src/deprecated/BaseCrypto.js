export default class BaseCrypto
{
	 
     static cryptoFunc(data, crypt = 0){
		var CryptoJSAesJson = {
			stringify: function (cipherParams) {
				var j = {ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64)};
				if (cipherParams.iv) j.iv = cipherParams.iv.toString();
				if (cipherParams.salt) j.s = cipherParams.salt.toString();
				return JSON.stringify(j);
			},
			parse: function (jsonStr) {
				var j = JSON.parse(jsonStr);
				var cipherParams = CryptoJS.lib.CipherParams.create({ciphertext: CryptoJS.enc.Base64.parse(j.ct)});
				if (j.iv) cipherParams.iv = CryptoJS.enc.Hex.parse(j.iv);
				if (j.s) cipherParams.salt = CryptoJS.enc.Hex.parse(j.s);
				return cipherParams;
			}
		};
		if(crypt == 0){
			return JSON.parse(CryptoJS.AES.decrypt(data, window.pass, {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8));
		}else{
			return CryptoJS.AES.encrypt(JSON.stringify(data), window.pass, {format: CryptoJSAesJson}).toString();
		}
	}
	
	static cryptoData(data, crypt = 0){
		
		if(data instanceof Object || data instanceof Array){
			for(var key in data){
				if(data[key] instanceof Object || data[key] instanceof Array){
					BaseCrypto.cryptoData(data[key], crypt);
				}
				else{
					if(crypt == 0){
						data[key] = BaseCrypto.cryptoFunc(data[key], crypt);
					}
					else{
						if(typeof data[key] != 'undefined')
							data[key] = BaseCrypto.cryptoFunc(data[key], crypt);
						else
							delete data[key];
					}
					
				}
			}
		}else{
			//return JSON.parse(CryptoJS.AES.decrypt(data, window.pass, {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8));
			//return CryptoJS.AES.encrypt(JSON.stringify(data), window.pass, {format: CryptoJSAesJson}).toString()
		}
	}
	
	
};