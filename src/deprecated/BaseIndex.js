import React from 'react'
export default class BaseIndex extends React.Component 
{
	constructor(props) {
        super(props);
        this.state = {
            header: "Панель",
			charts: [],
        };
    }
	
    render() 
    {
		var self = this;
        return (
            <div>
                <h3 className="ui header">{this.state.header}</h3>
            </div>
        );
    }
}
