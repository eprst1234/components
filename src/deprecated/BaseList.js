import React from 'react'

export default class BaseList extends React.Component{
    /**
     * 
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            items: [],
            total_pages: null,
            current_page: 0,
            last_page: 0,
            per_page: 0,
            fields: {},
            deleted_at: {},
            orderBy: [{
                order: 'id',
                sort: 'asc',
            }]
        };
    }
	
	filterField(field, event){
		var val = event.target.value;
        var self = this;
		var key = '';
		this.state.fields.map(function(data, i){
			if(data.k == field)
				key = i;
		});
		
		if (val != '') {	
            this.setState(function (previousState, currentProps) {
				if(key == ''){
					previousState.fields.push({k: field, v: val});
				}else{
					previousState.fields[key].k = field;
					previousState.fields[key].v = val;
				}
                return previousState;
            });
        }else{
			
			if(key !== ''){
				this.setState(function (previousState, currentProps) {
					previousState.fields.splice(key, 1);
					return previousState;
				});
			}
		}
	}

    /**
     * 
     */
    componentDidMount() {
		
		var self = this;
		
		/*$('.ui.add').modal({
			onHide: function(){
				console.log('qqq');
			},
		});*/
		
        $('body > .ui.dimmer.modals > .ui.small.add.modal').remove();
		$('body > .ui.dimmer.modals > .ui.add.modal').remove();
        document.title = this.state.title;
		
		this.props.parent.refs.preload.setState({handleWatchVisible: true}, function(){
			self.handleWatch();
		});
		
		try{
			this.setState(function (previousState, currentProps) {
				previousState.date_start = null;
				previousState.date_end = null;
				return previousState;
			});
		}catch(e){}
		

        $('.ui.checkbox').checkbox();
        $('.ui.dropdown.item').dropdown();
		
        this.startTimer();
        this.setQuery(this);
		this.didMountCallback();
    }
	didMountCallback(){
		
	}

    /**
     *
     */
    setQuery(object)
    {
        if(Object.keys(this.props.location.query).length !== 0){
            object.setState(function (previousState, currentProps) {
                var self = this;
                previousState.current_page = this.props.location.query.page;

                var re = /^([A-Za-z]+)\[([A-Za-z0-9\_]+)\]\[([A-Za-z0-9\_]+)\]/i;

                previousState.fields = [];
                Object.keys(this.props.location.query).forEach(function (item, i, arr) {
                    try {
                        var res = item.toString().match(re);
                        if(res !== null && res.length >= 3){
                            switch(res[1])
                            {
                                case 'fields':
                                    if(res[3] === 'k'){
                                        break;
                                    }
                                    previousState.fields.push({k: self.props.location.query['fields[' + res[2] + '][k]'], v: self.props.location.query['fields[' + res[2] + '][v]']});
                                    break;
                                case 'orderby':
                                    try {
                                        previousState.orderBy.forEach(function (orderByItem, orderByI, orderByArr) {
                                            if(orderByItem.order == res[2]){
                                                previousState.orderBy[orderByI] = {order: res[2], sort: self.props.location.query['orderby[' + res[2] + ']']};
                                            }else{
                                                previousState.orderBy.push({order: res[2], sort: self.props.location.query['orderby[' + res[2] + ']']});
                                            }
                                        });
                                    } catch (e) {
                                        console.log(e);
                                    }
                                    break;
                                case 'like':
                                    try {
                                        previousState.like.forEach(function (likeItem, likeI, likeArr) {
                                            if(likeItem.field == res[2]){
                                                 previousState.like[likeI] = {field: res[2], value: self.props.location.query['like[' + res[2] + ']'].replace(/\%/, '').replace(/\%/, ''), val: self.props.location.query['like[' + res[2] + ']']};
                                            }
                                        });
                                    } catch (e) {
                                        console.log(e);
                                    }

                                    break;
                                case 'has':
                                    try {
                                        previousState.has.forEach(function (hasItem, hasI, hasArr) {
                                            if(hasItem.field == res[2]){
                                                previousState.has[hasI] = {field: res[2], value: self.props.location.query['has[' + res[2] + ']'].split(',')[0], val: self.props.location.query['has[' + res[2] + ']']};
                                            }
                                        });
                                    } catch (e) {
                                        console.log(e);
                                    }
                                    break;
                            }
                        }
                    } catch (e) {
                        console.log(e);
                    }
                });
            });
        }
    }
    
    sort(field, sort, event)
    {
        var self = this;
        var prov = false;
        
        this.setState(function(previousState, currentProps) {
                previousState.orderBy.forEach(function(item, i, arr) {
                    if(item.order == field)
                    {
                        if(sort==false)
                            previousState.orderBy.splice(i, 1);
                        else
                            item.sort = sort;
                        
                        prov = true;
                    }
                });
                
                if(!prov)
                    previousState.orderBy.push({order: field, sort: sort});
                    
                self.handleWatch();
                return previousState;
        }); 
    }

    /**
     * 
     * @param event
     */
    handleAdd(event) 
    {
		var self = this;
		this.openModal('open');
        var form = this.refs.form;
        form.handleClearForm();
		self.setRelationId();
        form.setState(function(previousState, currentProps) {
            return previousState.status = "create";
        });
    }
	

    /**
     *
     * @param event
     */
    handleUpdate(event) 
    {
		this.openModal('open');
        this.refs.form.setState(function(previousState, currentProps) {
            return previousState.status = "update";
        });
    }
	
	getAddModal(){
		return '.ui.add.modal';
	}

    /**
     *
     * @param event
     */
    handleView(event)
    {
		this.openModal('open');
        this.refs.form.setState(function(previousState, currentProps) {
            return previousState.status = "update";
        });
    }

    /**
     * 
     */
    handleWatch()
    {
		var self = this;
        this.props.parent.getModels(this, function (response) {
			self.handleWatchCallback(response);
        });
    }
	
	
	openModal(modal){
		this.controlModal(modal, true);
	}
	
	closeModal(modal){
		this.controlModal(modal, false);
	}
	
	
	controlModal(modal, val){
		this.setState(function(prv){
			prv[modal] = val;
		})
	}
	
	
	handleWatchCallback(response){
		
	}
	
    
    /**
     * 
     */
     
     setRelationId()
     {
        //empty
     }
     
     startTimer()
     {
        //empty
     }
}