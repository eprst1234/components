import Api from "../api/Api";
import PayMethod from "./PayMethod";
import { Auth } from 'components';

export default class ComponentRepository
{
    /**
     *
     * @type {{}}
     */
    static components = {};

    /**
     *
     * @param key
     * @returns {*}
     */
    static getComponent(key)
    {
        return ComponentRepository.components[key];
    }

    /**
     *
     * @param component
     * @param key
     * @param force
     */
    static setComponent(component, key, force = false)
    {
        if(force || typeof ComponentRepository.components[key] === 'undefined'){
            ComponentRepository.components[key] = component;
        }
    }

    /**
     *
     * @param key
     * @returns {*}
     */
    static get(key)
    {
        return ComponentRepository.getComponent(key);
    }

    /**
     *
     * @param component
     * @param key
     */
    static set(component, key)
    {
        return ComponentRepository.setComponent(component, key);
    }

    /**
     *
     * @param component
     * @param key
     */
    static resetComponent(component, key)
    {
        ComponentRepository.components[key] = component;
    }

    /**
     * можно вернуть текущего пользователя,
     * запрос работает синхронно
     * если пользователь по каким то причинам не будет получен то вернется null
     *
     *
     * @param key
     * @param force
     * @returns {*}
     */
    static resolve(key, force = false)
    {
        if(typeof ComponentRepository.components[key] === 'undefined' || force){
            switch(key){
                case 'user':
                    ComponentRepository.components[key] = ComponentRepository.userResolver();
                    break;
				case 'payMethod':
					ComponentRepository.components[key] = new PayMethod();
					break;
            }
        }

        return ComponentRepository.components[key];
    }

    /**
     *
     * @returns {*|}
     */
    static userResolver()
    {
        let user = null;

        Api.makeRequest(
            {
                type: "GET",
                url: laroute.route("api.v1.call", {target: 'profile', method: 'index'}),
                cache: false,
                async: false,
                success: function (response) {
                    user = response.data;
                    Auth.setUser(user);
                },
                error: function () {

                }
            }
        );

        return user;
    }
}