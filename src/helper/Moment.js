import ComponentRepository from './ComponentRepository';
import moment from "moment";
import 'moment/locale/ru';
moment.locale('ru');

export default class Moment
{
    static format = 'DD.MM.YYYY HH:mm:ss';
	
	
	static diff(date1, date2){
		date1 = moment(date1, Moment.format);
		date2 = moment(date2, Moment.format);
		return date1.unix() - date2.unix();
	}
	
	static diffNow(date){
		return Moment.diff(date, ComponentRepository.resolve('user').now);
	}
}