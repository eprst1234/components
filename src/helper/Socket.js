class Socket{
	
	static params = {
		url: window.wsUrl,
		session: window.session,
	}
	
	static setParams(params){
		Socket.params = params;
	}
	
	constructor(onOpen, onMessagem, open = true){
		this.params = Socket.params;
		var self = this;
		var onerror = '';
		var onclose = ''
		var onmessage = '';
		var onopen = '';
		var idInterval = false;
		
		if(open)
			this.open();

		this.actions = {};


		this.onmessage = function(data){
			onMessage(data);
		};

		onopen = function (event) {
			onOpen();
		}

		onmessage = function (event){
			if(event){
				var json = JSON.parse(event.data);
				if(typeof self.actions[json.action.toCamelCase()] !== 'undefined'){
					self.actions[json.action.toCamelCase()](json.data);
				}else{
					self.onmessage(json);
				}
			}
		}


		onerror = function (event) {
			if (idInterval == false) {
				idInterval = window.setTimeout(
					function () {
						self.socket = new WebSocket(window.wsUrl);
						self.socket.onopen = onopen;
						self.socket.onmessage = onmessage;
						self.socket.onerror = onerror;
						self.socket.onclose = onclose;
						idInterval = false;
					},
					10000);
			}
		};

		onclose = function (event) {
			if (idInterval == false) {
				idInterval = window.setTimeout(
					function () {
						self.socket = new WebSocket(window.wsUrl);
						self.socket.onopen = onopen;
						self.socket.onmessage = onmessage;
						self.socket.onerror = onerror;
						self.socket.onclose = onclose;
						idInterval = false;
					},
					10000);
			}
		};


		this.socket.onopen = onopen;
		this.socket.onmessage = onmessage;
		this.socket.onerror = onerror;
		this.socket.onclose = onclose;
	}
	
	
	open(){
		this.socket = new WebSocket(this.params.url);
	}
	
	send(action, args, data){
		if(typeof args === 'undefined'){
			args = {};
		}
		if(typeof data === 'undefined'){
			data = {};
		}

		data['sessionId'] = this.params.session;
		this.socket.send(JSON.stringify({
			action: action,
			data: data,
			arguments: args
		}));
	}

}

String.prototype.toCamelCase = function(){
    return this.replace('.', ' ').replace(/^([A-Z])|\s(\w)/g, function(match, p1, p2, offset) {
        if (p2) return p2.toUpperCase();
        return p1.toLowerCase();
    });
};

export default Socket;