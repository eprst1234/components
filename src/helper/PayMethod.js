import Api from "../api/Api";

export default class Course
{
	constructor(){
		var self = this;
		this.data = [];
		this.request = false;
		this.callable = function(){};
		Api.get('pay-method', 'index').call(function(response){
			self.request = true;
			self.data = response.data;
			self.callable(self.data);
		});
	}
	
	set(func){
		if(this.request){
			func(this.data);
		}else{
			this.callable = func;
		}
	}
	
	get(){
		return this.data;
	}
    
}