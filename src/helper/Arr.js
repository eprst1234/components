export default class Arr
{
    static propIndexOf(arr, search, prop){
		for (var i = 0, len = arr.length; i < len; i++) {
			if (arr[i][prop] === search) return i;
		}
		return -1;
	}
	
	static sort(arr, prop){
		var newArr = [];
		while(arr.length > 0){
			var min = Arr.min(arr, prop);
			newArr.push(arr[Arr.min(arr, prop)]);
			arr.splice(min, 1);
		}
		return newArr;
	}
	
	static min(arr, prop){
		var min = 0;
		for (var i = 0; i < arr.length; i++) {
			if (arr[i][prop] < arr[min][prop])
				min = i;
		}
		return min;
	}
	
	static getPropRecursive(arr, path, exception = true){
		var pathArr = path.split('.');
		pathArr.map(function(data, i){
			if(exception && !(data in arr)){
				throw {
					message: 'Не найдено свойство объекта',
					array: arr,
					step: i,
					field: data,
				};
			}
			arr = arr[data];
		});
		return arr;
	}
}