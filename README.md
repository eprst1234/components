# Components

This components for another project...

## Getting Started

....

## Examples

*  [BaseForm](#BaseHeader)
*


### BaseForm
        
#### Property:

Name | Default | Type | Description
:--- | :--- | :--- | :---
uri |  |  | 
head |  |  | 
isEdit | false | {bool} | 
id | 0 | {number} | 
goBack |  | {func} | 
editedFields | [] | {array} | 
panelIcon | 'edit' | {string} | 


Example code:
~~~
    <BaseForm
        uri='region'
        head='This my Form'
        isEdit={true}
        id={this.props.match.params.id}
        goBack={this.props.history.goBack}
    />
~~~
